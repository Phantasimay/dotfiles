SELECT
  *
FROM
  produk;

DROP TABLE barang;

CREATE TABLE produk (
  id VARCHAR(10) NOT NULL,
  name VARCHAR(100) NOT NULL,
  description TEXT,
  price INT UNSIGNED NOT NULL,
  quantity INT UNSIGNED NOT NULL DEFAULT 0,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB;

DESC produk;

DELETE FROM produk
WHERE
  id REGEXP '^P000..$';

INSERT INTO
  produk (id, name, price, quantity)
VALUES
  ('P0001', 'Mie Ayam Original', 15000, 100),
  ('P0002', 'Bakso Jumbo', 22000, 100),
  ('P0003', 'Bakso Spesial', 25000, 100),
  ('P0004', 'Mie Ayam Bakso Jumbo', 25000, 100),
  ('P0005', 'Mie Ayam Bakso Original', 17000, 100),
  ('P0006', 'Mie Ayam  Bakso Tengkleng', 24000, 100),
  ('P0007', 'Teh Es/Anget', 3000, 100),
  ('P0008', 'Jeruk Es/Anget', 3000, 100),
  ('P0009', 'Es Tape', 3000, 100),
  ('P0010', 'Es Teler', 10000, 100),
  ('P0011', 'Kentang Goreng', 11000, 100),
  ('P0012', 'Kerupuk', 1000, 100);

SELECT
  *
FROM
  produk
ORDER BY
  id ASC;

ALTER TABLE produk ADD PRIMARY KEY (id);

SHOW
CREATE TABLE produk;

INSERT INTO
  produk (id, name, price, quantity)
VALUES
  ('P0001', 'Mie Ayam Original', 15000, 100);

SELECT
  *
FROM
  produk
WHERE
  price = 25000;

ALTER TABLE produk
ADD COLUMN category ENUM ('Makanan', 'Minuman', 'Lain-lain') AFTER name;

UPDATE produk
SET
  category = 'Makanan'
WHERE
  id REGEXP '^P00[01-14]';

UPDATE produk
SET
  category = 'Minuman'
WHERE
  id REGEXP '^P00(16|17|18)';

UPDATE produk
SET
  category = 'Lain-lain'
WHERE
  id REGEXP '^P00(19|20)';

SELECT
  *
FROM
  produk
WHERE
  id REGEXP '^P00([1][1-9]|[2][0-9])';

UPDATE produk
SET
  description = 'Mie Ayam Original + Ceker'
WHERE
  id REGEXP '^P0003';

UPDATE produk
SET
  description = 'Mie Ayam bumbu spesial'
WHERE
  id REGEXP '^P0004';

UPDATE produk
SET
  price = price + 2000
WHERE
  id REGEXP '^P0007';

UPDATE produk
SET
  price = price -15000;

SELECT
  id AS 'Kode',
  name AS 'Nama Produk',
  category AS 'Kategori',
  price AS 'Harga',
  quantity AS 'Jumlah Stok'
FROM
  produk;

SELECT
  menu.id AS 'Kode',
  menu.name AS 'Nama Produk',
  menu.category AS 'Kategori',
  menu.price AS 'Harga',
  menu.quantity AS 'Jumlah Stok'
FROM
  produk AS menu;

SELECT
  *
FROM
  produk
WHERE
  category != 'Makanan';

SELECT
  *
FROM
  produk
WHERE
  price > 15000;

SELECT
  *
FROM
  produk
WHERE
  price <= 10000;

SELECT
  *
FROM
  produk
WHERE
  price > 10000
  AND category != 'Makanan';

SELECT
  name,
  price
FROM
  produk
WHERE
  price < 10000
  OR category = 'Makanan';

SELECT
  id,
  name
FROM
  produk
WHERE
  price < 10000
  OR category = 'Makanan';

SELECT
  *
FROM
  produk
WHERE
  (
    price > 10000
    OR category != 'Makanan'
  )
  AND description IS NOT NULL;

SELECT
  *
FROM
  produk
WHERE
  description LIKE '%Original%';

SELECT
  *
FROM
  produk
WHERE
  description REGEXP 'Original';

SELECT
  *
FROM
  produk
WHERE
  description NOT LIKE '%Original%';

SELECT
  *
FROM
  produk
WHERE
  description NOT REGEXP 'Original';

SELECT
  *
FROM
  produk
WHERE
  price BETWEEN 10000 AND 20000;

SELECT
  *
FROM
  produk
WHERE
  price IN (10000, 20000);

SELECT
  *
FROM
  produk
WHERE
  price = 10000
  OR price = 20000;

SELECT
  category,
  name,
  price
FROM
  produk
ORDER BY
  name DESC,
  price ASC;

SELECT
  category,
  name,
  price
FROM
  produk
WHERE
  price > 0
ORDER BY
  name DESC,
  price ASC;

SELECT
  category,
  name,
  price
FROM
  produk
ORDER BY
  price DESC,
  name ASC;

SELECT
  category,
  name,
  price
FROM
  produk
ORDER BY
  price DESC,
  name ASC
LIMIT
  7;

SELECT
  category,
  name,
  price
FROM
  produk
ORDER BY
  price DESC,
  name ASC
LIMIT
  0, 7;

SELECT
  category,
  name,
  price
FROM
  produk
ORDER BY
  price DESC,
  name ASC
LIMIT
  2, 5;

SELECT
  category
FROM
  produk;

SELECT DISTINCT
  category
FROM
  produk;

SELECT
  2 + -50 AS hasil;

SELECT
  name,
  price,
  price DIV 1000 AS 'Harga dalam ribuan'
FROM
  produk;

SELECT
  name,
  price,
  price DIV 1000 > 15 AS 'Harga dalam ribuan'
FROM
  produk;

SELECT
  name,
  price,
  price DIV 1000 AS 'Harga dalam ribuan'
FROM
  produk
WHERE
  price DIV 1000 > 15;

SELECT
  name,
  price,
  price / 1000 AS 'Harga dalam ribuan'
FROM
  produk;

SELECT
  name,
  price,
  price / 1000 > 15 AS 'Harga dalam ribuan'
FROM
  produk;

SELECT
  name,
  price,
  price / 1000 AS 'Harga dalam ribuan'
FROM
  produk
WHERE
  price DIV 1000 > 15;

SELECT
  PI () AS 'PI',
  POWER(10, 2) AS '10^2',
  EXP(2) AS 'E+2',
  COS(10) AS 'cos 10',
  SIN(10) AS 'sin 10',
  TAN(10) AS 'tan 10',
  LOG (10) AS 'log 10',
  LN(10) AS 'ln 10';

ALTER TABLE produk
-- DROP PRIMARY KEY,
ADD PRIMARY KEY (id);

DESC produk;

ALTER TABLE produk
DROP PRIMARY KEY,
ADD COLUMN new_id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT FIRST;

UPDATE produk p
SET
  p.new_id = SUBSTRING(p.id, 2);

SELECT
  LAST_INSERT_ID ()
FROM
  produk;

SELECT
  *
FROM
  produk;

INSERT INTO
  produk (id, name, price, quantity)
VALUES
  ('P0013', 'Onion Burger', 24000, 100);

SELECT
  new_id,
  LOWER(name) "Lower Case Menu",
  LENGTH (name) "Jumlah huruf"
FROM
  produk;

SELECT
  new_id,
  EXTRACT(
    YEAR
    FROM
      created_at
  ) 'Ekstrak Tahun Input',
  YEAR (created_at) 'Alternatif Ekstrak Tahun',
  EXTRACT(
    MONTH
    FROM
      created_at
  ) 'Ekstrak Bulan Input',
  MONTH (created_at) 'Alternatif Ekstrak Bulan'
FROM
  produk;

UPDATE produk
SET
  category = 'Minuman'
WHERE
  id REGEXP '^P002[23]';

UPDATE produk
SET
  category = 'Lain-lain'
WHERE
  new_id REGEXP '2[14567]';

SELECT
  new_id,
  name,
  CASE category
    WHEN 'Makanan' THEN 'Awas Kaaarbo!!'
    WHEN 'Minuman' THEN 'Perbanyak Air putih'
    ELSE 'Jangan banyak2!!'
  END 'Peringatan Diet'
FROM
  produk;

SELECT
  new_id,
  name,
  price,
  IF (
    price <= 15000,
    'Murah',
    IF (price <= 19000, 'Sedang', 'Mahal')
  ) 'Estimasi Harga'
FROM
  produk;

SELECT
  name,
  description,
  IFNULL (description, 'Kosong') 'description'
FROM
  produk;

SELECT
  COUNT(new_id) 'total id',
  MIN(price) 'harga terendah',
  MAX(price) 'harga tertinggi',
  AVG(price) 'harga rerata',
  SUM(quantity) 'total stok'
FROM
  produk;

SELECT
  category,
  COUNT(new_id) 'total id',
  MIN(price) 'harga terendah',
  MAX(price) 'harga tertinggi',
  AVG(price) 'harga rerata',
  SUM(quantity) 'total stok'
FROM
  produk
GROUP BY
  category;

SELECT
  category,
  COUNT(new_id) total_id,
  MIN(price) harga_terendah,
  MAX(price) harga_tertinggi,
  AVG(price) harga_rerata,
  SUM(quantity) total_stok
FROM
  produk
GROUP BY
  category
HAVING
  total_stok > 700;

DESC wishlist;

SHOW
CREATE TABLE wishlist;

SHOW INDEX
FROM
  wishlist;

ALTER TABLE produk ADD UNIQUE KEY (name);

ALTER TABLE produk
DROP INDEX name;

ALTER TABLE produk ADD CONSTRAINT name UNIQUE (name, id);

ALTER TABLE produk
DROP CONSTRAINT name;

SELECT
  *
FROM
  produk;

DELETE FROM produk
WHERE
  new_id REGEXP '37';

ALTER TABLE produk ADD CONSTRAINT check_price CHECK (price >= 1000);

INSERT INTO
  produk (id, name, price, quantity)
VALUES
  ('P0027', 'Mie Ayam Orig', 350, 100);

UPDATE produk
SET
  price = 500
WHERE
  new_id = 25;

EXPLAIN
SELECT
  *
FROM
  produk
WHERE
  price > 10000
  AND category = 'Makanan';

SELECT
  COUNT(DISTINCT category)
FROM
  produk;

SELECT
  COUNT(category)
FROM
  produk;

SELECT
  (
    SELECT
      COUNT(DISTINCT category)
    FROM
      produk
  ) / (
    SELECT
      COUNT(category)
    FROM
      produk
  ) AS 'SV';

SELECT
  3 / 27 AS 'HASIL';

SHOW INDEX
FROM
  produk;

DESC produk;

ALTER TABLE produk ADD INDEX categ_pric (category, price);

SELECT
  id,
  name
FROM
  produk
WHERE
  category = 'Lain-lain'
ORDER BY
  price;

SELECT
  id,
  name
FROM
  produk
WHERE
  category = 'Lain-lain'
  and price >= 10000
ORDER BY
  price;

EXPLAIN
SELECT
  id,
  name
FROM
  produk
WHERE
  category = 'Lain-lain'
  and price >= 10000
ORDER BY
  price;

ALTER TABLE produk ADD FULLTEXT full (name, description);

SELECT
  *
FROM
  produk
WHERE
  MATCH (name, description) AGAINST ('mie' IN NATURAL LANGUAGE MODE);

SELECT
  *
FROM
  produk
WHERE
  MATCH (name, description) AGAINST ('mie -spesial' IN BOOLEAN MODE);

SELECT
  *
FROM
  produk
WHERE
  MATCH (name, description) AGAINST (
    'mie'
    WITH
      QUERY EXPANSION
  );

SELECT
  new_id,
  IFNULL (category, description)
FROM
  produk;

CREATE TABLE wishlist (
  id INT NOT NULL AUTO_INCREMENT,
  id_produk VARCHAR(10) NOT NULL,
  description TEXT,
  PRIMARY KEY (id),
  CONSTRAINT fk_wishlist_produk FOREIGN KEY (id_produk) REFERENCES produk (id)
) ENGINE = InnoDB;

SHOW TABLES;

DESC produk;

DESC wishlist;

SHOW INDEX
FROM
  produk;

SHOW
CREATE TABLE produk;

SHOW
CREATE TABLE wishlist;

ALTER TABLE wishlist
-- DROP CONSTRAINT fk_wishlist_produk;
ADD CONSTRAINT fk_wishlist_produk FOREIGN KEY (id_produk) REFERENCES produk (id) ON DELETE RESTRICT ON UPDATE CASCADE;

-- hati2 dalam menggunakan foreign key behavior
ALTER TABLE produk
-- DROP INDEX id;
ADD UNIQUE KEY (id);

SELECT
  *
FROM
  wishlist;

DROP TABLE wishlist;

ALTER TABLE wishlist
DROP CONSTRAINT fk_wishlist_produk,
DROP INDEX fk_wishlist_produk;

ALTER TABLE produk MODIFY COLUMN id_new VARCHAR(100) NOT NULL;

UPDATE produk
SET
  id_new = id;

ALTER TABLE produk MODIFY COLUMN id VARCHAR(10) NOT NULL AFTER new_id;

ALTER TABLE produk
DROP COLUMN id_new,
ADD COLUMN id_new VARCHAR(10) NOT NULL;

ALTER TABLE produk
DROP PRIMARY KEY,
ADD PRIMARY KEY (new_id, id);

SELECT
  id_new,
  COUNT(id_new)
FROM
  produk
GROUP BY
  id_new;

DESC wishlist;

ALTER TABLE wishlist
-- ADD UNIQUE KEY(id),
-- ADD COLUMN customer_id INT NOT NULL;
-- ADD COLUMN customer_id INT;
-- DROP CONSTRAINT fk_customer,
-- DROP INDEX fk_customer,
-- DROP COLUMN  customer_id;
ADD CONSTRAINT fk_customer FOREIGN KEY (customer_id) REFERENCES customers (id);

SHOW
CREATE TABLE customers;

SHOW
CREATE TABLE wishlist;

SHOW INDEX
FROM
  wishlist;

ATER TABLE customers
DROP INDEX id;

SELECT
  *
FROM
  wishlist;

UPDATE wishlist
SET
  description = 'Bugs hanya joke'
WHERE
  id_produk = 'P0027';

INSERT INTO
  wishlist (id_produk, description)
VALUES
  ('P0011', 'Kentang renyah digoreng manis');

DELETE FROM produk
WHERE
  id = 'P0011';

SELECT
  p.name,
  p.id,
  w.description
FROM
  wishlist w
  JOIN produk p ON (w.id_produk = p.id);

SELECT
  *
FROM
  wishlist w
  JOIN produk p ON (w.id_produk = p.id);

CREATE TABLE customers (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) UNIQUE KEY NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  last_name VARCHAR(100),
  PRIMARY KEY (id)
) ENGINE = InnoDB;

UPDATE wishlist
SET
  customer_id = 1
WHERE
  id = 5;

SELECT
  *
FROM
  customers;

SHOW
CREATE TABLE customers;

DESC customers;

INSERT INTO
  customers (email, first_name, last_name)
VALUES
  ('adam@gmail.com', 'galam', 'must'),
  ('ngawi@gmail.com', 'wusung', 'dadab');

SELECT
  *
FROM
  wishlist w
  JOIN customers c ON (w.customer_id = c.id);

SHOW DATABASES;

SHOW TABLES;

CREATE TABLE wallet (
  id INT NOT NULL AUTO_INCREMENT,
  customer_id INT NOT NULL,
  balance INT NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  UNIQUE KEY (customer_id),
  CONSTRAINT fk_wallet FOREIGN KEY (customer_id) REFERENCES customers (id)
) ENGINE = InnoDB;

-- RELASI SQL:
-- One2One (One reference to one foreign key)
-- One2Many (One reference to many foreign keys)
-- Many2Many (Many references to many foreign keys)
SHOW
CREATE TABLE wallet;

DESC wallet;

SELECT
  *
FROM
  wallet;

INSERT INTO
  wallet (customer_id, balance)
VALUES
  (1, 50000),
  (2, 46300);

SELECT
  *
FROM
  customers c
  JOIN wallet w ON (c.id = w.customer_id);

CREATE TABLE categories (
  id VARCHAR(10) NOT NULL,
  category VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

ALTER TABLE produk
RENAME COLUMN category TO old_category;

ALTER TABLE category
DROP INDEX id;

SHOW
CREATE TABLE produk;

SHOW INDEX
FROM
  produk;

SELECT
  *
FROM
  produk;

SHOW
CREATE TABLE categories;

SHOW INDEX
FROM
  category;

SELECT
  *
FROM
  category;

ALTER TABLE produk
-- DROP COLUMN category;
-- ADD COLUMN category VARCHAR(10);
ADD CONSTRAINT fk_categories -- Jangan lupa pake ADD.
FOREIGN KEY fk_categories (category) REFERENCES categories (id);

INSERT INTO
  categories (id, category)
VALUES
  ('C0001', 'bakso'),
  ('C0002', 'mie'),
  ('C0003', 'cemilan'),
  ('C0004', 'minuman');

SELECT
  *
FROM
  categories;

SELECT
  *
FROM
  produk;

UPDATE produk
SET
  category = 'C0001'
WHERE
  name REGEXP '^bakso';

UPDATE produk
SET
  category = 'C0002'
WHERE
  name REGEXP '^mie';

UPDATE produk
SET
  category = 'C0003'
WHERE
  id IN ('P0012', 'P0013', 'P0011');

UPDATE produk
SET
  category = 'C0004'
WHERE
  name REGEXP 'Es';

SELECT
  *
FROM
  produk p
  JOIN categories c ON (p.category = c.id);

-- bolak-balik tidak masalah
-- JOIN categories c ON(c.id=p.category);
SHOW TABLES;

CREATE TABLE orders (
  id INT NOT NULL AUTO_INCREMENT,
  total INT NOT NULL,
  order_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

DESC orders;

CREATE TABLE order_detail (
  id_produk VARCHAR(10) NOT NULL,
  id_order INT NOT NULL,
  price INT NOT NULL,
  quantity INT NOT NULL,
  PRIMARY KEY (id_produk, id_order)
) ENGINE = InnoDB;

DESC order_detail;

SHOW TABLES;

ALTER TABLE order_detail ADD CONSTRAINT CHK_order_detail_produk FOREIGN KEY (id_produk) REFERENCES produk (id);

ALTER TABLE order_detail ADD CONSTRAINT CHK_order_detail_orders FOREIGN KEY (id_order) REFERENCES orders (id);

SHOW
CREATE TABLE order_detail;

SELECT
  *
FROM
  order_detail;

INSERT INTO
  orders (total)
VALUES
  (50000);

INSERT INTO
  order_detail (id_produk, id_order, price, quantity)
VALUES
  ('P0004', 1, 25000, 1),
  ('P0002', 1, 15000, 1),
  ('P0006', 2, 25000, 1),
  ('P0002', 2, 15000, 1),
  ('P0004', 3, 10000, 1),
  ('P0005', 3, 15000, 1);

SELECT
  *
FROM
  order_detail od
  JOIN orders o ON (o.id = od.id_order)
  JOIN produk p ON (p.id = od.id_produk);

SELECT
  o.id,
  p.id,
  od.quantity,
  p.price,
  p.name
FROM
  order_detail od
  JOIN orders o ON (o.id = od.id_order)
  JOIN produk p ON (p.id = od.id_produk);

SELECT
  *
FROM
  categories;

INSERT INTO
  categories (id, category)
VALUES
  ('C0005', 'lakas'),
  ('C0006', 'asal');

SELECT
  *
FROM
  produk;

INSERT INTO
  produk (id, name, price, quantity)
VALUES
  ('X0001', 'Mie Ayam Xriginal', 15000, 100),
  ('X0002', 'BaksX Jumbo', 22000, 100),
  ('X0003', 'BaksX Spesial', 25000, 100);

SELECT
  p.id,
  c.id,
  c.category,
  p.name
FROM
  produk p
  JOIN categories c ON (p.category = c.id);

SELECT
  p.id,
  c.id,
  c.category,
  p.name
FROM
  produk p
  INNER JOIN categories c ON (p.category = c.id);

SELECT
  p.id,
  c.id,
  c.category,
  p.name
FROM
  produk p
  LEFT JOIN categories c ON (c.id = p.category);

SELECT
  p.id,
  c.id,
  c.category,
  p.name
FROM
  produk p
  RIGHT JOIN categories c ON (p.category = c.id);

SELECT
  p.id,
  c.id,
  c.category,
  p.name
FROM
  produk p
  CROSS JOIN categories c;

CREATE TABLE numberers (id INT NOT NULL, PRIMARY KEY (id)) ENGINE = InnoDB;

INSERT INTO
  numberers (id)
VALUES
  (1),
  (2),
  (3),
  (4),
  (5),
  (6),
  (7),
  (8),
  (9),
  (10);

SELECT
  *,
  (a.id * b.id) hasil
FROM
  numberers a
  CROSS JOIN numberers b
ORDER BY
  a.id,
  b.id ASC;

SHOW
CREATE TABLE numberers;

SELECT
  *
FROM
  produk
WHERE
  produk.price > (
    SELECT
      AVG(price)
    FROM
      produk
  );

SELECT
  MAX(price)
FROM
  produk;

SELECT
  *
FROM
  produk
ORDER BY
  (price) DESC;

UPDATE produk
SET
  price = 30000
WHERE
  id = 'X0003';

SELECT
  MAX(price)
FROM
  (
    SELECT
      price
    FROM
      categories c
      INNER JOIN produk p ON (c.id = p.category)
  ) harga_kategori;

CREATE TABLE guestbooks (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) NOT NULL,
  title VARCHAR(200) NOT NULL,
  content TEXT,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

DESC guestbooks;

SELECT
  *
FROM
  guestbooks;

INSERT INTO
  guestbooks (email, title, content)
VALUES
  ('adam@gmail.com', 'hello', 'hello'),
  ('adam@gmail.com', 'hello', 'hello'),
  ('adam@gmail.com', 'hello', 'hello');

SELECT DISTINCT
  email
FROM
  customers
UNION ALL
SELECT DISTINCT
  email
FROM
  guestbooks;

SELECT
  email,
  COUNT(email)
FROM
  (
    SELECT
      email
    FROM
      customers
    UNION ALL
    SELECT
      email
    FROM
      guestbooks
  ) all_mail
GROUP BY
  email;

SELECT DISTINCT
  email
FROM
  customers
WHERE
  email IN (
    SELECT DISTINCT
      email
    FROM
      guestbooks
  );

SELECT DISTINCT
  c.email
FROM
  customers c
  INNER JOIN guestbooks g ON (g.email = c.email);

SELECT DISTINCT c.email,g.email
  FROM customers c
  LEFT	JOIN guestbooks g ON(g.email=c.email)
  WHERE g.email IS NULL
;

START TRANSACTION;

INSERT INTO guestbooks(email,title,content)
  VALUES
    ('contoh4@gmail.com','contoh','contoh'),
    ('contoh5@gmail.com','contoh','contoh'),
    ('contoh6@gmail.com','contoh','contoh')
;

SELECT *
  FROM guestbooks
;

COMMENT;

ROLLBACK;

UPDATE guestbooks
  SET title='go home'
  WHERE id=17
;

LOCK TABLE produk READ;

SELECT *
  FROM produk
  WHERE id='P0002'
;

UPDATE produk
  SET quantity=quantity-50
  WHERE id='P0002'
;
