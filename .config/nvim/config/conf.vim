 " vim: ft=vim ts=2 sts=2 sw=2 et
 "=============================== Float Term =================================
 let g:floaterm_keymap_toggle = 'tt'
 let g:floaterm_opener = 'drop'
 let g:floaterm_width = 0.95
 let g:floaterm_height = 0.7
 let g:floaterm_wintype = 'float'
 let g:floaterm_autohide = 1
 let g:floaterm_autoclose = 0
 let g:fzf_floaterm_newentries = {
     \ '+root' : {
       \ 'title': 'Root Shell',
       \ 'cmd': 'sudo sh' },
     \ '+fish' : {
       \ 'title': 'Fish Shell',
       \ 'cmd': 'fish' },
     \ '+pwsh' : {
       \ 'title': 'Powershell',
       \ 'cmd': 'pwsh' }
     \ }
 let g:NERDTreeHijackNetrw = 0
 let g:lf_replace_netrw = 1
 let g:lf_map_keys = 0
 let g:lf_open_new_tab = 1

 "================================ sql-language-server =================================
" let g:LanguageClient_serverCommands = {
"     \ 'sql': ['sql-language-server', 'up', '--method', 'stdio'],
"     \ }

" 'nametag': 'typeDB://ipDB:portDB/nameDB?user=DBowner&password=passparaseDB'
" OR
" 'nametag': 'to/file/database?user=DBowner&password=passparaseDB'

" \  'dev': 'mysql://localhost:3306/test_db?user=sandun&password=@damevA5758'
let g:dbs = {
\  'dev': 'mysql://localhost:3306/mysql?user=sandun&password=@damevA5758'
\ }

let g:dadbod#ui#buffer#treesitter_highlight = 'sql'


 "================================ Ultisnip =================================
" let g:UltiSnipsExpandTrigger="<C-Tab>"
" let g:UltiSnipsExpandOrJumpTrigger = "<tab>"
" let g:UltiSnipsListSnippets ="<Tab>"
" let g:UltiSnipsJumpForwardTrigger="<Tab>"
" let g:UltiSnipsJumpBackwardTrigger="<c-z>"
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

 "================================ CLOSTAG =================================
 " These are the file extensions where clostag plugin is enabled.
 let g:closetag_filenames = '*.html,*.xhtml,*.phtml'
 " These are the file types where closetag plugin is enabled.
 let g:closetag_filetypes = 'html,xhtml,phtml'
 " This will make the list of non-closing tags case-sensitive (e.g. `<Link>` will be closed while `<link>` won't.)
 let g:closetag_emptyTags_caseSensitive = 1
 " Disables auto-close if not in a "valid" region (based on filetype)
 let g:closetag_regions = {
     \ 'typescript.tsx': 'jsxRegion,tsxRegion',
     \ 'javascript.jsx': 'jsxRegion',
     \ 'typescriptreact': 'jsxRegion,tsxRegion',
     \ 'javascriptreact': 'jsxRegion',
     \}
 " Shortcut for closing tags, default is '>'
 let g:closetag_shortcut = '>'

 "============================== LimeLight ===================================
 let g:limelight_default_coefficient = 0.1
 let g:limelight_conceal_guifg = '#665766'
 let g:limelight_paragraph_span = 0
 let g:limelight_priority = -10

 "================================= EMMET ==================================
 let g:user_emmet_leader_key=','

 "=============================== SUDA VIM =================================
 let g:suda_smart_edit = 1

 "============================== Tagalong ====================================
 let g:tagalong_verbose = 1

 "=========================== Tmux Integrator =================================
 let g:tmux_navigator_no_mappings = 0

"============================== Table Mode ===================================
let g:table_mode_update_time = 4000
let g:table_mode_corner='+'
let g:table_mode_corner_corner='|'
let g:table_mode_header_fillchar='='
" function! s:isAtStartOfLine(mapping)
"   let text_before_cursor = getline('.')[0 : col('.')-1]
"   let mapping_pattern = '\V' . escape(a:mapping, '\')
"   let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
"   return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
" endfunction
"
" inoreabbrev <expr> <bar><bar>
"           \ <SID>isAtStartOfLine('\|\|') ?
"           \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
" inoreabbrev <expr> __
"           \ <SID>isAtStartOfLine('__') ?
"           \ '<c-o>:silent! TableModeDisable<cr>' : '__'
"

"================================ Great Thesaury =================================
let g:tq_map_keys=0
let g:tq_openoffice_en_file="~/wiki/thesaury/MyThes-1.0/th_en_US_new[.dat,.idx]"
let g:tq_mthesaur_file="~/wiki/thesaury/mthesaur.txt"
let g:tq_cilin_txt_file="~/wiki/thesaury/cilin.txt"
let g:tq_language=['en', 'ru', 'de', 'cn']
" let g:tq_online_backends_timeout = 0.4
let g:tq_enabled_backends=[
			\"openoffice_en",
			\"mthesaur_txt",
			\"cilin_txt",
            \]
   "          \"yarn_synsets",
   "          \"datamuse_com",
			" \"openthesaurus_de",

"============================== Vim Markdown ==================================
let g:vim_markdown_folding_style_pythonic = 1
let g:vim_markdown_override_foldtext = 0
let g:vim_markdown_no_default_key_mappings = 1
let g:vim_markdown_toc_autofit = 1
let g:vim_markdown_emphasis_multiline = 0
let g:tex_conceal = ""
let g:vim_markdown_math = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_toml_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_no_extensions_in_markdown = 1
let g:vim_markdown_autowrite = 1
let g:vim_markdown_new_list_item_indent = 0

"============================== NVIM Path ====================================
if has('nvim')
"============================== Host Program ==================================
let g:python3_host_prog = '/usr/bin/python3'
" let g:node_host_prog = '/usr/local/bin/neovim-node-host'
" let g:loaded_node_provider = 0
let g:ruby_host_prog = '/usr/bin/ruby3.1'
let g:perl_host_prog = '/usr/bin/perl'

"============================== Fix Noice Cursor ==================================
hi NoiceCursor guibg=#665252

 finish
endif

"============================== VIM Path ====================================
 "=============================== COC ======================================
 " DIAGNOSTICS
 let g:coc_filetype_map = {'pandoc': 'markdown'}
 let g:coc_global_extensions = [
    \ 'coc-lists',
    \ 'coc-snippets',
    \ 'coc-pairs',
    \ 'coc-prettier',
    \ 'coc-marketplace',
    \ 'coc-diagnostic',
    \ 'coc-gist',
    \ 'coc-eslint',
    \ 'coc-stylelintplus',
    \ 'coc-stylelint',
    \ 'coc-htmlhint',
    \ 'coc-html-css-support',
    \ 'coc-markdownlint',
    \ 'coc-rust-analyzer',
    \ 'coc-sh',
    \ 'coc-sql',
    \ 'coc-toml',
    \ 'coc-xml',
    \ 'coc-yaml',
    \ 'coc-tsserver',
    \ 'coc-html',
    \ 'coc-css',
    \ 'coc-json',
    \ 'coc-vimtex',
    \ 'coc-gocode',
    \ 'coc-spell-checker',
    \]

 " Use`:prettier` to format patterns
 command! -nargs=0 Prettier :CocCommand prettier.forceFormatDocument
 " Use `:Format` to format current buffer
 command! -nargs=0 Format :call CocAction('format')
 " Use `:Fold` to fold current buffer
 command! -nargs=? Fold :call CocAction('fold', <f-args>)
 " use `:OR` for organize import of current buffer
 command! -nargs=0 OR   :call CocAction('runCommand', 'editor.action.organizeImport')
 " Add status line support, for integration with other plugin, checkout `:h coc-status`
 " set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

 "=========================== AIRLINE/LIGHTLINE ==============================
 " Code-Dark-Theme
 let g:airline_theme = 'codedark'
 let g:lightline#bufferline#enable_devicons  = 1
 let g:lightline#bufferline#min_buffer_count = 2
 let g:lightline#bufferline#show_number      = 1
 let g:lightline#bufferline#unicode_symbols  = 1
 let g:lightline#trailing_whitespace#indicator = '•'
 let g:lightline#ale#indicator_checking = "\uf110 "
 let g:lightline#ale#indicator_infos = "\uf129 "
 let g:lightline#ale#indicator_warnings = "\uf071 "
 let g:lightline#ale#indicator_errors = "\uf05e "
 let g:lightline#ale#indicator_ok = "\uf00c "

 let g:lightline = {
      \ 'mode_map': {
      \   'n' : 'N',
      \   'i' : 'I',
      \   '(i)' : 'IN',
      \   'R' : 'R',
      \   'v' : 'V',
      \   'V' : 'VL',
      \   "\<C-v>": 'VB',
      \   'c' : 'C',
      \   's' : 'S',
      \   'S' : 'SL',
      \   "\<C-s>": 'SB',
      \   't': 'T',
      \ },
      \ 'active': {
      \   'left': [['mode', 'paste'],
      \            ['filename'],
      \            ['filetype', 'fileformat', 'fileencoding']],
      \   'right': [['linter_checking', 'linter_errors', 'linter_warnings', 'linter_ok', 'trailing', 'percent', 'lineinfo'],
      \             ['readonly', 'githunks'],
      \             ['method', 'coc']]
      \ },
      \ 'tabline': {
      \   'left': [['buffers']],
      \   'right': [['close']]
      \ },
      \ 'component_type': {
      \   'linter_checking': 'left',
      \   'linter_warnings': 'warning',
      \   'linter_errors': 'error',
      \   'linter_ok': 'left',
      \   'trailing': 'error',
      \   'buffers': 'tabsel'
      \ },
      \ 'component_expand': {
      \   'linter_checking': 'lightline#ale#checking',
      \   'linter_errors': 'lightline#ale#errors',
      \   'linter_warnings': 'lightline#ale#warnings',
      \   'linter_ok': 'lightline#ale#ok',
      \   'trailing': 'lightline#trailing_whitespace#component',
      \   'buffers': 'lightline#bufferline#buffers'
      \ },
      \ 'component_function': {
      \   'filename': 'LightlineFilename',
      \   'githunks': 'LightlineGitGutter',
      \   'method': 'coc#status#get',
      \   'linter': 'LinterStatus',
      \   'coc': 'coc#status',
      \ }}

 "============================== Diary ===================================
 let g:diary_diary='$HOME/wiki/diary'
 let g:diary_no_mappings=0
 let g:waikiki_default_maps=0
 let g:wiki_root = '$HOME/wiki'
 let g:waikiki_index = 'favorite.md'
 let g:zettel_root = $HOME.'/.cache/tags'
 let g:zettel_filetype = ['markdown']
 let g:zettel_taglink_prefix ="./"
 let g:calendar_diary=$HOME.'/wiki/diary'
 let g:calendar_no_mappings=0
 let g:calendar_diary_list = [
    \   {'name': 'Note', 'path': $HOME.'/wiki', 'ext': '.adoc'},
    \   {'name': 'Diary', 'path': $HOME.'/wiki/diary', 'ext': '.diary.adoc'},
    \ ]
 let g:zettel_extension = ".md"
 let g:zettel_root = "~/wiki"

 "=========================== Asynchronous Lint Engine =======================
 " Display detail of the error
  let g:ale_cursor_detail = 1
  let g:ale_sign_column_always = 1
  let g:ale_warn_about_trailing_blank_lines = 1
  let g:ale_warn_about_trailing_whitespace = 1
  let g:ale_sign_priority = 10
  let g:airline#extensions#ale#enabled = 1
 " with :CocConfig and add `"diagnostic.displayByAle": true`
  let g:ale_disable_lsp = 1
 " Fix new-line characters in code-diagnostic messages from CoC
  let g:ale_other_source_text_line_separator = "    "
 " Allow only explicitly setup linters
  let g:ale_linters_explicit = 0
 " When to run linters
  let g:ale_lint_on_enter = 1
  let g:ale_lint_on_text_changed = 0
  let g:ale_lint_on_insert_leave = 1
  let g:ale_lint_on_save = 1
  let g:ale_lint_on_filetype_changed = 1
 " Allow fixing files when they are saved
  let g:ale_fix_on_save = 1
  let g:ale_fixers = {
     \  '*': ['remove_trailing_lines', 'trim_whitespace'],
     \ 'javascript': ['eslint'],
     \}
 " Nice linter processes
 "let g:ale_command_wrapper = 'nice -n19 ionice -c3'
 " Error highlighting
  let g:ale_set_highlights = 1
  let g:ale_set_signs = 1
  let g:ale_sign_highlight_linenrs = 0
  let g:ale_sign_error = " "
  let g:ale_sign_warning = " "
  let g:ale_sign_info = " "
  let g:ale_sign_style_error = " "
  let g:ale_sign_style_warning = " "
  hi ALEErrorSign ctermbg=NONE ctermfg=red
  hi ALEWarningSign ctermbg=NONE ctermfg=yellow
 " Format for linter messages
  let g:ale_echo_msg_error_str = "err"
  let g:ale_echo_msg_warning_str = "warn"
  let g:ale_echo_msg_info_str = "info"
  let g:ale_echo_msg_log_str = "log"
  let g:ale_echo_msg_format = "%linter% [%severity%(%code%)]: %s"
  let g:ale_lsp_show_message_format = g:ale_echo_msg_format
 " Display information in floating windows
  let g:ale_open_list = 1
  let g:ale_keep_list_window_open = 0
  let g:ale_set_balloons = 1
  let g:ale_hover_cursor = 1
  let g:ale_floating_preview = 1
  function! CustomOpts() abort
    let [l:info, l:loc] = ale#util#FindItemAtCursor(bufnr(''))
    return {'title': ' ALE: ' . (l:loc.linter_name) . ' '}
  endfunction

  let g:ale_floating_preview_popup_opts = 'g:CustomOpts'
  let g:ale_floating_window_border = ['│', '─', '╭', '╮', '╯', '╰', '│', '─']
  let g:ale_virtualtext_cursor = 0
 " In ~/.vim/vimrc, or somewhere similar.
  let g:ale_linter_aliases = {'jsx': ['css', 'javascript'], 'vue': ['vue', 'javascript']}
  let g:ale_linters = {'jsx': ['stylelint', 'eslint'], 'vue': ['eslint', 'vls']}

 "=============================== HEXOKINESE ===============================
 let g:Hexokinase_refreshEvents = ['InsertLeave']
 " Hexokinese possible highlighters :Virtual, sign_column, Background, backgroundfull, foreground, foregroundfull.
 let g:Hexokinase_highlighters = ['backgroundfull']

 " Patterns to match for all filetypes
 " Can be a comma separated string or a list of strings
 let g:Hexokinase_optInPatterns = [
     \'full_hex',
     \'triple_hex',
     \'rgb',
     \'rgba',
     \'hsl',
     \'hsla',
     \'colour_names'
     \]

 " Filetype specific patterns to match
 " entry value must be comma seperated list
 let g:Hexokinase_ftOptInPatterns = {
     \'css': 'full_hex,rgb,rgba,hsl,hsla,colour_names',
     \'html': 'full_hex,rgb,rgba,hsl,hsla,colour_names'
     \}

 " Sample value, to keep default behavior don't define this variable
 let g:Hexokinase_ftEnabled = ['css', 'html', 'javascript']

 "============================== GIT GUTTER ================================
 hi GitGutterAdd guifg=#FF5F00 guibg=NONE
 hi GitGutterChange guifg=#bbbb00 guibg=NONE
 hi GitGutterDelete guifg=#ff2222 guibg=NONE
 let g:gitgutter_enabled = 1
 let g:gitgutter_map_keys = 0
 let g:gitgutter_highlight_linenrs = 1
 let g:gitgutter_realtime = 1
 let g:gitgutter_eager = 1
 let g:gitgutter_max_signs = 500
 let g:gitgutter_preview_win_floating = 1

 "============================== Diary ===================================

 let g:gutentags_project_root = ['.root', 'package.json', '.git']
 let g:gutentags_add_default_project_roots = 0
 let g:gutentags_define_advanced_commands = 1
 let g:gutentags_cache_dir = expand('$HOME/.cache/tags')
 let g:gutentags_plus_switch = 1
 let g:gutentags_generate_on_new = 1
 let g:gutentags_generate_on_missing = 1
 let g:gutentags_generate_on_write = 1
 let g:gutentags_generate_on_empty_buffer = 0
 let g:gutentags_ctags_extra_args = [
      \ '--tag-relative=yes',
      \ '--fields=+ailmnS',
      \ ]
let g:gutentags_ctags_exclude = [
      \ '.git', '.svg', '*.hg',
      \ '/tests/',
      \ '~/prefix32',
      \ '~/Documents',
      \ '~/Downloads',
      \ '~/PlayOnLinu.*',
      \ 'build',
      \ 'dist',
      \ 'sites//files/*',
      \ 'bin',
      \ 'node_modules',
      \ 'bower_components',
      \ 'cache',
      \ 'compiled',
      \ 'docs',
      \ 'example',
      \ 'bundle',
      \ 'vendor',
      \ '*.md',
      \ '*-lock.json',
      \ '*.lock',
      \ 'bundle.js',
      \ 'build.js',
      \ '.rc',
      \ '*.json',
      \ '.min.',
      \ '*.map',
      \ '*.bak',
      \ '*.zip',
      \ '*.pyc',
      \ '*.class',
      \ '*.sln',
      \ '*.Master',
      \ '*.csproj',
      \ '*.tmp',
      \ '*.csproj.user',
      \ '*.cache',
      \ '*.pdb',
      \ 'tags*',
      \ 'cscope.*',
      \ '*.css',
      \ '*.less',
      \ '*.scss',
      \ '.exe', '.dll',
      \ '.mp3', '.ogg', '*.flac',
      \ '.swp', '.swo',
      \ '.bmp', '.gif', '.ico', '.jpg', '*.png',
      \ '.rar', '.zip', '.tar', '.tar.gz', '.tar.xz', '.tar.bz2',
      \ '.pdf', '.doc', '.docx', '.ppt', '*.pptx',
      \ ]
 command! -nargs=0 GutentagsClearCache call system('rm ' . g:gutentags_cache_dir . '/*')

 "============================== Vim-Test ===================================
 let test#strategy = "neovim"
 let test#neovim#term_position = "vertical"
 let test#enabled_runners = ["javascript#jest"]
 let g:test#javascript#runner = 'jest'
 let g:test#neovim#start_normal = 1
 let g:test#preserve_screen = 1

 "============================== VISUAL MULTI ==============================
  let g:VM_maps = {}
  let g:VM_maps["Add Cursor Down"] = '<C-j>'
  let g:VM_maps["Add Cursor Up"] = '<C-k>'

" ============================ Asyncrun ====================================
 let g:SprintForceRun = 0
 let g:SprintHidden = 1
 let g:asyncrun_open =10

"====================================== Quickpeek ===========================
let g:quickpeek_auto = v:true

 "=============================== ColorV =================================
 let g:colorv_preview_ftype = ''

 "=============================== Polyglot =================================
 let g:highlight_space_errors = 0

 let g:Startscreen_function = 'fishies'

 "============================== Vim Markdown ==================================
let g:vim_markdown_folding_style_pythonic = 1
let g:vim_markdown_override_foldtext = 0
let g:vim_markdown_no_default_key_mappings = 1
let g:vim_markdown_toc_autofit = 1
let g:vim_markdown_emphasis_multiline = 0
let g:tex_conceal = ""
let g:vim_markdown_math = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_toml_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_no_extensions_in_markdown = 1
let g:vim_markdown_autowrite = 1
let g:vim_markdown_new_list_item_indent = 0
