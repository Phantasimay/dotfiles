" vim: ft=vim ts=2 sts=2 sw=2 et
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"    __      __         _ _ _    __      ___
"    \ \    / /        (_) | |   \ \    / (_)
"     \ \  / /_ _ _ __  _| | | __ \ \  / / _ _ __ ___
"      \ \/ / _` | '_ \| | | |/ _` \ \/ / | | '_ ` _ \
"       \  / (_| | | | | | | | (_| |\  /  | | | | | | |
"        \/ \__,_|_| |_|_|_|_|\__,_| \/   |_|_| |_| |_|
"
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"   	     __          ___ _             _
" 		  /\ \ \___     / _ \ |_   _  __ _(_)_ __  ___
" 		 /  \/ / _ \   / /_)/ | | | |/ _` | | '_ \/ __|
"		/ /\  / (_) | / ___/| | |_| | (_| | | | | \__ \
"		\_\ \/ \___/  \/    |_|\__,_|\__, |_|_| |_|___/
"                                    |___/
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"================================ BASIC SETTING ================================
"-----------------------------GVIM - GUI VERSION--------------------------------

	if has('gui_running')

    	" Set the color scheme.
    		color slate

		" Font
			if has("macvim")
    			set guifont=Menlo\ Regular:h14
			elseif has("win32")
				set guifont="Consolas 14"
			else
				set guifont=Consolas\ 18
			endif

		" Hide the toolbar.
			set guioptions-=T

    	" Hide the right-side scroll bar.
    		set guioptions-=r

		" Start Lex Tree and put the cursor back in the other window.
			autocmd VimEnter * :Lexplore | wincmd p

	endif

 if !has('nvim') " =====[[VIM ONLY CONF]] =====================================================
" Disable compatibility with vi which can cause unexpected issues.
  set nocompatible
  set ttymouse=xterm
  set mouse=vi
  set history=10000
  set fillchars+=vert:\│
  set shortmess+=cF
  set shortmess-=S
  set tags="./tags;,tags"
  syntax enable
  filetype plugin indent on
  set smarttab nojoinspaces incsearch hlsearch autoread hidden autoindent
  set ttyfast lazyredraw
  set nosc noru nosm

" Don't load GUI menus; set here before GUI starts
  if has('gui_running')
    set guioptions+=M
    set guiheadroom=0
  endif
" Disable the vim bell
  set visualbell

" Fixes common keyboard problem
  set backspace=indent,eol,start

"Some servers have issues with backup files
  set noswapfile nobackup nowritebackup

" Use UTF-8 if we can and env LANG didn't tell us not to
  if has('multi_byte') && !exists('$LANG') && &encoding ==# 'latin1'
    scriptencoding utf-8
    set encoding=utf-8
  endif

" Treat numbers with a leading zero as decimal, not octal
  set nrformats-=octal

" Make sessions usable
  if exists('+sessionoptions')
    set sessionoptions-=localoptions,options  " No buffer options or mappings
    set sessionoptions+=unix,slash
  endif

" Time out on key codes but not mappings.
" Basically this makes terminal Vim work sanely.
  set notimeout
  set updatetime=5000
  set ttimeout
  set ttimeoutlen=10
  set timeoutlen=500  "default 1000 ms
  syntax sync minlines=256
  set synmaxcol=300
  set re=1

  set wildmenu
  set wildoptions=pum

  set dictionary=/usr/share/dict/words
  set complete+=k

  set foldnestmax=1
  set foldmethod=manual
  set foldlevel=1
" set pastetoggle=<F2>

 else " =====[[NVIM ONLY CONF]] ======================================================
  set mouse=a
  set smoothscroll
  set foldnestmax=4
  set foldlevel=1
  set foldcolumn=1
  set foldexpr=nvim_treesitter#foldexpr()
  set foldmethod=expr
 endif " =====[[END CONF]] ======================================================

" If available, use GNU grep niceties for searching
 if system('grep --version') =~# '^grep (GNU grep)'
   set grepprg=rg\ --vimgrep
   set grepformat^=%f:%l:%c:%m
 endif

 set scrolljump=1
 set formatprg=fmt\ -w\ 80
 set wildcharm=<C-z>

 set list listchars=tab:\|\ ,eol:↲,nbsp:⣿,trail:•,extends:⟩,precedes:⟨
 set confirm

 set nowrap wrapscan
 set whichwrap=b,s,<,>,[,]
 set noshowmode showmatch
 set matchpairs+=<:>
 set scrolloff=999
 set sidescrolloff=999
 set showtabline=0
 set mmp=15000
 " sign unplace *

 set syn=auto
 let &fcs='eob: '
 set showbreak= "\ "

 set foldenable
 set vop+=folds,unix,slash
 set vop-=options
 set foldclose=all

 set tabstop=2
 set shiftwidth=2
 set softtabstop=2
 set expandtab noshiftround
 set breakindent smartindent copyindent
 set smartcase ignorecase

 set bh=wipe
 set t_vb=
 set nofixendofline
 set cinoptions+=:0
 set icon
 set secure exrc
 set ruf=%30(%=%#LineNr#%.50F\ [%{strlen(&ft)?&ft:'none'}]\ %l:%c\ %p%%%)
 let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
 let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
 " don't give |ins-completion-menu| messages.
 set shortmess=aoOtI
 set conceallevel=2


 "RWXROB flavour (:h fo-table)
 set formatoptions-=trowan2vbB
 set formatoptions+=1Mmljqc

 " Only turn on 'cursorline' if my colorscheme loaded
 if exists('+cursorline')
    set cursorline cursorcolumn
    set cmdheight=1
    set number relativenumber
    if has('vim')
    	set ruler showcmd
      set laststatus=2
      set showcmdloc=tabline
    else
      set noruler noshowcmd
      set laststatus=0
 endif
 set signcolumn=number
 set termguicolors
 hi lineNr guifg=#CDF910 guibg=NONE
 hi link CursorLineine CursorColumn
 hi clear CursorLine
 hi SpecialKey guifg=#CDF910 guibg=NONE
 hi CursorLine guibg=#2b2b2b cterm=NONE
 hi cursorlineNr guibg=#CDF910 guifg=#002400 cterm=bold
 hi Folded guifg=#828482 guibg=NONE
 hi cursorlinefold guifg=#ff2222 guibg=NONE
 hi cursorlinesign guifg=#ff2222 guibg=NONE
 hi statusline guifg=#77ff77 guibg=NONE
 hi signcolumn guibg=#000000
 hi Pmenu guibg=#2b2b2b
 hi Pmenusel guibg=#665252 guifg=#ffffff
 hi NonText guifg=#C90000
 hi VertSplit cterm=NONE
 hi tkLink ctermfg=Blue cterm=bold,underline guifg=blue gui=bold,underline
 hi tkBrackets ctermfg=gray guifg=gray

"------------------STATUS_LINE------------------
" Status line
 set showcmdloc=tabline
 set statusline+=%3*[%{StatuslineMode()}]%{SpellCheckStatus()}
 set statusline+=%2*\|%c\|%l
 set statusline+=%1*%S
 set statusline+=%h%m
 set statusline+=%=
 set statusline+=%f
 set statusline+=%=
 set statusline+=%h%r
 set statusline+=%1*%y
 set statusline+=%5*\ %P/%L
 set statusline+=%2*\|t:%n\|

" Colors
 hi User1 ctermbg=brown ctermfg=white guibg=black guifg=white
 hi User2 ctermbg=lightgreen ctermfg=black guibg=lightgreen guifg=black
 hi User3 ctermbg=brown  ctermfg=lightcyan guibg=maroon guifg=yellow
 hi User4 ctermbg=brown ctermfg=green guibg=black guifg=lightgreen
 hi User5 ctermbg=brown ctermfg=green guibg=gray guifg=black

" Mode
 function! StatuslineMode()
   let l:mode=mode()
   if l:mode==#"n"
     return "NORMAL"
   elseif l:mode==#"V"
     return "VISUAL LINE"
   elseif l:mode==?"v"
     return "VISUAL"
   elseif l:mode==#"i"
     return "INSERT"
   elseif l:mode ==# "\<C-V>"
 	return "V-BLOCK"
   elseif l:mode==#"R"
     return "REPLACE"
   elseif l:mode==?"s"
     return "SELECT"
   elseif l:mode==#"t"
     return "TERMINAL"
   elseif l:mode==#"c"
     return "COMMAND"
   elseif l:mode==#"!"
     return "SHELL"
   else
 	  return "VIM"
   endif
 endfunction

 " Spell Check Status
	function! SpellCheckStatus()
		if &spell
			return " [SPELL]"
		else
			return ''
		endif
	endfunction
 endif

"============================ FIXED COMMAND ===============================

" enable omni-completion
 set omnifunc=syntaxcomplete#Complete

" Don't assume I'm editing C; let the filetype set this
 set include=

" Break lines at word boundaries
 set linebreak

" Don't allow setting options via buffer content
 set nomodeline

" Options for file search with gf/:find
  set path-=/usr/include  " Let the C/C++ filetypes set that
  set path+=**            " Search current directory's whole tree

" Keep undo files, hopefully in a dedicated directory
 if has('persistent_undo')
   set undofile
   set undodir^=$XDG_STATE_HOME/nvim/undo//
   set undoreload=10000
   if has('win32') || has('win64')
     set undodir-=$XDG_STATE_HOME/nvim/undo/
     set undodir^=~/vimfiles/cache/undo/
     set undoreload=10000
   endif
 endif

" Let me move beyond buffer text in visual block mode
 if exists('+virtualedit')
   set virtualedit+=block
   set virtualedit=onemore
 endif

" Better Completion
 if exists('+completeopt')
	set complete=.,w,b,u,t
	set completeopt=menu,menuone,noinsert,noselect
	set wildignore+=**/node_modules/**
	set wildignore+=.hg,.git,.svn                    " Version control
	set wildignore+=*.aux,*.out,*.toc                " LaTeX intermediate files
	set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg   " binary images
	set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest " compiled object files
	set wildignore+=*.spl                            " compiled spelling word lists
	set wildignore+=*.sw?                            " Vim swap files
	set wildignore+=*.DS_Store                       " OSX bullshit
	set wildignore+=*.luac                           " Lua byte code
	set wildignore+=migrations                       " Django migrations
	set wildignore+=go/pkg                           " Go static files
	set wildignore+=go/bin                           " Go bin files
	set wildignore+=go/bin-vagrant                   " Go bin-vagrant files
	set wildignore+=*.pyc                            " Python byte code
	set wildignore+=*.orig                           " Merge resolution files
	set wildignore+=*.docx,*.pdf,*.flv,*.img,*.xlsx	 " MS Document file
 endif

 " disable spellcapcheck
 set spc=

 "Fix enval tiny vim
 if has("eval")
  let skip_defaults_vim = 1
endif

 "mark trailing spaces as errors
 match IncSearch '\s\+$'

 "Autotrue colors
 if !has('gui_running')
   set t_Co=256
 endif

 " Integrate with system clipboard (not on macOS due to problems)
  if has('unix') && !has('mac')
      set clipboard=unnamedplus,unnamed
  endif

 " Fix Maping Alt/Meta
 for i in range(97,122)
     let c = nr2char(i)
     exec "map \e".c." <M-".c.">"
     exec "map! \e".c." <M-".c.">"
 endfor

 " Fix splitting
 set splitbelow splitright

 " Store info from no more than 100 files at a time, 9999 lines of text, 100kb of data. Useful for copying large amounts of data between files.
 set viminfo=%,<9999,'100,/100,:100,!,s100,h,f0,n~/.vim/cache/.viminfo
 "           | |     |    |    |    | |    | |  + viminfo file path
 "           | |     |    |    |    | |    | + file marks 0-9,A-Z 0=NOT stored
 "           | |     |    |    |    | |    + disable 'hlsearch' loading viminfo
 "           | |     |	  |    |    | + registers >100 KB of text are skipped.
 "           | |     |	  |    |    + include global variables.
 "           | |     |    |    + command-line history saved
 "           | |     |    + search history saved
 "           | |     + files marks saved
 "           | + lines saved each register (old name for <, vi6.2)
 "           + save/restore buffer list

"   Create the 'Tag' file
 command! MakeTag !ctags -R .
"  - Use ^] to jump to tag under cursor
"  - Use g^] for ambiguous tags
"  - Use ^t to jump back up the tag stack

"   reset the cursor on start (for older versions of vim, usually not required)
"  Ps = 0  -> blinking block.
"  Ps = 1  -> blinking block (default).
"  Ps = 2  -> steady block.
"  Ps = 3  -> blinking underline.
"  Ps = 4  -> steady underline.
"  Ps = 5  -> blinking bar (xterm).
"  Ps = 6  -> steady bar (xterm).
let &t_SI = "\e[5 q"
let &t_EI = "\e[1 q"

" File Browsing settings
	let g:netrw_banner=0
	let g:netrw_liststyle=3
	let g:netrw_showhide=1
	let g:netrw_winsize=20

"============================ KEY MAP ===============================

" Swap colon-semicolon:
	nnoremap ; :
	nnoremap : ;
	nnoremap ? :h<Space>
	xnoremap ; :
	xnoremap : ;

	nmap <silent> <C-Space> <Nop>
	nmap <silent> F <Nop>
	nmap <silent> f <Nop>

	" vmap <silent> <Space> <Nop>
	" nmap <silent> <Space> <Nop>
	nnoremap <silent> <leader>f <Nop>
	nnoremap <silent> <leader>ca <Nop>
	nnoremap <silent> gb <Nop>
	nnoremap <silent> gu <Nop>
	nnoremap <silent> gc <Nop>
	nnoremap <silent> ys <Nop>
	nnoremap <silent> yS <Nop>

" Document jumper w/o plugin
fu! ZetKey()
  if &filetype == "markdown"
    nnoremap <silent> <CR> gf
    nnoremap <silent> <Bslash> <C-o>
    nnoremap <leader>nf :e `echo _$(qn -d).md`<left><left><left><left><left><left><left><left><left><left><left><left><left><left><left>
    setlocal isfname+=32,],(,',?,`,:,;,&
    setlocal isfname-=#
    setlocal path+=$WIKI_DIR/,,**
    setlocal suffixesadd+=.md
    setlocal include=/\v%([.*]\(.*\))
    setlocal includeexpr=substitute(v:fname,'.*](','','\v%')
  elseif &filetype == "asciidoc"
    nnoremap <silent> <CR> gf
    nnoremap <silent> <Bslash> <C-o>
    nnoremap <leader>nf :e `echo _$(qn -d).adoc`<left><left><left><left><left><left><left><left><left><left><left><left><left><left><left>
    " https://stackoverflow.com/questions/695438/what-are-the-safe-characters-for-making-urls
    setlocal isfname+=32,[,',?,`,;,&						"all char as literal
    setlocal isfname-=#,:										"all char as space(non literal)
    setlocal path+=$WIKI_DIR/,,**								"target dir
    setlocal suffixesadd+=.adoc								"target file extension
    setlocal include=/\v%(:.*[.*])							"target regex
    setlocal includeexpr=substitute(v:fname,'[.*','','\v%')	"part to delete from regex
    setlocal nowrap
  elseif &buftype == "qf"
	nnoremap <CR> <CR>
  else
    nnoremap <CR> :!<space>
	nnoremap <Bslash> <CR>
  endif
endfu


 " Save file as sudo when no sudo permissions
	cmap w! :w `sudo tee >/dev/null %`

" Closing compaction in insert mode
	inoremap [ []<left>
	inoremap ( ()<left>
	inoremap { {}<left>
	inoremap < <><left>
	inoremap ' ''<left>
	inoremap " ""<left>
	inoremap /* /**/<left><left>

" Spell-check on\off
	nnoremap <silent> <C-z> :setlocal spell! spelllang=en_us<CR>

" Folding lines
	nnoremap <silent> <C-f> @=(foldlevel('.')?'za':"\<Space>") <CR>
	vnoremap <silent> <C-f> @=('zf') <CR>

" File Info
	nnoremap <leader>ll <cmd>echo expand('%:p')<CR>
" ToggleWrap
	nnoremap <leader>ss :set wrap!<CR>

" Togle list mode
	nnoremap <leader>sl :set list!<CR>

" Format a paragraph into lines
	map Q gq<CR><CR>

" Select all the text
	nnoremap <leader>A ggVG

" Opening a file explore
	nnoremap <silent> <leader>o :Lex<CR>

" Opening a file from explorer
	" nnoremap <leader>O :Explore<CR>

" Opening a terminal window
	nnoremap <c-t> :bo te<CR>
" Closing the terminal window
	tnoremap <c-t> :exit<CR>
" CTRL+I OR Esc to make the terminal scrollable and I to input mode
	tnoremap <c-i> <c-w><s-n>
	tnoremap <Esc> <C-\><C-n>

" You can split the window in Vim. y - in the y access , x - in the x access
	nnoremap <leader>\ :split<space>
	nnoremap <leader>- :vsplit<space>

 " Shortcutting split navigation
	nnoremap <C-Left> <C-w>h
	nnoremap <C-Down> <C-w>j
	nnoremap <C-Up> <C-w>k
	nnoremap <C-Right> <C-w>l
" Resize split windows using arrow keys by pressing:
" CTRL+UP, CTRL+DOWN, CTRL+LEFT, or CTRL+RIGHT.
	noremap <a-up> <c-w>+
	noremap <a-down> <c-w>-
	noremap <a-left> <c-w>>
	noremap <a-right> <c-w><

" Open OmniCompletion
	" imap <Tab> <C-X><C-O>
" Moving between tabs
	nnoremap <Tab> gt
  nnoremap <S-Tab> gT
	nnoremap <S-t> :tabnew<CR>

" Paste content target file in current currsor position
  nnoremap <leader>if :.-read<space>

" Opening\Creating a file in a new tab - write the tab to open
	nnoremap <leader>nt :tabedit<space>
	nnoremap <leader>fl :e **/*<C-z><S-Tab>
	nnoremap <leader>Fl :find **/*<C-z><S-Tab>

" Saving a file using CTRL+S
	nnoremap <silent> <C-S> :w<CR>
" Quitting force without saving
	nnoremap <silent> <C-q> :q!<CR>
" Quitting and saving a file
	nnoremap <leader>Q :wq<CR>
	nnoremap <leader>q :q<CR>

" Surround word with a wanted character
	" nnoremap <leader>rs <cmd>echo "Press a character: " \| let c = nr2char(getchar()) \| exec "normal viwo\ei" . c . "\eea" . c . "\e" \| redraw<CR>

" Replace all occurrences of a word
	nnoremap <leader>rw :%s/\<<c-r><c-w>\>//g<left><left>
	vnoremap <leader>rw :s///g<left><left><left>
  " inoremap <C-p> <c-r>=expand("")<Left><Left>
	nnoremap <leader>R :%s:::gI<Left><Left><Left><Left>
	nnoremap <leader>vt :!tmux splitw -t "$session_uuid:" -dhf <CR><CR>
	nnoremap <leader>ww :e $HOME/wiki/index.md<CR>
	nnoremap <leader>xo :!xdg-open %<cr><cr>

" Map V-Block to not confuse with Past
	noremap <leader>v <C-v>

" Fix Y function
	nmap Y y$

" open shellcheck buffers
  nnoremap <leader>cc :!clear && shellcheck %<CR>

" open buffer
  nnoremap <Leader>B :buffers<CR>
" show history
	nnoremap <Leader>H :history<CR>
" \k shows my marks
  nnoremap <leader>M :Marks<CR>
  nnoremap <leader>mv :!mv<Space>%<Space>
  nnoremap <leader>md :!mkdir<Space>
  nnoremap <leader>mf :!touch<Space>

  nnoremap <leader>N :!crf<Space>

  nnoremap <leader>O :!opout <c-r>%<CR><CR>

  nnoremap <leader>ph :!pandoc % --to=html5 > %.html

" \f shows the current 'formatoptions' at a glance
  nnoremap <leader>fo :setlocal formatoptions?<CR>

" Seeing the registers
	nnoremap <leader>rr <cmd>registers<CR>

" Fix reselect visual after intenting
	vnoremap < <gv
	vnoremap > >gv
" moving lines in visual mode
	vnoremap <C-J> :m '>+1<cr>gv=gv
	vnoremap <C-K> :m '>-2<cr>gv=gv

" Fix gx button to open URI
	nnoremap <silent> gu :execute 'silent! !xdg-open ' . shellescape(expand('<cWORD>'), 1)<cr><cr>
	vnoremap gu :silent execute "!xdg-open " . shellescape("<C-r>*") . " &"<cr>

if !has('nvim')
 " For copy and past
	vnoremap <C-Y> *y :let @+=@*<CR>
	map <C-P> +P
 " Remap normal/visual & to preserve substitution flags
 nnoremap <silent> & :&&<CR>
 if exists(':xnoremap')
   xnoremap <silent> & :&&<CR>
 endif
endif
 "=============================== AUTOMATION =================================
 "Put your automation script here
  if has("autocmd")
   augroup vanilla
   autocmd!
    if !has('nvim')
      "run on start
      au InsertEnter,CursorMovedI * :lclose
      au InsertEnter,BufWinLeave * set nobl
      au InsertEnter,BufWinEnter * set bl
	  " Potentialy change directory reference :
    endif

    "automatic save and load fold
    au BufWinEnter *.* silent! loadview
    " au BufEnter  *\(.md\)\@<!  silent! lcd %:p:h:gs/ /\\ /
    " au FileType markdown :lchdir %:p:h
    au BufEnter,BufWinEnter * silent! :lcd %:p:h
    au BufWinLeave *.* mkview
    au FileType asciidoc,markdown call ZetKey()
    au BufEnter,VimEnter $WIKI_DIR/* call ZetKey()
    "cursor switch
    au VimEnter * silent execute '!echo -ne "\e[1 q"' | redraw!
    au VimLeave * silent execute '!echo -ne "\e[ q"' | redraw!
    au InsertLeave * silent call cursor([getpos('.')[0], getpos('.')[2]+1])
    " Auto relativenumber
    au InsertEnter * set norelativenumber
    au InsertLeave * set relativenumber
    au QuickFixCmdPost [^l]* cwindow
    au QuickFixCmdPost    l* lwindow
    "run on start
    au FileType help au BufEnter <buffer> set wrap
    au VimEnter $WIKI_DIR/index* :! find $WIKI_DIR/*\.md -maxdepth 1 -type f > %
    "run noh on start
    au VimEnter,InsertEnter * :let @/=""
    au vimleavepre *.md :!perl -p -i -e 's,\[([^\]]+?)\]\(\),[\1](https://duck.com/lite?q=\1),g' %
    au vimleavepre *.adoc :!perl -p -i -e 's,link:\[([^\]]+?)\],link:https://duck.com/lite?kd=-1&kp=-1&q=\1\[\1],g' %
    "autoventer verticaly when enter insertmode
    au InsertEnter * norm zz
    "Vim Insert Mode timeout
    au CursorHoldI * stopinsert
    "Fix reindenting polyglot
    au BufEnter * set indentexpr=
    " mutt mail line wrapping
    au BufRead /tmp/mutt-* set textwidth=80
    "restore last cursor position when reopening file
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
    au BufReadPre  *.bin let &bin=1
    au BufReadPost *.bin if &bin | %!xxd
    au BufReadPost *.bin set ft=xxd | endif
    au BufWritePre *.bin if &bin | %!xxd -r
    au BufWritePre *.bin endif
    au BufWritePost *.bin if &bin | %!xxd
    au BufWritePost *.bin set nomod | endif
    "Automation LiveServer
    au FileType vim nnoremap <F7> :echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')<CR>
    au FileType help wincmd L
    au FileType nginx setlocal noet ts=4 sw=4 sts=4
    " Dockerfile settings
    au FileType dockerfile set noexpandtab
    " shell/config/systemd settings
    " au VimEnter $XDG_CONFIG_HOME/* set ft=sh
    au FileType fstab,systemd set noexpandtab
    au FileType gitconfig,sh,toml set noexpandtab
    " For all text files set 'textwidth' to 80 characters.
    au FileType text setlocal textwidth=80 fo+=2t ts=2 sw=2 sts=2 expandtab
    " spell check for git commits
    au FileType gitcommit setlocal spell
    "remove trailing whitespace on save
    au BufWritePre *\(.md\)\@<! %s/\s\+$//e
   augroup END
  endif
"------------------END_CONF------------------
