"============================= KEY MAPING ================================
 let g:vim_listmode_toggle="<F5>"
if has('nvim')
	 " Wayland Clipboard Support
 xnoremap <C-y> :call system("wl-copy", @")<cr>
 " nnoremap <C-p> :let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p
 nnoremap <C-p> :let @"=substitute(system("wl-paste --no-newline --primary"), '<C-v><C-m>', '', 'g')<cr>p
endif
 "=============================== MACRO =====================================
 " Set all macro on nnoremap

 " show RTFM
 nnoremap <silent> <leader>! :call <SID>show_documentation()<CR>
 nnoremap <Leader>. :LeaderBuffer<cr>
 " nnoremap <leader>css :call CheatSheetCommand()<CR>
 " nnoremap <leader>csq :call CheatSheetCursor()<CR>
 " nnoremap <leader>csl :call CheatList()<CR>

" Add a date timestamp between two new lines.
nnoremap <leader>id :call InsertDate()<CR>
nnoremap <leader>ir :call InsertRegulasi()<CR>
nnoremap <leader>ih :call InsertHeader()<CR>
nnoremap <leader>il :call ToDoLists()<CR>
nnoremap <leader>iL :call Lean()<CR>
nnoremap <leader>in :call NoteMd()<CR>
nnoremap <leader>ib :call BMC()<CR>
nnoremap <leader>it :call Taxonom()<CR>

if has('nvim')
lua << EOF
require('globals')
  -- [[ Basic Keymaps ]]
  -- Remap for dealing with word wrap
  Nmx( 'k', "v:count == 0 ? 'gk' : 'k'")
  Nmx( 'j', "v:count == 0 ? 'gj' : 'j'")
  Nmx( '<Up>', "v:count == 0 ? 'gk' : 'k'")
  Nmx( '<Down>', "v:count == 0 ? 'gj' : 'j'")
  Nm( '<Space>q', '<Cmd>q<CR>', 'Quit')
  Tm( '<Esc><Esc>', '<C-\\><C-n>', 'Exit terminal mode')

EOF
  finish
endif

"============================= Script Local =============================

 " Refactor concurrences of a search term
 function! s:search_action() abort
     call inputsave()
     let search_args = input("Search: ")
     call inputrestore()
     return ":CocSearch ".search_args."\r"
 endfunction
 " CODE NAVIGATION
 function! s:check_back_space() abort
         let col = col('.') - 1
         return !col || getline('.')[col - 1]  =~# '\s'
 endfunction

 " Show documentation in preview window.
 function! s:show_documentation() abort
   if (index(["vim","help"], &filetype) >= 0)
     execute "h ".expand("<cword>")
   elseif (coc#rpc#ready())
     call CocActionAsync("doHover")
   else
     execute "!" . &keywordprg . " " . expand("<cword>")
   endif
 endfunction

"============================= KEY MAPING ================================
 " Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
 inoremap <silent><expr> <S-TAB>
                         \ pumvisible() ? "\<C-n>" :
                         \ <SID>check_back_space() ? "\<S-TAB>" :
                         \ coc#refresh()
 inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
 " <Enter> to select completion
 " nnoremap <expr> <CR> &buftype ==# 'quickfix' ? "\<CR>" : <SID>search_action()
 inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
 "nnoremap <F7> use to toggle LiveServer
 " nnoremap <F8> :set list!<CR>
 " nnoremap <F9> :Lf<cr>
imap <C-l> <Plug>(fzf-complete-wordnet)
nmap <C-d> :DefineWord <C-R>=expand('<cword>')<CR><CR>
 nnoremap <F3> :call SelectFile()<CR><CR>
 nnoremap <F11> :ColorV<CR>
 nnoremap <F12> :call CocToggle()<CR>
 " Display ALE info
 nnoremap <silent> <leader>ai :ALEInfo<CR>
 nnoremap <silent> <leader>aI :ALEDetail<CR>
 " List available commands
 nnoremap <silent> <leader>ac  :<C-u>CocList commands<cr>
 " Enable ALE
 nnoremap <silent> <leader>abe :ALEEnableBuffer<CR>
 nnoremap <silent> <leader>ae :ALEEnable<CR>
 " Disable ALE
 nnoremap <silent> <leader>abd :ALEDisableBuffer<CR>
 nnoremap <silent> <leader>ad :ALEDisable<CR>
 " Run ALE linters for current buffer
 nnoremap <silent> <leader>al :ALELint<CR>
 nnoremap <silent> <leader>an <Plug>(ale_next_wrap)
 nnoremap <silent> <leader>ap <Plug>(ale_previous_wrap)
 " Restart ALE
 nnoremap <silent> <leader>ar :ALEReset<CR>
 " ALE fixers
 nnoremap <leader>af <Plug>(ale_fix)
 " tpope/vim-fugitive
 nnoremap <leader>gg :G<cr>
 nnoremap <leader>gf :Gdiff master<cr>
 nnoremap <leader>gl :G log -100<cr>
 nnoremap <leader>gP :G push<cr>
 " Commentor
 xmap c <Plug>Commentor
 nnoremap <leader>c <Plug>CommentorLine
 " Trigger autocomplete
 inoremap <silent><expr> <C-Space> coc#refresh()
 " Enable CoC
 nnoremap <leader>cA :CocEnable<CR>
 " Remap for do codeAction of current line
 nnoremap <silent> <leader>ca <Plug>(coc-codeaction)
 " Apply CodeAction to the selection (normal mode needs selection/motion suffix, such as `omap if` below)
 vmap <leader>a <Plug>(coc-codeaction-selected)
 nmap <leader>a <Plug>(coc-codeaction-selected)
" xmap ca <Plug>(coc-codeaction-selected)
" nnoremap <leader>cas <Plug>(coc-codeaction-selected)
 " Toggle diagnostics on/off
 nnoremap <silent> <leader>cat :call CocAction("diagnosticToggle")<CR>
 " Disable CoC
 nnoremap <leader>cd :CocDisable<CR>
 " Apply AutoFix to problem on the current line.
 nnoremap <leader>cf <Plug>(coc-fix-current)
 " Resume latest coc list.
 nnoremap <silent> <leader>clr :<C-u>CocListResume<CR>
 " Format (selected) code
 xmap <leader>cp <Plug>(coc-format-selected)
 nnoremap <leader>cp <Plug>(coc-format)
nnoremap <silent> <leader>cv :ColorVEdit<CR>
 " Restart CoC (needs second `<CR>` to confirm message from CoC saying that it is restarting)
 nnoremap <silent> <leader>cr :CocRestart<CR><CR>
 " Select function/class object
 xmap if <Plug>(coc-funcobj-i)
 omap if <Plug>(coc-funcobj-i)
 xmap af <Plug>(coc-funcobj-a)
 omap af <Plug>(coc-funcobj-a)
 xmap ic <Plug>(coc-classobj-i)
 omap ic <Plug>(coc-classobj-i)
 xmap ac <Plug>(coc-classobj-a)
 omap ac <Plug>(coc-classobj-a)

 nnoremap <leader>ft :FloatermToggle<CR>
 nnoremap <leader>lf :Lf<CR>
 vnoremap <leader>lf :Lf<CR>

 nnoremap  <leader>df :call wordnet#overviews("<C-r>=expand("<cword>")<CR>")<CR>
 nnoremap  <leader>ds :call wordnet#synonyms("<C-r>=expand("<cword>")<CR>")<CR>
 nnoremap  <leader>do :call wordnet#browse("<C-r>=expand("<cword>")<CR>")<CR>
 " janko/vim-test and puremourning/vimspector
 nnoremap <leader>dd :TestNearest -strategy=jest<CR>
 nnoremap <leader>da :call vimspector#Launch()<CR>
 nnoremap <leader>dC :call GotoWindow(g:vimspector_session_windows.code)<CR>
 nnoremap <leader>dV :call GotoWindow(g:vimspector_session_windows.variables)<CR>
 nnoremap <leader>dW :call GotoWindow(g:vimspector_session_windows.watches)<CR>
 nnoremap <leader>dS :call GotoWindow(g:vimspector_session_windows.stack_trace)<CR>
 nnoremap <leader>dO :call GotoWindow(g:vimspector_session_windows.output)<CR>
 nnoremap <leader>db :call AddToWatch()<CR>
 nnoremap <leader>de :call vimspector#Reset()<CR>
 nnoremap <leader>dT :call vimspector#ClearBreakpoints()<CR>
 nnoremap <S-j> :call vimspector#StepOut()<CR>
 nnoremap <S-l> :call vimspector#StepInto()<CR>
 nnoremap <S-k> :call vimspector#StepOver()<CR>
 nnoremap <S-h> :call vimspector#Restart()<CR>
 nnoremap <leader>dc :call vimspector#Continue()<CR>
 nnoremap <leader>dtc :call vimspector#RunToCursor()<CR>
 nnoremap <leader>dt :call vimspector#ToggleBreakpoint()<CR>
 nnoremap <leader>dtt :call vimspector#ToggleConditionalBreakpoint()<CR>
 " for visual mode, the visually selected text
 nmap <silent><Leader>di <Plug>VimspectorBalloonEval
 xmap <silent><Leader>di <Plug>VimspectorBalloonEval
 " run diagnostic code
 nnoremap <silent> <leader>dx :CocDiagnostics<CR>
 " Jump around diagnostic messages (let ALE do that when using both ALE & CoC in the same time)
 nnoremap <silent> <leader>d[ :ALEPrevious<CR>
 nnoremap <silent> <leader>d] :ALENext<CR>
 " Do default action for next/previous item.
 nnoremap <silent> <leader>dn  :<C-u>CocNext<CR>
 nnoremap <silent> <leader>dp  :<C-u>CocPrev<CR>
 " Show diagnostics list
 nnoremap <silent> <leader>d  :<C-u>CocList diagnostics<cr>

 " Manage extensions.
 nnoremap <silent> <leader>em :<C-u>CocList --no-quit extensions<cr>
 nnoremap <silent> <leader>eM :<C-u>CocList --no-quit marketplace<cr>
 " Update CoC
 nnoremap <silent> <leader>eu :CocUpdateSync<CR>:CocRebuild<CR>

 " Find symbol of current document.
 nnoremap <silent> <leader>fs  :<C-u>CocList outline<CR>

 "GitGutter
 nnoremap <silent> <leader>gn <Plug>(GitGutterNextHunk)
 nnoremap <silent> <leader>gp <Plug>(GitGutterPrevHunk)
 " GoTo code navigation
 nnoremap <silent> <leader>gdd <Plug>(coc-definition)
 nnoremap <silent> <leader>gdt <Plug>(coc-type-definition)
 nnoremap <silent> <leader>gD <Plug>(coc-declaration)
 nnoremap <silent> <leader>gi <Plug>(coc-implementation)
 nnoremap <silent> <leader>gr <Plug>(coc-references)
 nnoremap <silent> <leader>gru <Plug>(coc-references-used)

 " Display CoC info
 nnoremap <silent> <leader>ii :CocInfo<CR>
 " Organize imports of the current buffer
 nnoremap <leader>oi :call CocAction("runCommand", "editor.action.organizeImport")<CR>
 " Rename symbol
 nnoremap <leader>rs <Plug>(coc-rename)

 " Select range
 nnoremap <silent> <leader>sr <Plug>(coc-range-select)
 xmap <silent> sr <Plug>(coc-range-select)
 " Search workspace symbols.
 nnoremap <silent> <leader>ss  :<C-u>CocList --interactive symbols<CR>

 " Search workspace symbols.
 nnoremap <silent> <leader>tt :TestNearest<CR>
 nnoremap <silent> <leader>tf :TestFile<CR>
 nnoremap <silent> <leader>ts :TestSuite<CR>
 nnoremap <silent> <leader>tl :TestLast<CR>
 nnoremap <silent> <leader>tg :TestVisit<CR>

 nnoremap <silent> <leader>td <plug>DiaryH

 " Table mode
 function! s:isAtStartOfLine(mapping)
   let text_before_cursor = getline('.')[0 : col('.')-1]
   let mapping_pattern = '\V' . escape(a:mapping, '\')
   let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
   return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
 endfunction

 inoreabbrev <expr> <bar><bar>
           \ <SID>isAtStartOfLine('\|\|') ?
           \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
 inoreabbrev <expr> __
           \ <SID>isAtStartOfLine('__') ?
           \ '<c-o>:silent! TableModeDisable<cr>' : '__'
