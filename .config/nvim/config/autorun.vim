 "=============================== AUTOMATION =================================
 "Put your automation script here
  if has("autocmd")
   augroup automation
   autocmd!
    if !has('nvim')
		"run on start
		au VimEnter * HexokinaseTurnOn
		"fix bork bash detection
		au BufNewFile,BufRead * call DetectBash()
		" Save the undo sequence position on buffer load
		au InsertEnter * call DeleteHiddenBuffers()
		" Highlight the symbol and its references when holding the cursor.
		au CursorHold * silent call CocActionAsync("highlight")
		"Update signature help on jump placeholder
		au User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
		"autoprettier on save
		au InsertLeave,BufWritePre *\(.md\)\@<! :CocCommand prettier.formatFile
		"Setup formatexpr specified filetype(s).
		au FileType typescript,json set formatexpr=CocAction('formatSelected')
		" Find the git directory root on open of vim.
	else
		" Native LSP formatter
		" au BufWritePost <buffer> lua vim.lsp.buf.format()
		" au BufWritePost <buffer> FormatWrite
		" au BufWritePost * lua require('lint').try_lint()
		" au BufWritePost,BufEnter,FocusGained,ShellCmdPost,VimResume * call <SID>neogit#refresh_manually(expand('<afile>'))
  endif
		" au BufEnter * silent! FindGitRoot
    "run on start
    au InsertEnter * Limelight
    au VimLeave,InsertLeave * Limelight!
    au FileType help au BufEnter <buffer> Limelight
    " au InsertLeave,CursorMoved * TableModeDisable
    " git conf-Vim
    au FocusGained,CursorHold ?* if getcmdwintype() == '' | checktime | endif
    " au InsertEnter,InsertLeave *.md :TableModeToggle
    "Iluminate word
    au VimEnter * hi link illuminatedWord CursorLine
    au VimEnter * hi illuminatedWord cterm=bold gui=bold
    au VimEnter * hi illuminatedCurWord cterm=underline gui=underline
    "use asyncrun to auto correct
    au User AsyncRunStop copen | wincmd p
    " autocmd WinEnter * call OnTabEnter(expand("<amatch>"))
	"Automation LiveServer
    au FileType html nnoremap <F4> :AsyncRun live-server -q<CR>
    au FileType markdown nnoremap <F4> :!tmux splitw -t "$session_uuid:" -dhf 'mdv %'<CR><CR>
    au FileType markdown setlocal makeprg=pandoc\ -s\ -f\ markdown+smart\ --toc\ --metadata\ pagetitle="awesome ldap"\ --to=html5\ README.md\ -o\ index.html
   augroup END
  endif
