local init = {
	-- General function Library Neovim
	{ "nvim-lua/plenary.nvim",       event = "BufWinEnter", },
	-- icons
	{ "nvim-tree/nvim-web-devicons", event = "UIEnter",     enabled = vim.g.have_nerd_font },
	-- Asynchronous IO Library
	{ "nvim-neotest/nvim-nio",       event = "UIEnter", },
	{
		"folke/neodev.nvim",
		opts = {
			library = {
				plugins = { "nvim-dap-ui" },
				types = true
			},
		},
	},
}

if vim.fn.has("nvim") == 1 then
	return init
end
