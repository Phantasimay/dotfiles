local init = {
	'nvim-treesitter/nvim-treesitter',
	build = ':TSUpdate',
	event = 'VeryLazy',
	-- event = { "BufReadPre", "BufNewFile" },
	init = function(plugin)
		if vim.bo.filetype ~= "asciidoc" then
			require("lazy.core.loader").add_to_rtp(plugin)
			require "nvim-treesitter.query_predicates"
		end
	end,
	dependencies = {
		{ 'nvim-treesitter/nvim-treesitter-textobjects', },
		{ 'nvim-treesitter/nvim-treesitter-context', },
		{ 'RRethy/nvim-treesitter-textsubjects', },
		{ 'windwp/nvim-ts-autotag', },
		{ 'windwp/nvim-autopairs', },
	},
	config = function()
		-- [[ Configure Treesitter ]] ------------------------------------------------
		-- Enable the following syntax
		local Tree = {
			lua = {},
			tsx = {},
			regex = {},
			html = {},
			css = {},
			vimdoc = {},
			vim = {},
			latex = {},
			-- markdown = {},
			-- markdown_inline = {},
			bash = {},
			json = {},
			sql = {},
			query = {},
			xml = {},
			http = {},
			graphql = {},
			-- asciidoc = {},
		}
		---@class parser_configs
		local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()
		parser_configs.http = {
			install_info = {
				url = 'https://github.com/NTBBloodbath/tree-sitter-http',
				files = { 'src/parser.c' },
				branch = 'main'
			}
		}
		local Treesitter = require('nvim-treesitter.configs')
		Treesitter.setup({
			sync_install = false,
			ensure_installed = vim.tbl_keys(Tree),
			ignore_install = { "asciidoc", },
			auto_install = true,
			autopairs = { enable = true },
			autotag = { enable = true },
			modules = {},
			indent = {
				enable = true,
				disable = { 'yaml', 'python' },
			},
			highlight = { enable = true, additional_vim_regex_highlighting = false },
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = '<A-v>',
					node_incremental = '<A-v>',
					scope_incremental = '<C-Space>',
					node_decremental = 'v',
				},
			},

			textsubjects = {
				enable = true,
				prev_selection = 'v', -- (Optional) keymap to select the previous selection
				keymaps = {
					['<A-v>'] = 'textsubjects-smart',
					[']c'] = 'textsubjects-container-outer',
					['[c'] = { 'textsubjects-container-inner', desc = "Select inside containers (classes, functions, etc.)" },
				},
			},

			textobjects = {
				move = {
					enable = true,
					set_jumps = true, -- whether to set jumps in the jumplist
					goto_next_start = {
						[']f'] = '@function.outer',
						[']]'] = '@class.outer',
					},
					goto_next_end = {
						[']F'] = '@function.outer',
						[']['] = '@class.outer',
					},
					goto_next = {
						["]D"] = "@conditional.outer",
					},
					goto_previous_start = {
						['[f'] = '@function.outer',
						['[['] = '@class.outer',
					},
					goto_previous_end = {
						['[F'] = '@function.outer',
						['[]'] = '@class.outer',
					},
					goto_previous = {
						["[D"] = "@conditional.outer",
					}
				},
				swap = {
					enable = true,
					swap_next = {
						[']s'] = '@parameter.inner',
					},
					swap_previous = {
						['[s'] = '@parameter.inner',
					},
				},
				select = {
					enable = true,
					lookahead = true,
					keymaps = {
						['ai'] = { query = '@conditional.outer', desc = 'Select outer part of a contitional region' },
						['ii'] = { query = '@conditional.inner', desc = 'Select inner part of a contitional region' },
						['aa'] = { query = '@parameter.outer', desc = 'Select outer part of a parameter region' },
						['ia'] = { query = '@parameter.inner', desc = 'Select inner part of a parameter region' },
						['as'] = { query = '@statement.outer', desc = 'Select outer part of a statement region' },
						['is'] = { query = '@scopename.inner', desc = 'Select inner part of a statement region' },
						['aA'] = { query = '@attribute.outer', desc = 'Select outer part of a attribute region' },
						['iA'] = { query = '@attribute.inner', desc = 'Select inner part of a attribute region' },
						['r='] = { query = '@assignment.rhs', desc = 'Select right hand side of an assignment' },
						['l='] = { query = '@assignment.lhs', desc = 'Select left hand side of an assignment' },
						['af'] = { query = '@function.outer', desc = 'Select outer part of a function region' },
						['if'] = { query = '@function.inner', desc = 'Select inner part of a function region' },
						['a/'] = { query = '@comment.outer', desc = 'Select outer part of a comment region' },
						['i/'] = { query = '@comment.inner', desc = 'Select inner part of a comment region' },
						['a='] = { query = '@assignment.outer', desc = 'Select outer part of an assignment' },
						['i='] = { query = '@assignment.inner', desc = 'Select inner part of an assignment' },
						['ab'] = { query = '@block.outer', desc = 'Select outer part of a block region' },
						['ib'] = { query = '@block.inner', desc = 'Select inner part of a block region' },
						['ac'] = { query = '@class.outer', desc = 'Select outer part of a class region' },
						['ic'] = { query = '@class.inner', desc = 'Select inner part of a class region' },
						['aF'] = { query = '@frame.outer', desc = 'Select outer part of a frame region' },
						['iF'] = { query = '@frame.inner', desc = 'Select inner part of a frame region' },
						['al'] = { query = '@loop.outer', desc = 'Select outer part of a loop region' },
						['il'] = { query = '@loop.inner', desc = 'Select inner part of a loop region' },
					},
				},
			},
		})
		-- local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()
		-- parser_configs.asciidoc = {
		-- 	install_info = {
		-- 		url = 'https://github.com/Phantasimay/tree-sitter-asciidoc',
		-- 		files = { 'src/parser.c' },
		-- 		branch = 'master',
		-- 		generate_requires_npm = false, -- if stand-alone parser without npm dependencies
		-- 		requires_generate_from_grammar = false,
		-- 	},
		-- 	filetype = 'asciidoc',
		-- }
	end
}

if vim.fn.has('nvim') == 1 then
	return init
end
