local init = {
	-- -- Live Server Previewer
	'frabjous/knap',
	keys = {
		{
			'<F6>',
			mode = { 'n' },
			function()
				require('knap').close_viewer()
			end,
			desc = '[C]lose [V]iewer'
		},
		{
			'<F7>',
			mode = { 'n' },
			function()
				require('knap').toggle_autopreviewing()
			end,
			desc = '[V]iewer'
		},
	},
	config = function()
		--============================== Live Viewer ====================================
		local Viewer = {
			htmltohtml =
			"A=%outputfile% ; B=\"$HOME/${A%.html}-preview.html\" ; sed 's/<\\/head>/<meta http-equiv=\"refresh\" content=\"1\" ><\\/head>/' \"$A\" > \"$B\"",
			htmltohtmlviewerlaunch = "A=%outputfile% ; B=\"$HOME/${A%.html}-preview.html\" ; firefox \"$B\"",
			htmltohtmlviewerrefresh = "none",
			mdtohtml =
			"A=%outputfile% ; B=\"$HOME/${A%.html}-preview.html\" ; pandoc --standalone %docroot% -o \"$A\" && sed 's/<\\/head>/<meta http-equiv=\"refresh\" content=\"1\" ><\\/head>/' \"$A\" > \"$B\" ",
			-- mdtohtmlbufferasstdin = true,
			mdtohtmlviewerlaunch = "A=%outputfile% ; firefox \"$HOME/${A%.html}-preview.html\"",
			mdtohtmlviewerrefresh = "none",
			textopdfviewerlaunch = "apvlv %outputfile%",
			texttopdfviewerrefresh = "none",
			textopdfforwardjump = "false",
		}
		vim.g.knap_settings = Viewer
	end
}

if vim.fn.has('nvim') == 1 then
	return init
end
