return {
	"Phantasimay/formatter.nvim",
	branch = "sql_formatter",
	event = { "TextChanged" },
	config = function()
		-- [[ Formatters ]] ---------------------------------------------------
		local Format = require("formatter")
		Format.setup({
			filetype = {
				sh = require("formatter.filetypes.sh").shfmt,
				mysql = require("formatter.filetypes.sql").sql_formatter,
				python = require("formatter.filetypes.python").autoflake,
				javascript = require("formatter.filetypes.javascript").prettierd,
				typescript = require("formatter.filetypes.typescript").prettierd,
				css = require("formatter.filetypes.css").prettierd,
				html = require("formatter.filetypes.html").prettierd,
				json = require("formatter.filetypes.json").prettierd,
				-- markdown = require("formatter.filetypes.markdown").prettierd,
				["*"] = require("formatter.filetypes.any").remove_trailing_whitespace,
			},
		})
	end,
}
