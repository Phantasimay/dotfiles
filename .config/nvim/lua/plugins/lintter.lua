return {
	"mfussenegger/nvim-lint",
	event = { "TextChanged" },
	config = function()
		-- [[ Linters ]] ---------------------------------------------------
		local Lint = require("lint")
		Lint.linters_by_ft = {
			sh = { "shellcheck" },
			-- mysql = { "sqlfluff" },
			python = { "flake8" },
			vim = { "vint" },
			javascript = { "eslint_d" },
			typescript = { "eslint_d" },
			css = { "stylelint" },
			html = { "markuplint" },
			json = { "jsonlint" },
			-- markdown = { "markdownlint" },
		}
	end,
}
