local init = {
	-- [[ Neovim UI/UX ]]
	{
		"folke/noice.nvim",
		event = { 'BufWinEnter' },
		init = function()
			vim.api.nvim_create_autocmd({ "InsertLeave", "CursorMoved", "TextChanged", },
				{
					-- pattern = { "*" },
					callback = function()
						require("noice").cmd("dismiss")
					end,
				})
		end,
		dependencies = {
			-- UI Library
			{
				"MunifTanjim/nui.nvim",
				priority = 1000,
			},
			-- Show notification
			{
				"rcarriga/nvim-notify",
				opts = {
					background_colour = "#000000",
					timeout = 3000,
					fps = 20,
					level = vim.log.levels.INFO,
					render = "compact",
					stages = "fade",
					top_down = false,
					max_height = function()
						return math.floor(vim.o.lines * 0.75)
					end,
					max_width = function()
						return math.floor(vim.o.columns * 0.75)
					end,
					on_open = function(win)
						vim.api.nvim_win_set_config(win, { zindex = 100 })
					end,
				},
			},
			-- Box action dialog
			{
				"stevearc/dressing.nvim",
				opts = {},
				init = function()
					---@diagnostic disable-next-line: duplicate-set-field
					vim.ui.select = function(...)
						require("lazy").load({ plugins = { "dressing.nvim" } })
						return vim.ui.select(...)
					end
					---@diagnostic disable-next-line: duplicate-set-field
					vim.ui.input = function(...)
						require("lazy").load({ plugins = { "dressing.nvim" } })
						return vim.ui.input(...)
					end
				end,
			},
			-- Keys stroke peeker.
			{
				"folke/which-key.nvim",
				event = 'UIEnter',
				keys = {
					{
						"<leader><F49>",
						function()
							require("which-key").show({ global = false })
						end,
						desc = "Buffer Local Keymaps (which-key)",
					},
				},
				config = function()
					local WKeys = require("which-key")
					WKeys.setup({
						preset = "helix",
						plugins = {
							presets = {
								operators = { gc = "Comments" },
							},
						},
						win = {
							border = "rounded", -- none, single, double, shadow, rounded
							wo = {
								winblend = 0, -- opacities
							},
						},
						layout = {
							height = { min = 4, max = 25 },
							width = { min = 20, max = 50 },
							align = "left", -- align columns left, center or right
						},
					})
				end,
			},
			-- Status bar navigator
			{
				'Bekaboo/dropbar.nvim',
				init = function()
					vim.ui.select = require('dropbar.utils.menu').select
				end
			},
		},
		keys = {
			{
				"<leader>em",
				mode = { 'n' },
				function()
					require("noice").cmd("telescope")
				end,
				desc = "[E]rror [M]essenge",
			},
		},
		config = function()
			require("globals")
			-- [[ Noice ]]--------------------------------------------------
			local Noice = require("noice")
			Noice.setup({
				progress = { enable = false },
				lsp = {
					override = {
						["vim.lsp.util.convert_input_to_markdown_lines"] = true,
						["vim.lsp.util.stylize_markdown"] = true,
						["cmp.entry.get_documentation"] = true,
					},
					-- signature = { enabled = false },
				},
				-- you can enable a preset for easier configuration
				presets = {
					command_palette = true, -- position the cmdline and popupmenu together
					bottom_search = false, -- use a classic bottom cmdline for search
					long_message_to_split = true, -- long messages will be sent to a split
					inc_rename = false, -- enables an input dialog for inc-rename.nvim
					lsp_doc_border = true, -- add a border to hover docs and signature help
				},
				routes = {
					view = "mini",
					{
						view = 'notify',
						filter = { event = 'msg_showmode' },
					},
					-- supress annoying messages
					{ filter = { event = 'msg_show', find = '; before #%d+', },  opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = '; after #%d+', },   opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = '>ed', },            opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = '<ed', },            opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = '[.]*L, [.]*B', },   opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = 'second[s]? ago', }, opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = 'yanked', },         opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = 'more lines', },     opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = 'fewer lines', },    opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = 'line less', },      opts = { skip = true }, },
					{ filter = { event = 'msg_show', find = 'written', },        opts = { skip = true }, },
					-- { filter = { event = 'msg_show', kind = '', },               opts = { skip = true }, },
				},
			})
		end,
	},

	-- Toggle floatterminal
	{
		"numToStr/FTerm.nvim",
		keys = {
			{
				'<leader>ft',
				mode = { 'n', 't' },
				function()
					require("FTerm").toggle()
				end,
				desc = "Toggle Float terminal",
			},
		},
	},

	-- twinlight still not okey for limelight
	{
		"junegunn/limelight.vim",
		event = "UIEnter",
	},
	{
		"tpope/vim-sleuth",
		event = "UIEnter",
	},
}

if vim.fn.has("nvim") == 1 then
	return init
else
	return {}
end
