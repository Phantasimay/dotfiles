local init = {
	{
		'pwntester/octo.nvim',
		dependencies = {
			'nvim-lua/plenary.nvim',
			'nvim-telescope/telescope.nvim',
			-- OR 'ibhagwan/fzf-lua',
			'nvim-tree/nvim-web-devicons',
		},
		opts = {},
		cmd = { 'Octo', },
	},
	{
		"NeogitOrg/neogit",
		dependencies = {
			"sindrets/diffview.nvim", -- optional - Diff integration
		},
		opts = {},
		cmd = { 'Neogit', 'NeogitResetStat' },
	},
	{
		'lewis6991/gitsigns.nvim',
		enabled = function()
			if vim.fn.isdirectory('.git') == 0 then
				return false
			end
		end,
		event = { 'BufReadPre', 'BufNewFile' },
		opts = {
			signcolumn = true,
			current_line_blame = true,
			current_line_blame_opts = {
				virt_text = true,
				virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
				delay = 500,
			},
			signs = {
				add = { text = '+' },
				change = { text = '~' },
				delete = { text = '_' },
				topdelete = { text = '‾' },
				changedelete = { text = '~' }
			}
		},
		on_attach = function()
			vim.wo.signcolumn = "number"
		end,
		config = function()
			require('globals')
			local gs = require('gitsigns')
			Nm(']h', gs.next_hunk, 'Next Hunk')
			Nm('[h', gs.prev_hunk, 'Prev Hunk')
			NVm('<leader>ghs', ':Gitsigns stage_hunk<CR>', 'Stage Hunk')
			NVm('<leader>ghr', ':Gitsigns reset_hunk<CR>', 'Reset Hunk')
			Nm('<leader>ghS', gs.stage_buffer, 'Stage Buffer')
			Nm('<leader>ghu', gs.undo_stage_hunk, 'Undo Stage Hunk')
			Nm('<leader>ghR', gs.reset_buffer, 'Reset Buffer')
			Nm('<leader>ghp', gs.preview_hunk_inline, 'Preview Hunk Inline')
			Nm('<leader>ghb', function() gs.blame_line({ full = true }) end, 'Blame Line')
			Nm('<leader>ghd', gs.diffthis, 'Diff This')
			Nm('<leader>ghD', function() gs.diffthis('~') end, 'Diff This ~')
			vim.keymap.set({ 'o', 'x' }, 'ih', ':<C-U>Gitsigns select_hunk<CR>', { desc = 'GitSigns Select Hunk' })
		end
	},
}

if vim.fn.has('nvim') == 1 then
	return init
end
