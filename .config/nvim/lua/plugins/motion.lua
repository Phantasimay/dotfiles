local init = {
	-- Jumper key.
	{
		'folke/flash.nvim',
		event = 'UIEnter',
		opts = {},
		keys = {
			{
				's',
				mode = { 'n', 'x', 'o' },
				function() require('flash').jump() end,
				desc = 'Flash'
			},
			{
				'S',
				mode = { 'n', 'x', 'o' },
				function() require('flash').treesitter() end,
				desc = 'Flash Treesitter'
			},
			{
				'r',
				mode = 'o',
				function() require('flash').remote() end,
				desc = 'Remote Flash'
			},
			{
				'R',
				mode = { 'o', 'x' },
				function() require('flash').treesitter_search() end,
				desc = 'Treesitter Search'
			},
			{
				'<c-s>',
				mode = { 'c' },
				function() require('flash').toggle() end,
				desc = 'Toggle Flash Search'
			}
		}
	},
	{
		'ThePrimeagen/harpoon',
		branch = 'harpoon2',
		opts = {},
		dependencies = {
			{ 'nvim-lua/plenary.nvim' },
			{
				'nvim-telescope/telescope.nvim',
				branch = "0.1.x",
			},
		},
		keys = {
			{
				'<leader>ml',
				mode = { 'n' },
				function()
					require('harpoon').ui:toggle_quick_menu(
						require('harpoon'):list())
				end,
				desc = '[M]arks [L]ist'
			},
			{
				'ma',
				mode = { 'n' },
				function() require('harpoon'):list():append() end,
				desc = 'Add Mark'
			},
			{
				'<leader>mp',
				mode = { 'n' },
				function() require('harpoon'):list():prev() end,
				desc = 'Jump prev buff'
			},
			{
				'<leader>mn',
				mode = { 'n' },
				function() require('harpoon'):list():next() end,
				desc = 'Jump next buff'
			},
			{
				'<leader>sm',
				mode = { 'n' },
				function()
					local conf = require('telescope.config').values
					local function toggle_telescope(harpoon_files)
						local file_paths = {}
						for _, item in ipairs(harpoon_files.items) do
							table.insert(file_paths, item.value)
						end
						require('telescope.pickers').new({}, {
							prompt_title = 'Harpoon',
							finder = require('telescope.finders').new_table({
								results = file_paths,
							}),
							previewer = conf.file_previewer({}),
							sorter = conf.generic_sorter({}),
						}):find()
					end
					toggle_telescope(require('harpoon'):list())
				end,
				desc = '[S]earch [H]arpoon'
			},
		},
	},
}

if vim.fn.has('nvim') == 1 then
	return init
end
