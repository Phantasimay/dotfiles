-- Model I
local init = {
	{
		"dhruvasagar/vim-table-mode",
		-- enabled = false,
		dependences = { "godlygeek/tabular", },
		keys = {
			{
				"|",
				mode = { 'i' },
				function()
					vim.cmd([[let g:table_mode_header_fillchar='=']])
					vim.cmd([[let g:table_mode_corner_corner='|']])
					vim.cmd([[let g:table_mode_syntax = 0]])
					vim.cmd(':TableModeToggle')
					vim.cmd(':norm a|')
				end,
				desc = "[T]ableize"
			},
		},
	},
	{
		"renerocksai/calendar-vim",
		keys = {
			{
				"<leader>k",
				mode = { 'n' },
				'<plug>CalendarV',
				desc = "[K]alender"
			},
		},
	},

	-- Rendering Scienctific formula
	{
		"jbyuki/nabla.nvim",
		keys = {
			{
				"<leader>pf",
				mode = { 'n' },
				function()
					require("nabla").popup()
				end,
				desc = "Toggle Formula"
			},
		},
	},
	-- Draw Ascii diagram
	{
		"jbyuki/venn.nvim",
		keys = {
			{
				"<leader>dg",
				mode = { 'n', 'v' },
				function()
					Toggle_venn()
				end,
				desc = "Toggle venn mode"
			},
		},
		config = function()
			-- [[ Venn Graph ]]
			function _G.Toggle_venn()
				local venn_enabled = vim.inspect(vim.b.venn_enabled)
				if venn_enabled == "nil" then
					vim.b.venn_enabled = true
					vim.cmd([[setlocal ve=all]])
					-- draw a line on HJKL keystokes
					vim.api.nvim_buf_set_keymap(0, "n", "J", "<C-v>j:VBox<CR>", { noremap = true })
					vim.api.nvim_buf_set_keymap(0, "n", "K", "<C-v>k:VBox<CR>", { noremap = true })
					vim.api.nvim_buf_set_keymap(0, "n", "L", "<C-v>l:VBox<CR>", { noremap = true })
					vim.api.nvim_buf_set_keymap(0, "n", "H", "<C-v>h:VBox<CR>", { noremap = true })
					-- draw a box by pressing 'f' with visual selection
					vim.api.nvim_buf_set_keymap(0, "v", "<CR>", ":VBox<CR>", { noremap = true })
				else
					vim.cmd([[setlocal ve=]])
					vim.cmd([[mapclear <buffer>]])
					vim.b.venn_enabled = nil
					--     vim.api.nvim_buf_del_keymap(0, "n", "J")
					--     vim.api.nvim_buf_del_keymap(0, "n", "K")
					--     vim.api.nvim_buf_del_keymap(0, "n", "L")
					--     vim.api.nvim_buf_del_keymap(0, "n", "H")
					--     vim.api.nvim_buf_del_keymap(0, "v", "f")
				end
			end
		end,
	},
}

if vim.fn.has("nvim") == 1 then
	return init
end
