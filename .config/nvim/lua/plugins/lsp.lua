local init = {
	"neovim/nvim-lspconfig",
	branch = "master",
	event = { "BufReadPre", "BufNewFile" },
	opts = { inlay_hints = { enabled = true } },
	dependencies = {
		{
			"williamboman/mason.nvim",
			cmd = "Mason",
			build = ":MasonUpdate",
			opts = {},
		},
		{ "WhoIsSethDaniel/mason-tool-installer.nvim", },
		{ "williamboman/mason-lspconfig.nvim", },
		-- Previewer LSP
		{ "hrsh7th/cmp-nvim-lsp" },
		{
			"nvim-telescope/telescope.nvim",
			branch = "0.1.x"
		},
		{ "mfussenegger/nvim-lint" },
		{
			"zeioth/garbage-day.nvim",
			enabled = true,
			event = "VeryLazy",
			opts = {
				aggressive_mode = true,
			}
		},
		-- Refactoring tool
		{
			"nvim-pack/nvim-spectre",
			enabled = true,
			config = function()
				require('spectre').setup({
					result_padding = '',
					is_block_ui_break = true,
				})
			end,
			keys = {
				{
					"<leader>rS",
					mode = { 'n', 'v' },
					function()
						require('spectre').toggle()
					end,
					desc = "[S]pecter"
				},
				{
					"<leader>rRc",
					mode = { 'n', 'v' },
					function()
						require('spectre').open_visual({ select_word = true })
					end,
					desc = "[R]efactori [C]urrent word"
				},
			},
		},
		-- { "nanotee/sqls.nvim" },
		-- Embedded LSP
		{
			"jmbuhr/otter.nvim",
			opts = {
				activate = { '*' },
			},
			-- config = function()
			-- 	local Otter = require('otter')
			-- 	--- Activate the current buffer by adding and synchronizing
			-- 	---@param languages table|nil List of languages to activate. If nil, all available languages will be activated.
			-- 	---@param completion boolean|nil Enable completion for otter buffers. Default: true
			-- 	---@param diagnostics boolean|nil Enable diagnostics for otter buffers. Default: true
			-- 	---@param tsquery string|nil Explicitly provide a treesitter query. If nil, the injections query for the current filetyepe will be used. See :h treesitter-language-injections.
			-- 	Otter.activate(languages, completion, diagnostics, tsquery)
			-- end,
		},
		-- Lsp Peeker
		{
			"rmagatti/goto-preview",
			event = 'VimEnter',
			config = function()
				require('globals')
				require('goto-preview').setup({})

				-- [[ Peek LSP ]]
				Nm(
					"<leader>pd",
					'<cmd>lua require("goto-preview").goto_preview_definition()<CR>',
					"[P]eek [D]efinition"
				)
				Nm(
					"<leader>pD",
					'<Cmd>lua require("goto-preview").goto_preview_declaration()<CR>',
					"[P]eek [D]eclaration"
				)
				Nm(
					"<leader>pi",
					'<Cmd>lua require("goto-preview").goto_preview_implementation()<CR>',
					"[P]eek [I]mplementation"
				)
				Nm(
					"<leader>pr",
					'<Cmd>lua require("goto-preview").goto_preview_references()<CR>',
					"[P]eek [R]eferences"
				)
				Nm(
					"<leader>pt",
					'<Cmd>lua require("goto-preview").goto_preview_type_definition()<CR>',
					"[P]eek [T]ype definition"
				)
				Nm(
					"<leader>pc",
					'<Cmd>lua require("goto-preview").close_all_win { skip_curr_window = true }<CR>',
					"[P]eek [C]lose"
				)
			end
		},
	},

	config = function()
		vim.api.nvim_create_autocmd("LspAttach", {
			group = vim.api.nvim_create_augroup("kickstart-lsp-attach", { clear = true }),
			callback = function(event)
				require("globals")

				-- [[ Diagnostic keymaps ]]
				Nm("[d", function() vim.diagnostic.jump({ count = -1, float = true }) end,
					"Go to previous diagnostic message")
				Nm("]d", function() vim.diagnostic.jump({ count = 1, float = true }) end, "Go to next diagnostic message")
				Nm("<leader>/", function() vim.diagnostic.open_float({ border = "rounded" }) end,
					"Open floating diagnostic message")
				Nm("<leader>?", vim.diagnostic.setloclist, "Open diagnostics list")

				-- [[ Buildin LSP ]]
				Nm("<leader>gd", vim.lsp.buf.definition, "[G]oto [D]efinition")
				Nm("<leader>gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
				Nm("<leader>gI", vim.lsp.buf.implementation, "[G]oto [I]mplementation")
				Nm("<leader>gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences")
				Nm("<C-A-K>", function() vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled()) end,
					"[I]nlay [H]int")
				Nm("K>",
					vim.lsp.buf.hover or require("telescope.builtin").find_files({ cwd = vim.fn.stdpath("config") }),
					"Hover Documentation")
				NIm("<C-k>", vim.lsp.buf.signature_help, "Signature Documentation")
				Nm("<leader>cA", vim.lsp.buf.code_action, "[C]ode [A]ction")
				Nm("<leader>gt", vim.lsp.buf.type_definition, "Type [D]efinition")
				Nm("<leader>ds", require("telescope.builtin").lsp_document_symbols, "[D]ocument [S]ymbols")
				Nm("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
				Nm("<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "[--]orkspace [S]ymbols")
				Nm("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
				Nm("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
				Nm("<leader>wl", function()
					print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
				end, "[W]orkspace [L]ist Folders")

				-- local hint = vim.lsp.inlay_hint.get({ bufnr = 0 })[1] -- 0 for current buffer
				--
				-- local client = vim.lsp.get_client_by_id(hint.client_id)
				-- local resp = client.request_sync('inlayHint/resolve', hint.inlay_hint, 100, 0)
				-- local resolved_hint = assert(resp and resp.result, resp.err)
				-- vim.lsp.util.apply_text_edits(resolved_hint.textEdits, 0, client.encoding)
				--
				-- location = resolved_hint.label[1].location
				-- client.request('textDocument/hover', {
				-- 	textDocument = { uri = location.uri },
				-- 	position = location.range.start,
				-- })

				-- Autoformatter detection
				local lsp_fmt_group = vim.api.nvim_create_augroup('AlterLspProvider', {})
				vim.api.nvim_create_autocmd('BufWritePre', {
					group = lsp_fmt_group,
					callback = function(ev)
						local client = vim.lsp.get_clients({ bufnr = ev.buf })[1]
						if client then
							if client.server_capabilities.documentFormattingProvider then
								vim.lsp.buf.format({ async = false })
							else
								-- vim.lsp.buf.format({ name = 'efm' })
								vim.cmd(":FormatWrite")
							end
						else
							vim.cmd(":FormatWrite")
							require('lint').try_lint()
						end
					end,
				})

				-- When you move your cursor, the highlights will be cleared (the second autocommand).
				local client = vim.lsp.get_client_by_id(event.data.client_id)
				if client and client.server_capabilities.documentHighlightProvider then
					vim.api.nvim_create_augroup('lsp_document_highlight', {
						clear = false
					})
					vim.api.nvim_clear_autocmds({
						buffer = event.buf,
						group = 'lsp_document_highlight',
					})
					vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
						buffer = event.buf,
						callback = vim.lsp.buf.document_highlight,
					})
					vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
						buffer = event.buf,
						callback = vim.lsp.buf.clear_references,
					})
				end
			end,
		})

		-- Extended lsp capabilities
		local capabilities = vim.lsp.protocol.make_client_capabilities()
		capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())
		capabilities.textDocument.foldingRange = {
			dynamicRegistration = false,
			lineFoldingOnly = true
		}
		capabilities.textDocument.completion.completionItem.snippetSupport = true
		capabilities.textDocument.completion.completionItem.resolveSupport = {
			properties = { 'documentation', 'detail', 'additionalTextEdits', }
		}

		-- LSP Prevents inline buffer annotations
		vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
			virtual_text = function()
				vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
					pattern = { "*" },
					callback = function()
						-- vim.diagnostic.disable()
						vim.diagnostic.open_float(nil, {
							focus = "true",
							border = "rounded",
						})
					end,
				})
			end,
			border = "rounded",
			min = "Warning",
			source = "if_many",
			signs = true,
			underline = true,
			update_on_insert = true,
			severity_sort = true,
		})
		-- local sign = function(opts)
		--   vim.fn.sign_define(opts.name, {
		--     texthl = opts.name,
		--     text = opts.text,
		--     numhl = ''
		--   })
		-- sign({name = 'DiagnosticSignError', text = '✘'})
		-- sign({name = 'DiagnosticSignWarn', text = '▲'})
		-- sign({name = 'DiagnosticSignHint', text = '⚑'})
		-- sign({name = 'DiagnosticSignInfo', text = '»'})
		-- end
		--

		local servers = {

			-- Servers
			-- "efm",
			"html-lsp",
			"css-lsp",
			"json-lsp",
			"bash-language-server",
			-- "marksman",
			"vim-language-server",
			"typescript-language-server",
			"lua-language-server",
			"pyright",
			"intelephense",
			"sqlls",

			-- Formatters
			"prettierd",
			"shfmt",
			"autoflake",
			"sql-formatter",

			-- Linters
			"stylelint",
			"markuplint",
			-- "markdownlint",
			"vint",
			"shellcheck",
			"jsonlint",
			"flake8",
			-- "sqlfluff",
			{
				"eslint_d",
				timeout = 10000,
			},

			-- Debugger
			"bash-debug-adapter",
			"js-debug-adapter",
		}

		local ensure_installed = servers
		vim.list_extend(ensure_installed, {
			-- Used to custom server
		})
		-- [[ Auto Installer 3rd party ]] ---------------------------------------------------
		local Installer = require("mason-tool-installer")
		Installer.setup({
			ensure_installed = ensure_installed,
			auto_update = true,
		})
		vim.api.nvim_create_autocmd("User", {
			pattern = "MasonToolsStartingInstall",
			callback = function()
				vim.schedule(function()
					print("mason-tool-installer is starting")
				end)
			end,
		})
		vim.api.nvim_create_autocmd("User", {
			pattern = "MasonToolsUpdateCompleted",
			callback = function(e)
				vim.schedule(function()
					print(vim.inspect(e.data) or {})
				end)
			end,
		})

		-- [[ Mason-Lspconfig ]] ---------------------------------------------------
		require("mason-lspconfig").setup({
			handlers = {
				function(server_name)
					local server = servers[server_name] or {}
					-- server.capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {})
					require("lspconfig")[server_name].setup(server)
				end,
			},
		})

		-- [[ Specific LSPConfig ]] ---------------------------------------------------
		require 'lspconfig'.lua_ls.setup({
			capabilities = capabilities,
			settings = {
				Lua = {
					hint = { enable = true },
					diagnostics = {
						globals = { 'vim', 'setup' }
					},
					runtime = {
						version = "LuaJIT",
						-- path = vim.split(package.path, ";"),
					},
					completion = { callSnippet = "Replace" },
					telemetry = { enable = false },
					workspace = {
						checkThirdParty = false,
						words = { "vim%.uv" },
						library = {
							"${3rd}/luv/library",
							-- unpack(vim.api.nvim_get_runtime_file("", true)),
							-- [vim.fn.expand("$VIMRUNTIME/lua")] = true,
							-- [vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true,
						},
					},
				}
			}
		})
		require 'lspconfig'.vimls.setup({
			capabilities = capabilities,
			workspace = { checkThirdParty = false }
		})
		require 'lspconfig'.sqlls.setup({
			cmd = { "sql-language-server", "up", "--method", "stdio" },
			-- filetypes = { 'sql', 'mysql' },
			capabilities = capabilities,
			single_file_support = true,
			root_dir = function(fname)
				local root_files = {
					".sqllsrc.json",
				}
				return require 'lspconfig'.util.path.dirname(fname)
					or require 'lspconfig'.util.root_pattern(table.unpack(root_files))(fname)
					or require 'lspconfig'.util.find_git_ancestor(fname)
					or require 'lspconfig'.util.path.dirname(fname)
					or vim.loop.cwd()
			end,
			settings = {
				sqlls = {
					connections = {
						name = "sql-language-server",
						adapter = "mysql",
						host = "localhost",
						port = 3306,
						user = "sandun",
						password = "@damevA5758",
						database = "test2_db",
						-- projectPaths = "mysql://localhost:3306/test_db?user=sandun&password=@damevA5758",
						-- ssh = {
						--   user = "ubuntu",
						--   remoteHost = "ec2-xxx-xxx-xxx-xxx.ap-southeast-1.compute.amazonaws.com",
						--   dbHost = "127.0.0.1",
						--   port = 3306,
						--   identityFile = "~/.ssh/id_rsa",
						--   passphrase = "123456"
						-- }
					}
				}
			}
		})
		require 'lspconfig'.ts_ls.setup({
			capabilities = capabilities,
			settings = {
				typescript = {
					inlayHints = {
						includeInlayParameterNameHints = "literal",
						includeInlayParameterNameHintsWhenArgumentMatchesName = false,
						includeInlayFunctionParameterTypeHints = false,
						includeInlayVariableTypeHints = false,
						includeInlayPropertyDeclarationTypeHints = false,
						includeInlayFunctionLikeReturnTypeHints = true,
						includeInlayEnumMemberValueHints = true,
					},
				},
				javascript = {
					inlayHints = {
						includeInlayParameterNameHints = "all",
						includeInlayParameterNameHintsWhenArgumentMatchesName = false,
						includeInlayFunctionParameterTypeHints = true,
						includeInlayVariableTypeHints = true,
						includeInlayPropertyDeclarationTypeHints = true,
						includeInlayFunctionLikeReturnTypeHints = true,
						includeInlayEnumMemberValueHints = true,
					},
				},
			},
		})
		require 'lspconfig'.intelephense.setup({
			capabilities = capabilities,
			settings = {
				intelephense = {
					stubs = {
						"bcmath",
						"bz2",
						"Core",
						"curl",
						"date",
						"dom",
						"fileinfo",
						"filter",
						"gd",
						"gettext",
						"hash",
						"iconv",
						"imap",
						"intl",
						"json",
						"libxml",
						"mbstring",
						"mcrypt",
						"mysql",
						"mysqli",
						"password",
						"pcntl",
						"pcre",
						"PDO",
						"pdo_mysql",
						"Phar",
						"readline",
						"regex",
						"session",
						"SimpleXML",
						"sockets",
						"sodium",
						"standard",
						"superglobals",
						"tokenizer",
						"xml",
						"xdebug",
						"xmlreader",
						"xmlwriter",
						"yaml",
						"zip",
						"zlib",
						"wordpress-stubs",
						"woocommerce-stubs",
						"acf-pro-stubs",
						"wordpress-globals",
						"wp-cli-stubs",
						"genesis-stubs",
						"polylang-stubs"
					},
					environment = {
						includePaths = {
							'~/.config/composer/vendor/php-stubs/',
							'~/.config/composer/vendor/wpsyntex/'
						}
					},
					files = {
						maxSize = 5000000,
					},
					root_dir = function()
						return vim.loop.cwd()
					end,
				},
			},
		})
	end,
}

if vim.fn.has("nvim") == 1 then
	return init
end
