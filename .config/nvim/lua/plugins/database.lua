local init = {
	{
		'kristijanhusak/vim-dadbod-ui',
		-- event = "VeryLazy",
		dependencies = {
			{ 'tpope/vim-dadbod',                     lazy = true },
			{ 'kristijanhusak/vim-dadbod-completion', ft = { 'sql', 'mysql', 'plsql' }, lazy = true },
			{ "nvim-lua/plenary.nvim", },
			{ "hrsh7th/nvim-cmp", },
		},
		keys = {
			{
				'<CR>',
				mode = { 'v', 'n' },
				":DB<CR>",
				{ noremap = true, silent = true },
			},
			{
				'<leader>da',
				mode = { 'n' },
				'<Plug>(DBUI_AddConnection)',
				desc = 'Connect to DB',
			},
			{
				'<leader>dh',
				mode = { 'n' },
				'<Plug>(DBUI_ToggleDetails)',
				desc = 'Detail DB',
			},
			{
				'<leader>db',
				mode = { 'n' },
				'<cmd>DBUI<CR>',
				desc = 'Start DB UI',
			},
			{
				'<leader>dt',
				mode = { 'n' },
				'<cmd>DBUIToggle<CR>',
				desc = 'Toggle DB UI',
			},
			{
				'<leader>df',
				mode = { 'n' },
				'<cmd>DBUIFindBuffer<CR>',
				desc = 'Toggle DB Query',
			},
		},
		config = function()
			vim.g.db_ui_save_location = vim.fn.stdpath "config" .. require("plenary.path").path.sep .. "db_ui"
			-- local function db_completion()
			-- 	require("cmp").setup.buffer { sources = { { name = "vim-dadbod-completion" } } }
			-- end
			-- local autocomplete_group = vim.api.nvim_create_augroup('vimrc_autocompletion', { clear = true })
			-- vim.api.nvim_create_autocmd("FileType", {
			-- 	pattern = {
			-- 		"sql",
			-- 	},
			-- 	command = [[setlocal omnifunc=vim_dadbod_completion#omni]],
			-- 	group = autocomplete_group,
			-- })
			--
			-- vim.api.nvim_create_autocmd("FileType", {
			-- 	pattern = {
			-- 		"sql",
			-- 		"mysql",
			-- 		"plsql",
			-- 	},
			-- 	callback = function()
			-- 		vim.schedule(db_completion)
			-- 	end,
			-- 	group = autocomplete_group,
			-- })
			vim.filetype.add({
				pattern = { [".*-query-[^%.]*"] = "sql" },
			})
			vim.treesitter.language.register("sql", "mysql")
			vim.g.db_ui_use_nerd_fonts = 1
			vim.g.db_ui_show_database_icon = 1
			vim.g.db_ui_use_nvim_notify = 1
			vim.g.db_ui_show_help = 0
			vim.g.db_ui_win_position = 'right'
		end,
	},
	{
		"rest-nvim/rest.nvim",
		ft = "http",
		tag = 'v1.2.1',
		dependencies = {
			{ "nvim-lua/plenary.nvim" },
			{ "amirali/rest-ui.nvim" },
		},
		keys = {
			{
				'<leader>rs',
				mode = { 'v', 'n' },
				"<cmd>Rest run<cr>",
				desc = "Run request under the cursor"
			},
			{
				'<leader>rl',
				mode = { 'v', 'n' },
				"<cmd>Rest run last<cr>",
				desc = "Re-run latest request",
			},
		},
	},
}
return init
