local init = {
	'ron89/thesaurus_query.vim',
	keys = {
		{ '<leader>ts', mode = { 'n' }, ':Thesaurus<Space>',                         desc = 'Search on thesaurus' },
		{ '<leader>tr', mode = { 'n' }, '<cmd>ThesaurusQueryReplaceCurrentWord<CR>', desc = 'Repalace thesaurus' },
		{ '<leader>tl', mode = { 'n' }, '<cmd>ThesaurusQueryLookupCurrentWord<CR>',  desc = 'Lookup thesaurus' },
	},
}

if vim.fn.has('nvim') == 1 then
	return init
end
