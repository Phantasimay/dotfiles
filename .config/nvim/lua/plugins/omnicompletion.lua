local init = {
	'hrsh7th/nvim-cmp',
	event = 'InsertEnter',
	version = false,
	dependencies = {
		{
			'L3MON4D3/LuaSnip',
			version = "v2.*",
			build = "make install_jsregexp",
		},
		{ 'saadparwaiz1/cmp_luasnip', },
		{ 'honza/vim-snippets', },
		{ 'SergioRibera/cmp-dotenv', },
		{
			"chrisgrieser/nvim-scissors",
			opts = {
				snippetDir = "~/.config/nvim/UltiSnips/",
			}
		},
		{ 'rafamadriz/friendly-snippets', },
		{ 'hrsh7th/cmp-nvim-lua', },
		{ 'hrsh7th/cmp-buffer', },
		{ 'hrsh7th/cmp-cmdline', },
		{ 'hrsh7th/cmp-path', },
		{ 'hrsh7th/cmp-emoji', },
		{ 'hrsh7th/cmp-nvim-lsp-signature-help', },
		-- { 'lukas-reineke/cmp-rg', },
		{ 'octaltree/cmp-look', },
		{ 'lukas-reineke/cmp-under-comparator' },
		{ 'onsails/lspkind-nvim', },
		{ 'hrsh7th/cmp-nvim-lsp', },
		{
			'rcarriga/cmp-dap',
			ft = { 'sh', 'typescript', 'javascript' },
		},
		{ 'windwp/nvim-autopairs', },
	},
	config = function()
		require('globals')
		-- [[ Configure Autopairs ]]--------------------------------------------------
		local Autopairs = require('nvim-autopairs')
		Autopairs.setup({
			check_ts = true,
		})

		-- [[ Configure nvim-cmp ]] ---------------------------------------------------
		local Cmp = require('cmp')
		require("luasnip.loaders.from_vscode").lazy_load()
		require("luasnip.loaders.from_vscode").lazy_load({ paths = { "~/.config/nvim/UltiSnips" } })
		require("luasnip.loaders.from_snipmate").lazy_load()
		require("luasnip.loaders.from_snipmate").lazy_load({ paths = { "~/.config/nvim/UltiSnips" } })
		local Luasnip = require('luasnip')
		local Lspkind = require('lspkind')
		local cmp_buffer = require('cmp_buffer')
		vim.api.nvim_set_hl(0, "CmpGhostText", { link = "Comment", default = true })
		Cmp.setup({
			enabled = function()
				return vim.api.nvim_get_option_value("buftype", { buf = 0 }) ~= "prompt"
					or require("cmp_dap").is_dap_buffer()
			end,
			completion = {
				completeopt = 'menu,menuone,preview,noselect',
			},
			snippet = {
				expand = function(args)
					if not Luasnip then
						return
					end
					Luasnip.lsp_expand(args.body)
				end,
			},
			window = {
				completion    = Cmp.config.window.bordered(),
				documentation = Cmp.config.window.bordered(),
			},
			sources = {
				{ name = 'nvim_lsp_signature_help' }, -- Must on top to activate lsp signature help.
				-- { name = 'rg' },
				{ name = 'vim-dadbod-completion' },
				{
					name = 'buffer',
					option = {
						get_bufnrs = function()
							local buf = vim.api.nvim_get_current_buf()
							local byte_size = vim.api.nvim_buf_get_offset(buf,
								vim.api.nvim_buf_line_count(buf))
							if byte_size > 1024 * 1024 then -- 1 Megabyte max
								return {}
							end
							-- return { buf }
							return vim.api.nvim_list_bufs()
						end
					},
				},
				{
					name = 'luasnip',
					option = {
						use_show_condition = false,
						show_autosnippets = true
					}
				},
				{ name = 'friendly_snippets' },
				{ name = 'vim_snippets' },
				{ name = 'nvim_lsp' },
				{ name = 'nvim_lua' },
				{ name = 'path' },
				{ name = "dotenv" },
				{
					name = 'look',
					keyword_length = 2,
					option = {
						convert_case = true,
						loud = true,
						dict = '/usr/share/dict/words'
					}
				},
				{ name = "otter" },
				{ name = 'emoji' },
			},
			formatting = {
				fields = { 'kind', 'abbr' },
				format = Lspkind.cmp_format({
					mode = 'symbol',
					maxwidth = 50,
					ellipsis_char = '...',
					show_labelDetails = true,
					with_text = false,
					-- menu = {
					--  vim-dadbod-completion = "[DB]",
					-- 	rg = '[Rg]',
					-- 	buffer = '[Buffer]',
					-- 	nvim_lsp = '[LSP]',
					-- 	luasnip = '[Snippet]',
					-- 	tags = '[Tag]',
					-- 	path = '[Path]',
					-- 	asciidoc = '[adoc]',
					-- 	look = '[Dict]',
					-- }
				})
			},
			sorting = {
				comparators = {
					function(...) return cmp_buffer:compare_locality(...) end,
					Cmp.config.compare.offset,
					Cmp.config.compare.exact,
					Cmp.config.compare.score,
					require("cmp-under-comparator").under,
					Cmp.config.compare.kind,
					Cmp.config.compare.sort_text,
					Cmp.config.compare.length,
					Cmp.config.compare.order,
				}
			},
			cmdline = {
				{ '/',
					{
						mapping = Cmp.mapping.preset.cmdline(),
						sources = {
							{ name = 'buffer' }
						}
					} },
				{ ':', {
					mapping = Cmp.mapping.preset.cmdline(),
					sources = Cmp.config.sources({
							{ name = 'path' }
						},
						{
							{
								name = 'cmdline',
								option = {
									ignore_cmds = { 'Man', '!' }
								}
							}
						})
				} },
			},
			experimental = {
				ghost_text = {
					hl_group = "CmpGhostText",
				},
			},
			mapping = Cmp.mapping.preset.insert {
				['<C-d>'] = Cmp.mapping.scroll_docs(-4),
				['<C-f>'] = Cmp.mapping.scroll_docs(4),
				['<CR>'] = Cmp.mapping.confirm {
					behavior = Cmp.ConfirmBehavior.Replace,
					select = false,
				},
				['<Tab>'] = Cmp.mapping(function(fallback)
					if Cmp.visible() then
						Cmp.select_next_item()
					elseif Luasnip.expand_or_jumpable() then
						Luasnip.expand_or_jump()
						-- elseif has_words_before() then
						-- 	Cmp.complete()
					else
						fallback()
					end
				end, { 'i', 's' }),
				['<S-Tab>'] = Cmp.mapping(function(fallback)
					if Cmp.visible() then
						Cmp.select_prev_item()
					elseif Luasnip.jumpable(-1) then
						Luasnip.jump(-1)
					else
						fallback()
					end
				end, { 'i', 's' }),
			},
		})

		Cmp.setup.filetype({ "dap-repl", "dapui_watches", "dapui_hover" }, {
			sources = {
				{ name = "dap" },
			},
		})

		-- If you want insert `(` after select function or method item
		local cmp_autopairs = require('nvim-autopairs.completion.cmp')
		Cmp.event:on(
			'confirm_done',
			cmp_autopairs.on_confirm_done()
		)

		-- [[ Cmp ]]
		Im('<C-n>', require('cmp').select_next_item)
		Im('<C-p>', require('cmp').select_prev_item)
		Im('<C-Space>', function() require('cmp').complete {} end)
	end
}

if vim.fn.has('nvim') == 1 then
	return init
end
