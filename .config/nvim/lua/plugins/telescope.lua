local init = {
	-- Fuzzy Finder (files, lsp, etc)
	"nvim-telescope/telescope.nvim",
	branch = "0.1.x",
	dependencies = {
		{
			'nvim-telescope/telescope-fzf-native.nvim',
			build =
			'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build',
			-- build = "make",
			cond = function()
				return vim.fn.executable("cmake") == 1
			end,
		},
		-- { "nvim-telescope/telescope-ui-select.nvim", },
		-- { "nvim-telescope/telescope-file-browser.nvim", },
		{ "ivanjermakov/telescope-file-structure.nvim", },
		-- { "folke/noice.nvim", },
		{ "rest-nvim/rest.nvim", },
	},
	keys = {
		{
			'<leader>sn',
			mode = { 'n' },
			function()
				require("telescope.builtin").find_files({ cwd = vim.fn.stdpath("config") })
			end,
			desc = "[S]earch [N]eovim files"
		},
		-- Enable telescope fzf native, if installed
		{
			"<leader>sF",
			mode = { 'n' },
			function()
				-- You can pass additional configuration to telescope to change theme, layout, etc.
				require("telescope.builtin").current_buffer_fuzzy_find(require("telescope.themes").get_dropdown({
					winblend = 10,
					previewer = false,
				}))
			end,
			desc = "[F]uzzily search in current buffer"
		},
		{
			'<leader><space>',
			mode = { 'n' },
			'<cmd>Telescope file_structure<CR>',
			desc = 'File Structure'
		},
		{
			"<leader>so",
			mode = { 'n' },
			"<cmd>Telescope oldfiles<CR>",
			desc = "[S]earch recently [O]pened files"
		},
		{
			"<leader>sG",
			mode = { 'n' },
			"<cmd>Telescope git_files<CR>",
			desc = "Search [G]it [F]iles"
		},
		{
			"<C-P>",
			mode = { 'n' },
			"<cmd>Telescope find_files<CR>",
			desc = "[S]earch [F]iles"
		},
		{
			'<leader>sz',
			mode = { 'n' },
			function()
				require("telescope.builtin").find_files(require("telescope.themes").get_ivy({
					pwd = "$WIKI_DIR",
					cwd = "$WIKI_DIR",
					sort_mru = true,
					sort_lastused = true,
					initial_mode = "normal",
					layout_config = {
						preview_width = 0.6,
					},
				}))
			end,
			desc = "[S]earch [Z]etelkestein"
		},
		{
			"<leader>sh",
			mode = { 'n' },
			"<cmd>Telescope help_tags<CR>",
			desc = "[S]earch [H]elp"
		},
		{
			"<leader>sw",
			mode = { 'n' },
			"<cmd>Telescope grep_string<CR>",
			desc = "[S]earch current [W]ord"
		},
		{
			"<leader>sg",
			mode = { 'n' },
			function()
				local function is_git_repo()
					vim.fn.system("git rev-parse --is-inside-work-tree")
					return vim.v.shell_error == 0
				end
				local function get_git_root()
					local dot_git_path = vim.fn.finddir(".git", ".;")
					return vim.fn.fnamemodify(dot_git_path, ":h")
				end
				local opts = {}
				if is_git_repo() then
					opts = {
						cwd = get_git_root(),
					}
				end
				require("telescope.builtin").live_grep(opts)
			end,
			desc = "[S]earch by [G]rep"
		},
		{
			"<leader>sd",
			mode = { 'n' },
			"<cmd>Telescope diagnostics<CR>",
			desc = "[S]earch [D]iagnostics"
		},
		{
			"<leader>sk",
			mode = { 'n' },
			"<cmd>Telescope keymaps<CR>",
			desc = "[S]earch [K]eymaps"
		},
		{
			"<leader>sb",
			mode = { 'n' },
			function()
				require("telescope.builtin").buffers(require("telescope.themes").get_ivy({
					sort_mru = true,
					sort_lastused = true,
					initial_mode = "normal",
					layout_config = {
						-- Set preview width, 0.7 sets it to 70% of the window width
						preview_width = 0.6,
					},
				}))
			end,
			desc = "[S]earch [B]uffers"
		},
		{
			"<leader>sR",
			mode = { 'n' },
			"<cmd>Telescope resume<CR>",
			desc = "[S]earch [R]esume"
		},
		{
			"<leader>sr",
			mode = { 'n' },
			function()
				require("telescope").extensions.rest.select_env()
			end,
			desc = "[S]earch [R]EST API"
		},
	},

	config = function()
		require("globals")

		-- [[ Configure Telescope ]]--------------------------------------------------
		local Telescope = require("telescope")
		pcall(Telescope.load_extension, "fzf")
		-- pcall(Telescope.load_extension, "ui-select")
		pcall(Telescope.load_extension, "file_structure")
		pcall(Telescope.load_extension, "noice")
		pcall(Telescope.load_extension, "rest")
		Telescope.setup({
			extensions = {
				-- file_browser = {
				-- 	-- theme = "ivy",
				-- 	hijack_netrw = true,
				-- 	path = "%:p:h",
				-- 	select_buffer = true,
				-- },
				-- ["ui-select"] = {
				-- 	require("telescope.themes").get_dropdown(),
				-- },
				fzf = {
					fuzzy = true,
					override_generic_sorter = true,
					override_file_sorter = true,
					case_mode = "smart_case",
				},
			},
			defaults = {
				vimgrep_arguments = {
					"rg",
					"--color=never",
					"--no-heading",
					"--with-filename",
					"--line-number",
					"--column",
					"--smart-case",
				},
				path_display = { "truncate " },
				path = "%:p:h",
				select_buffer = true,
				mappings = {
					i = {
						["<C-u>"] = false,
						["<C-d>"] = false,
					},
				},
			},
		})
	end,
}

if vim.fn.has("nvim") == 1 then
	return init
end
