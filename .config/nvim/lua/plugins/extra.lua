local init = {
	-- [[Plugins that don't require any configuration]]
	-- Plugin optimization
	{
		"pteroctopus/faster.nvim",
		opts = {},
		event = 'UIEnter',
	},
	-- {
	-- 	"tjdevries/present.nvim",
	-- 	opts = {},
	-- 	event = 'UIEnter',
	-- 	keys = {
	-- 		{
	-- 			'<leader>pp',
	-- 			mode = { 'n' },
	-- 			function()
	-- 				require("present").start_presentation {}
	-- 			end,
	-- 		},
	-- 	},
	-- },
	-- Scroll over EOF
	-- {
	-- 	'Aasim-A/scrollEOF.nvim',
	-- 	event = { 'CursorMoved', 'WinScrolled' },
	-- 	opts = {},
	-- 	config = function()
	-- 		require('scrollEOF').setup()
	-- 	end,
	-- },
	-- Extended textobject
	{
		"kylechui/nvim-surround",
		version = "*",
		opts = {},
		event = 'UIEnter',
	},
	-- Uniq snipplets html builder
	{
		"mattn/emmet-vim",
		ft = { 'html' },
	},
	-- Old code assistent before AI
	{
		"Phantasimay/cheat-sh-nvim",
		branch = 'fix_getn',
		keys = {
			{
				'<S-p>',
				mode = { 'n' },
				':call CheatSheetCommand()<cr>',
			},
			{
				'<A-S-p>',
				mode = { 'n' },
				function()
					vim.cmd(':call CheatSheetCursor()<cr>')
				end,
			},
		},
	},
	-- Competitive Programming tool
	{
		"xeluxee/competitest.nvim",
		opts = {},
		cmd = { "CompetiTest" },
	},
	-- 'gc' to comment visual regions/lines
	{
		"numToStr/Comment.nvim",
		opts = {},
		keys = {
			{
				'gcc',
				mode = { 'n' },
				function()
					require('comment').toggle()
				end,
			},
			{
				'gc',
				mode = { 'v' },
				function()
					require('comment').toggle()
				end,
			},
		},
	},
	{
		"https://git.sr.ht/~jcc/vim-sway-nav",
		config = function()
			require('vim-sway-nav').setup()
		end,
	},
	{
		"alexghergh/nvim-tmux-navigation",
		event = function()
			if vim.fn.exists('$TMUX') == 1 then
				return 'VeryLazy'
			end
		end,
		config = function()
			require("globals")
			local Nav = require("nvim-tmux-navigation")

			Nav.setup({
				disable_when_zoomed = true, -- defaults to false
			})

			TNm("<C-h>", Nav.NvimTmuxNavigateLeft)
			TNm("<C-j>", Nav.NvimTmuxNavigateDown)
			TNm("<C-k>", Nav.NvimTmuxNavigateUp)
			TNm("<C-l>", Nav.NvimTmuxNavigateRight)
			TNm("<C-\\>", Nav.NvimTmuxNavigateLastActive)
			TNVm("<C-Space>", Nav.NvimTmuxNavigateNext)
		end,
	},
}

if vim.fn.has("nvim") == 1 then
	return init
end
