local init = {
	"mfussenegger/nvim-dap",
	event = "VeryLazy",
	-- ft = { 'sh', 'typescript', 'javascript' },
	dependencies = {
		{
			"jay-babu/mason-nvim-dap.nvim",
			opts = { automatic_installation = true },
			cmd = { "DapInstall", "DapUninstall" },
		},
		{ "nvim-neotest/neotest", },
		{ "antoinemadec/FixCursorHold.nvim", },
		{ "theHamsta/nvim-dap-virtual-text",   opts = {} },
		{ "rcarriga/nvim-dap-ui",              opts = {} },
		{ "jbyuki/one-small-step-for-vimkind", },
		-- Neovim signature Library
		-- {
		-- 	"folke/neodev.nvim",
		-- 	opts = {
		-- 		library = {
		-- 			plugins = { "nvim-dap-ui" },
		-- 			types = true
		-- 		},
		-- 	},
		-- },
	},
	-- opts = {},
	keys = {
		{
			"<leader>du",
			mode = { 'n' },
			function()
				require("dapui").toggle()
			end,
			desc = 'Toggle Debuger UI',
		},
		{
			"<leader>dk",
			mode = { 'n' },
			function()
				require("dapui").eval()
			end,
			desc = "Hover Evaluate Expression",
		},
		{
			"<leader>dc",
			mode = { 'n' },
			function()
				require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '))
			end,
			desc = 'Toggle Conditional BP',
		},
		{
			"<leader>dl",
			mode = { 'n' },
			function()
				require('dap').set_breakpoint(nill, nill, vim.fn.input('Log point messages : '))
			end,
			desc = "View [L]og [P]oint",
		},
		{
			"<leader>dx",
			mode = { 'n' },
			function()
				require("dap").toggle_breakpoint()
			end,
			desc = "Toggle [B]reak [P]oint",
		},
		{
			"<leader>dr",
			mode = { 'n' },
			function()
				require("dap").repl.open()
			end,
			desc = "[O]pen REPL",
		},
		{
			"<F9>",
			mode = { 'n' },
			function()
				require("dap").step_out()
			end,
			desc = "Step Out",
		},
		{
			"<F10>",
			mode = { 'n' },
			function()
				require("dap").step_over()
			end,
			desc = "Step Over",
		},
		{
			"<F11>",
			mode = { 'n' },
			function()
				require("dap").step_into()
			end,
			desc = "Step Into",
		},
		{
			"<F12>",
			mode = { 'n' },
			function()
				require("dap").continue()
			end,
			desc = "Start Debugging",
		},
	},
	config = function()
		-- [[ Debugger Adapter Protocol ]] ---------------------------------------------------
		local dap = require("dap")
		local dapui = require("dapui")
		dap.listeners.after.event_initialized["dapui_config"] = function()
			dapui.open()
		end
		dap.listeners.before.launch.dapui_config = function()
			dapui.open({ reset = true })
		end
		dap.listeners.before.event_terminated.dapui_config = function()
			dapui.close({ reset = true })
		end
		dap.listeners.before.event_exited.dapui_config = function()
			dapui.close()
		end
		vim.api.nvim_set_hl(0, "DapStoppedLine", { default = true, link = "Visual" })
		vim.fn.sign_define('DapBreakpoint', { text = ' ', texthl = '', linehl = '', numhl = '' })

		-- Firefox Js debuger
		dap.adapters.firefox = {
			type = "executable",
			command = "node",
			-- args = { os.getenv('HOME') .. '/path/to/vscode-firefox-debug/dist/adapter.bundle.js' },
			args = { vim.fn.stdpath("data") .. "/mason/packages/vscode-firefox-debug/dist/adapter.bundle.js" },
		}

		-- Chrome Js debuger
		dap.adapters.chrome = {
			type = "executable",
			command = "node",
			-- args = { os.getenv("HOME") .. "/mason/packages/vscode-chrome-debug/out/src/chromeDebug.js" } -- TODO adjust
			args = { vim.fn.stdpath("data") .. "/mason/packages/vscode-chrome-debug/out/src/chromeDebug.js" },
		}

		-- Vscode Js debuger
		dap.adapters["pwa-node"] = {
			type = "server",
			host = "localhost",
			port = "${port}",
			executable = {
				command = "node",
				-- 💀 Make sure to update this path to point to your installation
				args = {
					vim.fn.stdpath("data") .. "/mason/packages/js-debug-adapter/js-debug/src/dapDebugServer.js",
					"${port}",
				},
			},
		}

		local js_based_languages = { "typescript", "javascript", "typescriptreact" }
		for _, language in ipairs(js_based_languages) do
			dap.configurations[language] = {
				{
					type = "pwa-node",
					request = "launch",
					name = "Launch Node Debugger",
					program = "${file}",
					cwd = "${workspaceFolder}",
				},
				{
					type = "pwa-node",
					request = "launch",
					name = "Launch Deno Debugger",
					runtimeExecutable = "deno",
					runtimeArgs = {
						"run",
						"--inspect-wait",
						"--allow-all",
					},
					program = "${file}",
					cwd = "${workspaceFolder}",
					attachSimplePort = 9229,
				},
				{
					type = "pwa-node",
					request = "launch",
					name = "Launch Jest Debugger",
					trace = true, -- include debugger info
					runtimeExecutable = "node",
					runtimeArgs = {
						"./node_modules/jest/bin/jest.js", -- Make sure to update this path to point to your installation
						"--runInBand",
					},
					rootPath = "${workspaceFolder}",
					cwd = "${workspaceFolder}",
					console = "integratedTerminal",
					internalConsoleOptions = "neverOpen",
				},
				{
					type = "pwa-node",
					request = "launch",
					name = "Launch Mocha Debugger",
					trace = true, -- include debugger info
					runtimeExecutable = "node",
					runtimeArgs = {
						"./node_modules/mocha/bin/mocha.js", -- Make sure to update this path to point to your installation
					},
					rootPath = "${workspaceFolder}",
					cwd = "${workspaceFolder}",
					console = "integratedTerminal",
					internalConsoleOptions = "neverOpen",
				},
				{
					type = "pwa-node",
					request = "attach",
					name = "Attach Js to Debugger",
					processId = require("dap.utils").pick_process,
					cwd = "${workspaceFolder}",
				},
				{
					name = "Debug with Chrome",
					type = "chrome",
					request = "attach",
					program = "${file}",
					cwd = vim.fn.getcwd(),
					sourceMaps = true,
					protocol = "inspector",
					port = 9222,
					webRoot = "${workspaceFolder}",
					-- url = "http://localhost:3000",
					--  userDataDir = "${workspaceFolder}/.vscode/vscode-chrome-debug-userdatadir"
				},
				{
					name = "Debug with Firefox",
					type = "firefox",
					request = "launch",
					reAttach = true,
					url = "http://localhost:3000",
					webRoot = "${workspaceFolder}",
					firefoxExecutable = "/usr/bin/firefox",
				},
			}
		end

		-- Ansible Debugger
		-- install ansibug 'python -m pip install ansibug'
		dap.adapters.ansible = {
			type = "executable",
			command = "python", -- or "/path/to/virtualenv/bin/python",
			args = { "-m", "ansibug", "dap" },
		}

		dap.configurations["yaml.ansible"] =
		{ -- You may need to replace "yaml.ansible" with the filetype you use for ansible playbooks
			{
				type = "ansible",
				request = "launch",
				name = "Debug with ansibug playbook",
				playbook = "${file}",
			},
		}

		-- Python Debugger
		dap.adapters.python = function(cb, config)
			if config.request == "attach" then
				---@diagnostic disable-next-line: undefined-field
				local port = (config.connect or config).port
				---@diagnostic disable-next-line: undefined-field
				local host = (config.connect or config).host or "127.0.0.1"
				cb({
					type = "server",
					port = assert(port, "`connect.port` is required for a python `attach` configuration"),
					host = host,
					options = {
						source_filetype = "python",
					},
				})
			else
				cb({
					type = "executable",
					command = "/mason/packages/virtualenvs/debugpy/bin/python",
					args = { "-m", "debugpy.adapter" },
					options = {
						source_filetype = "python",
					},
				})
			end
		end

		dap.configurations.python = {
			{
				-- The first three options are required by nvim-dap
				type = "python", -- the type here established the link to the adapter definition: `dap.adapters.python`
				request = "launch",
				name = "Launch Debugpy",
				-- command = os.getenv("VIRTUAL_ENV") .. "/bin/python",
				-- Options below are for debugpy, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for supported options

				program = "${file}", -- This configuration will launch the current file if used.
				pythonPath = function()
					-- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
					-- The code below looks for a `venv` or `.venv` folder in the current directly and uses the python within.
					-- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
					local cwd = vim.fn.getcwd()
					if vim.fn.executable(cwd .. "/venv/bin/python") == 1 then
						return cwd .. "/venv/bin/python"
					elseif vim.fn.executable(cwd .. "/.venv/bin/python") == 1 then
						return cwd .. "/.venv/bin/python"
					else
						return "/usr/bin/python"
					end
				end,
			},
		}

		-- Brightscript Debugger
		-- install roku-debug 'npm install roku-debug'
		-- Set the ROKU_DEV_TARGET and DEVPASSWORD environment variables to the IP address and password of your Roku device
		dap.adapters.brightscript = {
			type = "executable",
			command = "npx",
			args = { "roku-debug", "--dap" },
		}

		dap.configurations.brs = {
			{
				type = "brightscript",
				request = "launch",
				name = "launch Rokudebug",
				rootDir = "${workspaceFolder}",
				files = {
					"manifest",
					"source/**/*.*",
					"components/**/*.*",
					"images/**/*.*",
					"locale/**/*.*",
				},
				host = "${env:ROKU_DEV_TARGET}",
				remotePort = 8060,
				password = "${env:DEVPASSWORD}",
				outDir = "${workspaceFolder}/out/",
				enableDebugProtocol = true,
				fileLogging = false,
				enableVariablesPanel = true,
				logLevel = "off",
			},
		}

		-- C, C++, Rust Debugger(gdb)
		-- Requires gdb 14.0+
		dap.adapters.gdb = {
			type = "executable",
			command = "gdb",
			args = { "-i", "dap" },
		}

		-- C, C++, Rust Debugger(gdb)
		-- Requires codelldb 1.7.0+
		dap.adapters.codelldb = {
			type = "server",
			port = "${port}",
			executable = {
				-- CHANGE THIS to your path!
				command = vim.env.MASON .. "/packages/codelldb/codelldb", -- use absolute path
				args = { "--port", "${port}" },

				-- On windows you may have to uncomment this:
				-- detached = false,
			},
		}

		-- C, C++, Rust Debugger(lldb-vscode)
		-- Requires llvm
		dap.adapters.lldb = {
			type = "executable",
			command = "/usr/bin/lldb", -- adjust as needed, must be absolute path
			name = "lldb",
		}

		-- C, C++, Rust Debugger(cpptools)
		dap.adapters.cppdbg = {
			id = "cppdbg",
			type = "executable",
			command = vim.env.MASON .. "/packages/cpptools/extension/debugAdapters/bin/OpenDebugAD7", -- use absolute path
		}

		local c_based_languages = { "c", "cpp", "rust", "zig" }
		for _, language in ipairs(c_based_languages) do
			dap.configurations[language] = {
				{
					name = "Debug with Gdb",
					type = "gdb",
					request = "launch",
					program = function()
						return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
					end,
					cwd = "${workspaceFolder}",
					stopAtBeginningOfMainSubprogram = false,
				},
				{
					name = "Debug with Cpptools",
					type = "cppdbg",
					request = "launch",
					program = function()
						return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
					end,
					cwd = "${workspaceFolder}",
					stopAtEntry = true,
					setupCommands = {
						{
							text = "-enable-pretty-printing",
							description = "enable pretty printing",
							ignoreFailures = false,
						},
					},
				},
				{
					name = "Attach to gdbserver :1234",
					type = "cppdbg",
					request = "launch",
					MIMode = "gdb",
					miDebuggerServerAddress = "localhost:1234",
					miDebuggerPath = "/usr/bin/gdb",
					cwd = "${workspaceFolder}",
					program = function()
						return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
					end,
					setupCommands = {
						{
							text = "-enable-pretty-printing",
							description = "enable pretty printing",
							ignoreFailures = false,
						},
					},
				},
				{
					name = "Debug with Codelldb",
					type = "codelldb",
					request = "launch",
					program = function()
						return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
					end,
					cwd = "${workspaceFolder}",
					stopOnEntry = false,
				},
				{
					name = "Debug with Lldb",
					type = "lldb",
					request = "launch",
					program = function()
						return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
					end,
					cwd = "${workspaceFolder}",
					stopOnEntry = false,
					args = {},
					env = function()
						local variables = {}
						for k, v in pairs(vim.fn.environ()) do
							table.insert(variables, string.format("%s=%s", k, v))
						end
						return variables
					end,
					initCommands = function()
						-- Find out where to look for the pretty printer Python module
						local rustc_sysroot = vim.fn.trim(vim.fn.system("rustc --print sysroot"))

						local script_import = 'command script import "'
							.. rustc_sysroot
							.. '/lib/rustlib/etc/lldb_lookup.py"'
						local commands_file = rustc_sysroot .. "/lib/rustlib/etc/lldb_commands"

						local commands = {}
						local file = io.open(commands_file, "r")
						if file then
							for line in file:lines() do
								table.insert(commands, line)
							end
							file:close()
						end
						table.insert(commands, 1, script_import)

						return commands
					end,
					-- if you change `runInTerminal` to true, you might need to change the yama/ptrace_scope setting:
					--
					--    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
					--
					-- Otherwise you might get the following error:
					--
					--    Error on launch: Failed to attach to the target process
					--
					-- But you should be aware of the implications:
					-- https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
					-- runInTerminal = false,
				},
			}
		end

		-- Markdown Debugger
		-- Need install Mockdebug 'https://github.com/Microsoft/vscode-mock-debug.git'
		dap.adapters.markdown = {
			type = "executable",
			name = "mockdebug",
			command = "node",
			args = { "./out/debugAdapter.js" },
			cwd = "path/to/vscode-mock-debug/",
		}

		dap.configurations.markdown = {
			{
				type = "mock",
				request = "launch",
				name = "Debug with mockdebug",
				program = "/path/to/a/readme.md",
				stopOnEntry = true,
				debugServer = 4711,
			},
		}

		-- Go Debugger
		-- Need install vscode-go extension 'https://github.com/golang/vscode-go'
		-- or nvim-dap-go plugin 'https://github.com/leoluz/nvim-dap-go'
		dap.adapters.go = {
			type = "executable",
			command = "node",
			-- args = { os.getenv('HOME') .. '/dev/golang/vscode-go/dist/debugAdapter.js' },
			args = { vim.fn.stdpath("data") .. "/mason/packages/vscode-go/dist/debugAdapter.js" },
		}

		dap.adapters.delve = {
			type = "server",
			port = "${port}",
			executable = {
				command = "dlv",
				args = { "dap", "-l", "127.0.0.1:${port}" },
			},
		}

		-- https://github.com/go-delve/delve/blob/master/Documentation/usage/dlv_dap.md
		dap.configurations.go = {
			{
				type = "go",
				name = "Debug via vscode-go",
				request = "launch",
				showLog = false,
				program = "${file}",
				dlvToolPath = vim.fn.exepath("dlv"), -- Adjust to where delve is installed
			},
			{
				type = "delve",
				name = "Debug with devel",
				request = "launch",
				program = "${file}",
			},
			{
				type = "delve",
				name = "Debug with delve test", -- configuration for debugging test files
				request = "launch",
				mode = "test",
				program = "${file}",
			},
			-- works with go.mod packages and sub packages
			{
				type = "delve",
				name = "Debug test (go.mod)",
				request = "launch",
				mode = "test",
				program = "./${relativeFileDirname}",
			},
		}

		-- Ruby Debugger
		-- Need installed rdbg 'https://github.com/ruby/debug'
		-- dap.adapters.rdbg = function(callback, config)
		-- 	callback({
		-- 		type = "server",
		-- 		host = "127.0.0.1",
		-- 		port = "${port}",
		-- 		executable = {
		-- 			command = "bundle",
		-- 			args = {
		-- 				"exec",
		-- 				"rdbg",
		-- 				"-n",
		-- 				"--open",
		-- 				"--port",
		-- 				"${port}",
		-- 				"-c",
		-- 				"--",
		-- 				"bundle",
		-- 				"exec",
		-- 				config.command,
		-- 				config.script,
		-- 			},
		-- 		},
		-- 	})
		-- end
		-- -- Need installed readapt 'gem install readapt'
		-- dap.adapters.ruby = {
		-- 	type = "executable",
		-- 	command = "bundle",
		-- 	args = { "exec", "readapt", "stdio" },
		-- }
		-- dap.configurations.ruby = {
		-- 	{
		-- 		type = "ruby",
		-- 		name = "debug with rdbg",
		-- 		request = "attach",
		-- 		localfs = true,
		-- 		command = "ruby",
		-- 		script = "${file}",
		-- 	},
		-- 	{
		-- 		type = "ruby",
		-- 		name = "run rdbg spec",
		-- 		request = "attach",
		-- 		localfs = true,
		-- 		command = "rspec",
		-- 		script = "${file}",
		-- 	},
		-- 	{
		-- 		type = "ruby",
		-- 		request = "launch",
		-- 		name = "Debug with Rails",
		-- 		program = "bundle",
		-- 		programArgs = { "exec", "rails", "s" },
		-- 		useBundler = true,
		-- 	},
		-- }

		-- Dart/Flutter Debugger
		-- dap.adapters.dart = {
		-- 	type = "executable",
		-- 	command = "dart",
		-- 	args = { "debug_adapter" },
		-- }
		-- dap.adapters.flutter = {
		-- 	type = "executable",
		-- 	command = "flutter",
		-- 	args = { "debug_adapter" },
		-- }
		-- dap.configurations.dart = {
		-- 	{
		-- 		type = "dart",
		-- 		request = "launch",
		-- 		name = "Launch dart",
		-- 		dartSdkPath = "/opt/flutter/bin/cache/dart-sdk/bin/dart", -- ensure this is correct
		-- 		flutterSdkPath = "/opt/flutter/bin/flutter",  -- ensure this is correct
		-- 		program = "${workspaceFolder}/lib/main.dart", -- ensure this is correct
		-- 		cwd = "${workspaceFolder}",
		-- 	},
		-- 	{
		-- 		type = "flutter",
		-- 		request = "launch",
		-- 		name = "Launch flutter",
		-- 		dartSdkPath = "/opt/flutter/bin/cache/dart-sdk/bin/dart", -- ensure this is correct
		-- 		flutterSdkPath = "/opt/flutter/bin/flutter",  -- ensure this is correct
		-- 		program = "${workspaceFolder}/lib/main.dart", -- ensure this is correct
		-- 		cwd = "${workspaceFolder}",
		-- 	},
		-- }

		-- Haskell Debugger
		-- dap.adapters.haskell = {
		-- 	type = "executable",
		-- 	command = "haskell-debug-adapter",
		-- 	args = { "--hackage-version=0.0.33.0" },
		-- }
		-- dap.configurations.haskell = {
		-- 	{
		-- 		type = "haskell",
		-- 		request = "launch",
		-- 		name = "Debug",
		-- 		workspace = "${workspaceFolder}",
		-- 		startup = "${file}",
		-- 		stopOnEntry = true,
		-- 		logFile = vim.fn.stdpath("data") .. "/haskell-dap.log",
		-- 		logLevel = "WARNING",
		-- 		ghciEnv = vim.empty_dict(),
		-- 		ghciPrompt = " : ",
		-- 		-- Adjust the prompt to the prompt you see when you invoke the stack ghci command below
		-- 		ghciInitialPrompt = " : ",
		-- 		ghciCmd = "stack ghci --test --no-load --no-build --main-is TARGET --ghci-options -fprint-evld-with-show",
		-- 	},
		-- }

		-- PHP Debugger
		dap.adapters.php = {
			type = "executable",
			command = "node",
			args = { "/mason/packages/vscode-php-debug/out/phpDebug.js" },
		}

		dap.configurations.php = {
			{
				type = "php",
				request = "launch",
				name = "Debug with Xdebug",
				port = 9000,
			},
		}

		-- Perl Debugger
		dap.adapters.perl = {
			type = "executable",
			command = vim.env.MASON .. "/packages/perl-debug-adapter/perl-debug-adapter",
			args = {},
		}

		dap.adapters.perlsp = {
			type = "server",
			host = "127.0.0.1",
			port = "27011",
		}

		dap.configurations.perl = {
			{
				type = "perl",
				request = "launch",
				name = "Launch Perl Debuger",
				program = "${workspaceFolder}/${relativeFile}",
			},
			{
				name = "Debugger with Perl LSP",
				type = "perlsp",
				request = "launch",
				program = "${workspaceFolder}/${relativeFile}",
				reloadModules = true,
				stopOnEntry = false,
				cwd = "${workspaceFolder}",
			},
		}
		-- this is optional but can be helpful when starting out
		dap.set_log_level("TRACE")
		vim.opt.iskeyword:append({ "$", "@-@", "%" })

		-- Kotlin Debugger
		dap.adapters.kotlin = {
			type = "executable",
			command = "kotlin-debug-adapter",
			options = { auto_continue_if_many_stopped = false },
		}

		dap.configurations.kotlin = {
			{
				type = "kotlin",
				request = "launch",
				name = "This file",
				-- may differ, when in doubt, whatever your project structure may be,
				-- it has to correspond to the class file located at `build/classes/`
				-- and of course you have to build before you debug
				mainClass = function()
					local root = vim.fs.find("src", { path = vim.uv.cwd(), upward = true, stop = vim.env.HOME })[1]
						or ""
					local fname = vim.api.nvim_buf_get_name(0)
					-- src/main/kotlin/websearch/Main.kt -> websearch.MainKt
					return fname:gsub(root, ""):gsub("main/kotlin/", ""):gsub(".kt", "Kt"):gsub("/", "."):sub(2, -1)
				end,
				projectRoot = "${workspaceFolder}",
				jsonLogFile = "",
				enableJsonLogging = false,
			},
			{
				-- Use this for unit tests
				-- First, run
				-- ./gradlew --info cleanTest test --debug-jvm
				-- then attach the debugger to it
				type = "kotlin",
				request = "attach",
				name = "Attach to debugging session",
				port = 5005,
				args = {},
				projectRoot = vim.fn.getcwd,
				hostName = "localhost",
				timeout = 2000,
			},
		}

		-- Lua Debugger
		dap.adapters.nlua = function(callback, config)
			callback({ type = "server", host = config.host or "127.0.0.1", port = config.port or 8086 })
		end

		-- Need installed local-lua-debugger-vscode 'npm install local-lua-debugger-vscode'
		dap.adapters["local-lua"] = {
			type = "executable",
			command = "node",
			args = {
				-- check with 'npm list -g'
				"/absolute/path/to/local-lua-debugger-vscode/extension/debugAdapter.js",
			},
			enrich_config = function(config, on_config)
				if not config["extensionPath"] then
					local c = vim.deepcopy(config)
					-- \U0001f480 If this is missing or wrong you'll see
					-- "module 'lldebugger' not found" errors in the dap-repl when trying to launch a debug session
					c.extensionPath = "/absolute/path/to/local-lua-debugger-vscode/"
					on_config(c)
				else
					on_config(config)
				end
			end,
		}

		dap.configurations.lua = {
			{
				type = "nlua",
				request = "attach",
				name = "Attach to running Neovim instance",
			},
		}

		-- OCamel Debugger
		-- Need installed earlybird 'opam install earlybird'
		-- Change to (lang dune 3.7) or above and add (map_workspace_root false) to your dune-project.
		-- dap.adapters.ocamlearlybird = {
		-- 	type = "executable",
		-- 	command = "ocamlearlybird",
		-- 	args = { "debug" },
		-- }
		-- dap.configurations.ocaml = {
		-- 	{
		-- 		name = "OCaml Debug test.bc",
		-- 		type = "ocamlearlybird",
		-- 		request = "launch",
		-- 		program = "${workspaceFolder}/_build/default/test/test.bc",
		-- 	},
		-- 	{
		-- 		name = "OCaml Debug main.bc",
		-- 		type = "ocamlearlybird",
		-- 		request = "launch",
		-- 		program = "${workspaceFolder}/_build/default/bin/main.bc",
		-- 	},
		-- }

		-- DotNet Debugger
		-- Need installed netcoredbg 'https://github.com/Samsung/netcoredbg'
		-- dap.adapters.coreclr = {
		-- 	type = "executable",
		-- 	command = "/path/to/dotnet/netcoredbg/netcoredbg",
		-- 	args = { "--interpreter=vscode" },
		-- }
		-- dap.configurations.cs = {
		-- 	{
		-- 		type = "coreclr",
		-- 		name = "launch - netcoredbg",
		-- 		request = "launch",
		-- 		program = function()
		-- 			return vim.fn.input("Path to dll", vim.fn.getcwd() .. "/bin/Debug/", "file")
		-- 		end,
		-- 	},
		-- }

		-- Unity Debugger
		-- Need installed Unity debugging 'https://github.com/Unity-Technologies/vscode-unity-debug'
		-- Need installed mono 'https://github.com/mono/mono'
		-- dap.adapters.unity = {
		-- 	type = "executable",
		-- 	command = "/usr/bin/mono",
		-- 	args = { "<path-to-unity-debug-directory>/unity.unity-debug-x.x.x/bin/UnityDebug.exe" },
		-- }
		-- dap.configurations.cs = {
		-- 	{
		-- 		type = "unity",
		-- 		request = "attach",
		-- 		name = "Unity Editor",
		-- 	},
		-- }

		-- Elixer Debugger
		dap.adapters.mix_task = {
			type = "executable",
			command = vim.fn.stdpath("data") .. '/mason/packages/elixir-ls/debug_adapter.sh', -- debugger.bat for windows
			args = {},
		}
		dap.configurations.elixir = {
			{
				type = "mix_task",
				name = "Debug with Elixer LSP",
				task = "test",
				taskArgs = { "--trace" },
				request = "launch",
				startApps = true, -- for Phoenix projects
				projectDir = "${workspaceFolder}",
				requireFiles = {
					"test/**/test_helper.exs",
					"test/**/*_test.exs",
				},
			},
		}

		-- Godot Debuger
		-- Need install godot v4.0+ and running.
		dap.adapters.godot = {
			type = "server",
			host = "127.0.0.1",
			port = 6006,
			-- The port must match the Godot setting. Go to Editor -> Editor Settings, then find Debug Adapter under Network.
		}
		dap.configurations.gdscript = {
			{
				type = "godot",
				request = "launch",
				name = "Debug with Godot",
				project = "${workspaceFolder}",
				launch_scene = true,
			},
		}

		-- Bash Debugger
		dap.adapters.bashdb = {
			type = "executable",
			command = vim.fn.stdpath("data") .. "/mason/packages/bash-debug-adapter/bash-debug-adapter",
			name = "bashdb",
		}
		dap.configurations.sh = {
			{
				type = "bashdb",
				request = "launch",
				name = "Debug Sh with node",
				showDebugOutput = true,
				pathBashdb = vim.fn.stdpath("data") .. "/mason/packages/bash-debug-adapter/extension/bashdb_dir/bashdb",
				pathBashdbLib = vim.fn.stdpath("data") .. "/mason/packages/bash-debug-adapter/extension/bashdb_dir",
				trace = true,
				file = "${file}",
				program = "${file}",
				cwd = "${workspaceFolder}",
				pathCat = "cat",
				pathBash = "/bin/bash",
				pathMkfifo = "mkfifo",
				pathPkill = "pkill",
				args = {},
				env = {},
				terminalKind = "integrated",
			},
		}
	end,
}

if vim.fn.has("nvim") == 1 then
	return init
end
