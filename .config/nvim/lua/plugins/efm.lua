local init = {
	'creativenull/efmls-configs-nvim',
	enabled = false,
	version = 'v1.x.x',
	dependences = { "neovim/nvim-lspconfig", },
	config = function()
		-- require('plugins.lsp')
		local languages = require('efmls-configs.defaults').languages()
		languages = vim.tbl_extend('force', languages, {
			-- Custom languages, or override existing ones
			sh = {
				require('efmls-configs.formatters.shfmt'),
				require('efmls-configs.linters.shellcheck')
			},
			vim = { require('efmls-configs.linters.vint') },
			markdown = {
				require('efmls-configs.formatters.prettier_d'),
				require('efmls-configs.linters.markdownlint')
			}
		})
		local efmls_config = {
			filetypes = vim.tbl_keys(languages),
			init_options = {
				documentFormatting = true,
				documentRangeFormatting = true,
				hover = true,
				documentSymbol = true,
				codeAction = true,
				completion = true
			},
			settings = {
				rootMarkers = { ".git/" },
				languages = languages,
				lua = { diagnostics = { globals = { "vim" } } }
			}
		}
		require('lspconfig').efm.setup(vim.tbl_extend('force', efmls_config, {
			settings = {
				Lua = {
					diagnostics = {
						-- Get the language server to recognize the `vim` global
						globals = { 'vim' },
					},
				},
			},
		}))
		local lsp_fmt_group = vim.api.nvim_create_augroup(
			'LspFormattingGroup', {})
		vim.api.nvim_create_autocmd('BufWritePost', {
			group = lsp_fmt_group,
			callback = function(ev)
				local efm = vim.lsp.get_clients({
					name = 'efm',
					bufnr = ev.buf
				})
				if vim.tbl_isempty(efm) then
					return
				end
				vim.lsp.buf.format({ name = 'efm' })
			end
		})
		-- capabilities.textDocument.completion.completionItem.snippetSupport = true
		-- capabilities.textDocument.completion.completionItem.resolveSupport = {
		-- 	properties = {
		-- 		"documentation",
		-- 		"detail",
		-- 		"additionalTextEdits",
		-- 	},
		-- }

		-- Give me rounded borders everywhere
		-- local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
		-- function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
		-- 	opts = opts or {}
		-- 	opts.border = "rounded"
		-- 	return orig_util_open_floating_preview(contents, syntax, opts, ...)
		-- end

		-- Prevents formatter and linter conflict between efm with native LSP
		local nulls = {
			"vimls",
			"bashls",
			"cssls",
			"jsonls",
			"html",
			"tsserver",
			"lua_ls",
		}
		for _, server in ipairs(nulls) do
			require("lspconfig")[server].setup({
				handlers = {
					["textDocument/signatureHelp"] = function(...) end,
					["textDocument/publishDiagnostics"] = function(...) end,
					["textDocument/formatting"] = function(...) end,
					["textDocument/rangeFormatting "] = function(...) end,
				},
			})
		end
	end
}

return init
