-- Alias for function, that set new keybindings
-- local map = vim.api.nvim_set_keymap
local Key = vim.keymap.set; -- Command keymap
Cmd = vim.cmd;              -- Command function
Api = vim.api;              -- Neovim API
Lsp = vim.lsp;              -- LSP API
Fn = vim.fn;                -- Vim function
G = vim.g;                  -- Vim globals
Opt = vim.opt;              -- Vim optional

-- Normal expr mode keybinding setter
function Nmx(key, command, desc)
	Key('n', key, command, { expr = true, silent = true, desc = desc })
end

-- Input expr mode keybinding setter
function Imx(key, command, desc)
	Key('i', key, command, { expr = true, silent = true, desc = desc })
end

-- Visual expr mode keybinding setter
function Vmx(key, command, desc)
	Key('v', key, command, { expr = true, silent = true, desc = desc })
end

-- Terminal expr mode keybinding setter
function Tmx(key, command, desc)
	Key('t', key, command, { expr = true, silent = true, desc = desc })
end

-- Normal mode keybinding setter
function Nmf(key, func, desc)
	Key('n', key, func, { noremap = true, desc = desc })
end

-- Normal mode keybinding setter
function Nm(key, command, desc)
	Key('n', key, command, { noremap = true, desc = desc })
end

-- Input mode keybinding setter
function Im(key, command, desc)
	Key('i', key, command, { noremap = true, desc = desc })
end

-- Visual mode keybinding setter
function Vm(key, command, desc)
	Key('v', key, command, { noremap = true, desc = desc })
end

-- Terminal mode keybinding setter
function Tm(key, command, desc)
	Key('t', key, command, { noremap = true, desc = desc })
end

-- Visual mode keybinding setter
function NVm(key, command, desc)
	Key({ 'v', 'n' }, key, command, { noremap = true, desc = desc })
end

-- Visual and Imput mode keybinding setter
function NIm(key, command, desc)
	Key({ 'n', 'i' }, key, command, { noremap = true, desc = desc })
end

-- Visual and Imput mode keybinding setter
function VIm(key, command, desc)
	Key({ 'v', 'i' }, key, command, { noremap = true, desc = desc })
end

-- Visual and Imput mode keybinding setter
function NVIm(key, command, desc)
	Key({ 'n', 'v', 'i' }, key, command, { noremap = true, desc = desc })
end

-- Terminal mode keybinding setter
function TNm(key, command, desc)
	Key({ 't', 'n' }, key, command, { noremap = true, desc = desc })
end

-- Input mode keybinding setter
function TIm(key, command, desc)
	Key({ 't', 'i' }, key, command, { noremap = true, desc = desc })
end

-- Visual mode keybinding setter
function TVm(key, command, desc)
	Key({ 't', 'v' }, key, command, { noremap = true, desc = desc })
end

-- Visual mode keybinding setter
function TNVm(key, command, desc)
	Key({ 't', 'v', 'n' }, key, command, { noremap = true, desc = desc })
end

-- Visual and Imput mode keybinding setter
function TNIm(key, command, desc)
	Key({ 't', 'n', 'i' }, key, command, { noremap = true, desc = desc })
end

-- Visual and Imput mode keybinding setter
function TVIm(key, command, desc)
	Key({ 't', 'v', 'i' }, key, command, { noremap = true, desc = desc })
end

-- Visual and Imput mode keybinding setter
function TNVIm(key, command, desc)
	Key({ 't', 'n', 'v', 'i' }, key, command, { noremap = true, desc = desc })
end
