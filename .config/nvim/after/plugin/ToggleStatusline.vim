let s:hidden_all = 0
function! ToggleStatusline()
    if s:hidden_all  == 0
        let s:hidden_all = 1
        set showmode
        set ruler
        set rnu nu
        set laststatus=2
        set showcmd
		execute '!tmux set -g status on'
		if has('autocmd')
			augroup statusline
			autocmd!
				" Auto relativenumber
				au InsertEnter * set norelativenumber
				au InsertLeave * set relativenumber
			augroup END
		endif
    else
        let s:hidden_all = 0
        set noshowmode
        set nornu nonu
        set noruler
        set laststatus=0
        set noshowcmd
		execute '!tmux set -g status off'
		if has('autocmd')
			augroup statusline
			autocmd!
				" Auto relativenumber
				au InsertLeave * set nornu
			augroup END
		endif
    endif
endfunction

nnoremap <silent> <leader>ls :call ToggleStatusline()<CR>
