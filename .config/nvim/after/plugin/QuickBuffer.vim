function! QuickBuffer(pattern) abort
  if empty(a:pattern)
    call feedkeys(":LeaderBuffer \<C-d>")
    return
  elseif a:pattern is '*'
    call feedkeys(":ls!\<cr>:LeaderBuffer ")
    return
  elseif a:pattern =~ '^\d\+$'
    execute 'buffer' a:pattern
    return
  endif
  let l:globbed = '*' . join(split(a:pattern, ' '), '*') . '*'
  try
    execute 'buffer' l:globbed
  catch
    call feedkeys(':LeaderBuffer ' . l:globbed . "\<C-d>\<C-u>B " . a:pattern)
  endtry
endfun

command! -nargs=* -complete=buffer LeaderBuffer call QuickBuffer(<q-args>)
