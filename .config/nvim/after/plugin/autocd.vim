if has('nvim')
	finish
endif

function! OnTabEnter(path)
  if isdirectory(a:path)
    let dirname = a:path
  else
    let dirname = fnamemodify(a:path, ":h")
  endif
  execute "tcd ". dirname
endfunction

nnoremap <Leader>go :call OnTabEnter()<CR>
