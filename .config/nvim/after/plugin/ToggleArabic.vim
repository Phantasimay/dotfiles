" "------------------Hebrew_Toggle_Function------------------

	function! ToggleArabic()
		if &rl
			set noarabic
			set fillchars=eob:\
			set norl
			set keymap=
			set spell
			echom "Aarabic mode OFF"
		else
			set arabic
			" set encoding=utf-8
			set arabicshape
			set delcombine
			set fillchars=vert:l
			hi WinSeparator ctermbg=White
			set rl
			set keymap=arabic
			set delcombine
			set nospell
			center 155
			echom "Arabic mod ON"
		endif
	endfunction
" Toggle Hebrew key maps and Right-to-Left setting
	nnoremap <leader>at <cmd>call ToggleArabic()<CR>
