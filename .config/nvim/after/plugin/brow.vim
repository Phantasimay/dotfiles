"-------------------------------------------------------------------
" WebBrowser
"-------------------------------------------------------------------
" Author: Alexandre Viau
"
" Version: 1.7
" Description: Uses the lynx text browser to browse websites and local files and return the rendered web pages inside vim. The links in the web pages may be "clicked" to follow them, so it turns vim into a simple web text based web browser. This plugin is based on the "browser.vim" plugin.
" Author: Alexandre Viau (alexandreviau@gmail.com)
" Website: The latest version is found on "vim.org"
"
" Installation: {{{2
" Copy the plugin to the vim plugin directory.
" In the lynx.cfg file, set the following parameters:
" ACCEPT_ALL_COOKIES:TRUE
" MAKE_LINKS_FOR_ALL_IMAGES:TRUE
" Change the following paths to your lynx files:
" let s:lynxPath = "~/.config/lynx/"
" let s:lynxExe = "/bin/lynx"
" let s:lynxCfg = '-cfg=' . s:lynxPath . 'lynx.cfg'
" let s:lynxLss = '-lss=' . s:lynxPath . 'lynx.lss'
" let s:lynxCmd = s:lynxExe . ' ' . s:lynxCfg . ' ' . s:lynxLss
" let s:lynxDumpPath = "~/.local/state/lynx"
" let s:lynxToolsPath = "~/.local/state/lynx"
"
" Usage: {{{2
" <leader>wb :WebBrowser (Open a new web browser tab with the address specified)
" <leader>wc :exe 'WebBrowser "' . @* . '"'<cr> (Open a new web browser tab with the address in the clipboard)
" <leader>wg :exe 'WebBrowser www.google.com/search?q="' . input("Google ") . '"'<cr> (Do a google search using the specified search keywords and open the results in a new tab)
" <leader>wp :exe 'WebBrowser www.wikipedia.com/wiki/"' . input("Wikipedia ") . '"'<cr> (Do a wikipedia search using the specified search keywords and open the results in a new tab)
" <leader>wd :WebDump (Downloads the specified webpage without opening it in vim)
" <space>l (Open link)
" <space>h (Previous page ("back button"))
" <space>j (Highlight links and go to next link)
" <space>k (Highlight links and go to previous link)
"
" Todo: {{{2
" - Redo the code that gets the link number and search for it at the end of the file (no need to move cursor) and to do it in a mapping, it may be done in a function
" - I added basicXmlParser in the plugin, add a webBrowser.xml file and in it, have 2 keys: history and favorites, which will be string of utl links
" - Add links bar like vimExplorer (favorites & history) with links not from utl but with brackets [http://yahoo.com] thus no need for utl
" - Use image_links and accept_all_cookies options in the command run from the plugin instead of having to modify the .cfg file
"
" History {{{2
" 1.1 {{{3
" - Changed the file format to unix
" 1.2 {{{3
" - Changed the mappings
" 1.3 {{{3
" - Now the lynx command and others are ran using a call to the system() function so that the dos prompt window is not displayed on the screen
" - Added suggested mappings in comments (<insert>, <delete>, etc)
" - Added folds
" 1.4 {{{3
" - Changed mappings \wb etc to <leader>wb because it was causing my "w" key in normal mode to wait for another key
" 1.5 {{{3
" - Added the webdump command and function to download in batch
" 1.6 {{{3
" - Added documentation and usage
" 1.7 {{{3
" - Fix to run on linux.

com! -nargs=+ WebBrowser call OpenWebBrowser(<q-args>, 1)

" Open a new web browser tab with the address specified
nmap <leader>wb :WebBrowser
" Do a google search using the specified search keywords and open the results in a new tab
" nmap <leader>wg :exe 'WebBrowser lite.duckduckgo.com/lite?kd=-1&kp=-1&q="' . input("LiteDuck ") . '"'<cr>
nmap <leader>wg :exe 'WebBrowser lite.duckduckgo.com/lite?kd=-1&kp=-1&q=' . input("LiteDuck ") . ''<cr>
" Do a wikipedia search using the specified search keywords and open the results in a new tab
nmap <leader>wp :exe 'WebBrowser www.wikipedia.com/wiki/"' . input("Wikipedia ") . '"'<cr>

let s:lynxPath = "/home/sandun/.config/lynx/"
let s:lynxExe = "/usr/bin/lynx"
let s:lynxCfg = '-cfg=' . s:lynxPath . 'lynx.cfg'
let s:lynxLss = '-lss=' . s:lynxPath . 'lynx.lss'
let s:lynxCmd = s:lynxExe . ' ' . s:lynxCfg . ' ' . s:lynxLss
let s:lynxDumpPath = "/tmp/lynx/"
let s:lynxToolsPath = "/home/sandun/.local/state/lynx/"

" Create path to dump the files (may act as an history path but files of same name are replaced)
if isdirectory(s:lynxDumpPath) == 0
    call mkdir(s:lynxDumpPath)
endif

fun! OpenWebBrowser(address, openInNewTab)
	setlocal shell=sh
    " Percent-encode some characters because it causes problems in the command line on the windows test computer
    "! 	# 	$ 	& 	' 	( 	) 	* 	+ 	, 	/ 	: 	; 	= 	? 	@ 	[ 	]
    "%21 	%23 	%24 	%26 	%27 	%28 	%29 	%2A 	%2B 	%2C 	%2F 	%3A 	%3B 	%3D 	%3F 	%40 	%5B 	%5D
    let l:address = substitute(a:address, '&', '\\&', 'g')
    let l:address = substitute(l:address, '#', '\%23', 'g')
    " Substitute invalid characters
    let l:dumpFile = substitute(l:address, '\', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '/', '-', 'g')
    let l:dumpFile = substitute(l:dumpFile, ':', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '*', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '?', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '"', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '<', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '>', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '|', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '%', '_', 'g')
    let l:dumpFile = substitute(l:dumpFile, '=', '_', 'g')
    " Get extension of the file
    let l:extPos = strridx(a:address, '.')
    let l:extLen = strlen(a:address) - l:extPos
    let l:extName = strpart(a:address, l:extPos, l:extLen)
    " Open the webpage/file and dump it using the lynx -dump feature to the dump directory
    exe 'silent ! ' . s:lynxCmd . ' -dump ' . l:address . ' > "' . s:lynxDumpPath . l:dumpFile . '"'
    if l:extName == '.jpg' || l:extName == '.gif' || l:extName == '.png'
        let l:lynxDumpStr = system(s:lynxCmd . ' -dump ' . l:address)
        call writefile([l:lynxDumpStr], s:lynxDumpPath . l:dumpFile)
    else
        let l:lynxDumpStr = system(s:lynxCmd . ' -dump ' . l:address)
        let l:lynxDumpArr = split(l:lynxDumpStr, '\n')
        call writefile(l:lynxDumpArr, s:lynxDumpPath . l:dumpFile, 'b')
    endif
    " Select view method according to the page/file extension
    let l:vimFile = ''
    if l:extName == '.jpg' || l:extName == '.gif' || l:extName == '.png'
        exe 'silent !start "' . s:lynxToolsPath . 'i_view32.exe" "' . s:lynxDumpPath . l:dumpFile . '"'
    elseif l:extName == '.pdf'
        "exe 'silent !start ' . s:lynxToolsPath . 'foxitreader.exe "' . s:lynxDumpPath . l:dumpFile . '"'
        exe 'silent ! "' . s:lynxToolsPath . 'pdftotext.exe" "' . s:lynxDumpPath . l:dumpFile . '" "' . s:lynxDumpPath . l:dumpFile . '.txt"'
        let l:vimFile = s:lynxDumpPath . l:dumpFile . '.txt'
    else " Any other extension (html, htm or no extension etc.)
        let l:vimFile = s:lynxDumpPath . l:dumpFile
    endif
    " Open a file in the buffer
    if l:vimFile != ''
        if a:openInNewTab == 1
			setlocal isfname+=32,/,',?,`,:,;,&
			setlocal isfname-=[,]
            exe "tabnew"
            exe "set buftype=nofile"
			" Open a new web browser tab with the address in the clipboard
			nmap <silent> <leader>wc <cmd>exe 'WebBrowser "' . @* . '"'<cr>
            " Open link
            nnoremap <buffer> <space>l <cmd>exe 'WebBrowser ' . expand('<cWORD>')<cr>
            " Previous page ("back button")
            nnoremap <buffer> <space>h <cmd>exe 'normal u'<cr>
        else
            " Clear the buffer
            exe "norm ggdG"
        endif
        " Read the file in the buffer
        exe 'silent r ' . l:vimFile
		exe 'silent %s/]/ ]/g'
		exe 'silent %s/\[/[ /g'
		" Set syntax to have bold links
        " syn reset
        syn match Keyword /\[\d*\]\w*/ contains=Ignore
        syn match Ignore /\[\d*\]/ contained
        exe "norm gg"
        call append(0, [a:address])
    else
        " Return to previous cursor position to return to where the link was executed
		exe "norm! \<c-o>"
    endif
    " Add address to append register which acts as an history for the current session
    let @H = strftime("%x %X") . ' <url:' . substitute(a:address, '\"', '\\"', 'g') . '>'
endfun
