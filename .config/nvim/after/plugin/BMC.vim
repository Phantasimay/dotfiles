 function! BMC() abort
  " Get the position of the cursor, if it is the start of the file we want
  " a different behavior than if it is elsewhere.
  let cursor_pos = getpos(".")
  if cursor_pos[1] == "1"
    if cursor_pos[2] == "1"
      call append(0, ["= ", ":keywords: ", ":toc:", "", "== Segmen Konsumen", "", "", "== UPV (Unique Value Proposition)", "", "", "== Kanal", "", "", "== Hubungan Konsumen", "", "", "== Pendapatan", "", "", "=== Tipe", "", "", "=== Harga Tetap", "", "", "=== Harga Dinamis", "", "", "== Sumberdaya", "", "", "=== Jenis sumberdaya", "", "", "== Usaha/Aktivitas", "", "", "=== Golongan", "", "", "== Jaringan Relasi", "", "", "=== Motivasi untuk berrelasi", "", "", "== Biaya", "", "", "=== Kecenderungan model usaha", "", "", "=== Karakteristik", "", "", "'''''", "", "== MindMap", ". link:./deck/0.adoc[QnA]", ". link:./favorite.adoc[Favorite]", ". link:../canvas_20241101033700.adoc[Canvas]", "", "include::./reference/[]"])
      call cursor(cursor_pos[1], 1)
    endif
  else
    call append(0, ["= ", ":keywords: ", ":toc:", "", "== Segmen Konsumen", "", "", "== UPV (Unique Value Proposition)", "", "", "== Kanal", "", "", "== Hubungan Konsumen", "", "", "== Pendapatan", "", "", "=== Tipe", "", "", "=== Harga Tetap", "", "", "=== Harga Dinamis", "", "", "== Sumberdaya", "", "", "=== Jenis sumberdaya", "", "", "== Usaha/Aktivitas", "", "", "=== Golongan", "", "", "== Jaringan Relasi", "", "", "=== Motivasi untuk berrelasi", "", "", "== Biaya", "", "", "=== Kecenderungan model usaha", "", "", "=== Karakteristik", "", "", "'''''", "", "== MindMap", ". link:./deck/0.adoc[QnA]", ". link:./favorite.adoc[Favorite]", ". link:../canvas_20241101033700.adoc[Canvas]", "", "include::./reference/[]"])
    call cursor(cursor_pos[1]+3, 1)
  endif
endfunction
