if has('nvim')
  finish
endif
" integration gitstatus to lightline
 function! LightlineGitGutter() abort
   if !get(g:, 'gitgutter_enabled', 0)
     return ''
   endif
   let l:branchname = FugitiveHead()
   let [ l:added, l:modified, l:removed ] = GitGutterGetHunkSummary()
   if l:branchname != ""
     let l:branchname = "\uf126 " .. l:branchname
   endif
   return printf('%s +%d ~%d -%d', l:branchname, l:added, l:modified, l:removed)
 endfunction

