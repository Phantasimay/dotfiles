 " displays all the syntax rules for current position, useful when writing vimscript syntax plugins
 if has("syntax")
 function! <SID>SynStack() abort
   if !exists("*synstack")
     return
   endif
     echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
 endfunc
 endif

 nnoremap <leader>ss :call <SID>SynStack()<CR>
