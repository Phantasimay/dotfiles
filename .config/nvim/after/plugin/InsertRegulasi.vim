 function! InsertRegulasi() abort
  " Get the position of the cursor, if it is the start of the file we want
  " a different behavior than if it is elsewhere.
  let cursor_pos = getpos(".")
  if cursor_pos[1] == "1"
    if cursor_pos[2] == "1"
      call append(0, ["= ", ":keywords: regulasi", ":toc:", "", "== Aktif", "* ", "", "== Pasal Ketentuan Umum", "link:./", "", "== Pasal Peralihan dan Penutup", ". Masa tengang ()", ". Pencabutan:", "  *", "", "", "'''''", "", "== link:./deck/0.adoc[QnA]", "", "== MindMap", ". link:./favorite.adoc[Favorite]", ". link:./regulasi_20240724063752.adoc[Regulasi]", "", "include::./reference/"])
      call cursor(cursor_pos[1], 1)
    endif
  else
	call append(0, ["= ", ":keywords: regulasi", ":toc:", "", "== Aktif", "* ", "", "== Pasal Ketentuan Umum", "link:./", "", "== Pasal Peralihan dan Penutup", ". Masa tengang ()", ". Pencabutan:", "  *", "", "", "'''''", "", "== link:./deck/0.adoc[QnA]", "", "== MindMap", ". link:./favorite.adoc[Favorite]", ". link:./regulasi_20240724063752.adoc[Regulasi]", "", "include::./reference/"])
    call cursor(cursor_pos[1]+3, 1)
  endif
endfunction
