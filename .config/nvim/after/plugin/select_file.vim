if has('nvim')
	finish
endif

" Fzf integration
function! SelectFile() abort
	 let tmp = tempname()
	 execute '!ls | fzf >'.tmp
	 let fname = readfile(tmp)[0]
	 silent execute '!rm '.tmp
	 execute 'tabe '.fname
endfunction

nnoremap <Leader>sl :call SelectFile()<cr>
