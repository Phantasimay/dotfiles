if has('nvim')
	finish
endif
 "COC Toggle
 function! CocToggle() abort
    if g:coc_enabled
        CocDisable
    else
        CocEnable
    endif
 endfunction
