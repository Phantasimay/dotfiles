if has('nvim')
	finish
endif

"fix bork bash detection
 if has("eval")  " vim-tiny detection
 fun! DetectBash() abort
     if getline(1) == '#!/usr/bin/bash' || getline(1) == '#!/bin/bash'
         set ft=bash
         set shiftwidth=2
     endif
 endfun
 endif

 map <Leader>bb :call DetectBash()<cr>
