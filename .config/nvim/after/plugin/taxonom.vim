 function! Taxonom() abort
  " Get the position of the cursor, if it is the start of the file we want
  " a different behavior than if it is elsewhere.
  let cursor_pos = getpos(".")
  if cursor_pos[1] == "1"
    if cursor_pos[2] == "1"
	call append(0, ["= ", ":keywords: ", ":toc:", "", "image::[]", "", "* Species  : ", "* Local    : ", "* Synonyms : ", "* Part     : ", "", "== Organoleptis", "", ". Bentuk : ", "  * Bau   : ", "  * Warna : ", "  * Rasa  : ", "", "== Peringatan", "", "* ", "", "== Khasiat", "", ". ", "  * ", "", "== Takaran pakai", "", "", "== MindMap", ". link:./deck/0.adoc[QnA]", ". link:./favorite.adoc[Favorite]", ". link:./herbal_list_20231018172017.adoc[Herbal]", "", "'''''", "include::./reference/"])
      call cursor(cursor_pos[1], 1)
    endif
  else
      call append(0, ["= ", ":keywords: ", ":toc:", "", "image::[]", "", "* Species  : ", "* Local    : ", "* Synonyms : ", "* Part     : ", "", "== Organoleptis", "", ". Bentuk : ", "  * Bau   : ", "  * Warna : ", "  * Rasa  : ", "", "== Peringatan", "", "* ", "", "== Khasiat", "", ". ", "  * ", "", "== Takaran pakai", "", "", "== MindMap", ". link:./deck/0.adoc[QnA]", ". link:./favorite.adoc[Favorite]", ". link:./herbal_list_20231018172017.adoc[Herbal]", "", "'''''", "include::./reference/"])
    call cursor(cursor_pos[1]+3, 1)
  endif
endfunction

