if has('nvim')
  finish
endif
" puremourning/vimspector
  fun! GotoWindow(id)
    :call win_gotoid(a:id)
  endfun
  func! AddToWatch()
    let word = expand("<cexpr>")
    call vimspector#AddWatch(word)
  endfunction
  let g:vimspector_base_dir = expand('$HOME')
"  let g:vimspector_sidebar_width = 60
  let g:vimspector_sign_priority = {
    \    'vimspectorBP':         998,
    \    'vimspectorBPCond':     997,
    \    'vimspectorBPDisabled': 996,
    \    'vimspectorPC':         999,
    \ }

 function! JestStrategy(cmd)
  let testName = matchlist(a:cmd, '\v -t ''(.*)''')[1]
  call vimspector#LaunchWithSettings( #{ configuration: 'jest', TestName: testName } )
 endfunction
 let g:test#custom_strategies = {'jest': function('JestStrategy')}
