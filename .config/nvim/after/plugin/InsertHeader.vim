 function! InsertHeader() abort
  " Get the position of the cursor, if it is the start of the file we want
  " a different behavior than if it is elsewhere.
  let cursor_pos = getpos(".")
  if cursor_pos[1] == "1"
    if cursor_pos[2] == "1"
      call append(0, ["---", "title:", "subtitle: ", "Keyword: []", "---", ""])
      call cursor(cursor_pos[1]+6, 1)
    endif
  else
    call append(cursor_pos[1], ["---", "title:", "subtitle: ", "Keyword: []", "---", ""])
    call cursor(cursor_pos[1]+4, 1)
  endif
endfunction
