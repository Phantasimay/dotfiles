if has('nvim')
  finish
endif
" File name lightline
 function! LightlineFilename() abort
   let filename = expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
   let modified = &modified ? ' [+]' : ''
   return filename . modified
 endfunction
