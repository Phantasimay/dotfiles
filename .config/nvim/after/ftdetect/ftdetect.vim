       "Fix file type
       au bufnewfile,bufRead $SNIPPETS/bash/* set ft=bash
       au bufnewfile,bufRead $SNIPPETS/c/* set ft=c
       au bufnewfile,bufRead $SNIPPETS/css/* set ft=css
       au bufnewfile,bufRead $SNIPPETS/go/* set ft=go
       au bufnewfile,bufRead $SNIPPETS/html/* set ft=html
       au bufnewfile,bufRead $SNIPPETS/js/* set ft=javascript
       " au bufnewfile,bufRead $SNIPPETS/md/* set ft=markdown
       au bufnewfile,bufRead $SNIPPETS/perl/* set ft=perl
       au bufnewfile,bufRead $SNIPPETS/python/* set ft=python
       au bufnewfile,bufRead $SNIPPETS/sh/* set ft=sh
       au BufNewFile,BufRead *.bash,*.bash* set filetype=sh
       au BufNewFile,BufRead *.corntab set filetype=corntab
       au BufNewFile,BufRead *.cpp setlocal expandtab ts=2 sw=2
       au bufnewfile,bufRead .dockerignore set filetype=gitignore
       au BufNewFile,BufRead Fastfile,Appfile,Podfile set ft=ruby
       au bufnewfile,bufRead *gitconfig set filetype=gitconfig
       au BufNewFile,BufRead *.go setlocal noet ts=4 sw=4 sts=4 spell spellcapcheck=0
       au bufnewfile,bufRead *.gotmpl set ft=go
       au BufNewFile,BufRead *.hpp setlocal expandtab ts=2 sw=2
       au BufNewFile,BufRead *.hujson set ft=json
       au BufNewFile,BufRead *.jade setlocal expandtab ts=2 sw=2
       au BufNewFile,BufRead *.json setlocal expandtab ts=2 sw=2
       au bufnewfile,bufRead *.jsx set filetype=javascript.jsx
       au bufnewfile,bufRead keg set ft=yaml
       au bufnewfile,bufRead ~/wiki/* set  fdm=manual
       au BufNewFile,BufRead *.lua setlocal noet ts=4 sw=4 sts=4
       au BufNewFile,BufRead MAINTAINERS,*.toml set ft=toml formatprg=toml-fmt
       au bufnewfile,BufFilePre,BufRead *.md,*.markdown* set ft=markdown syntax=markdown ve=all fdm=manual
       au BufNewFile,BufRead *.md,*.txt,*.adoc setlocal nowrap tw=999 wm=5 fo+=wac ts=2 sw=2 sts=2 fp=fmt fdm=manual
       au bufnewfile,bufRead meta-data set ft=yaml
       au BufNewFile,BufRead *.mips set ft=mips
       au BufNewFile,BufRead *.mts set ft=typescript
       au BufNewFile,BufRead .nginx.conf*,nginx.conf* setf nginx
       au BufNewFile,BufRead *.njk,*.hbs set ft=html
       au bufnewfile,bufRead *.{peg,pegn} set ft=config
       au BufNewFile,BufReadPost *.scala setl shiftwidth=2 expandtab
       au bufnewfile,bufRead *.profile set filetype=sh
       au BufNewFile,BufRead *.profile set filetype=sh
       au BufNewFile,BufRead *.py setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=80 smarttab expandtab
       au BufNewFile,BufRead *ssh/config set filetype=sshconfig
       au bufnewfile,bufRead *.tex set filetype=tex
       au bufnewfile,bufRead /tmp/psql.edit.* set syntax=sql
       au BufNewFile,BufRead tmp/psql.* set syntax=sql
       au BufNewFile,BufRead .tmux.conf*,tmux.conf* setf tmux
       au bufnewfile,bufRead *.tp set filetype=type
       au bufnewfile,bufRead user-data set ft=yaml
       au bufnewfile,bufRead vim.* set filetype=vim
       au BufNewFile,BufRead *.vim setlocal noet ts=4 sw=4 sts=4
       au BufNewFile,BufRead *.workflow,*.nomad,*.nomad.tpl set ft=hcl
       au BufNewFile,BufRead *.yml,*.yaml setlocal spell expandtab ts=2 sw=2
