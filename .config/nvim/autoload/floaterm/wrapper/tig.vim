function! floaterm#wrapper#tig#(cmd, jobopts, config) abort
  return [v:true, 'tig']
endfunction
