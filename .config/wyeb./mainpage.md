<!-- this is text/markdown -->
<meta charset=utf8>
<style>
body{overflow-y:scroll} /*workaround for the delaying of the context-menu*/
a{background:linear-gradient(to right top, #ddf, white, white, white);
 color:#109; margin:1px; padding:2px; text-decoration:none; display:inline-block}
a:hover{text-decoration:underline}
img{height:1em; width:1em; margin:-.1em}
strong > code{font-size:1.4em}
</style>

###Specific Keys:
- **`e`** : Edit this page
- **`E`** : Edit main config file
- **`c`** : Open config directory
- **`m`** : Show this page
- **`M`** : Show **[history](wyeb:history)**
- **`b`** : Add title and URI of a page opened to this page

If **e,E,c** don't work, edit values '`mimeopen -n %s`' of ~/.config/wyeb./main.conf<br>
or change mimeopen's database by running '<code>mimeopen <i>file/directory</i></code>' in terminals.

For other keys, see **[help](wyeb:help)** assigned '**`?`**'.
Since wyeb is inspired by **[dwb](https://wiki.archlinux.org/index.php/dwb)**
and luakit, usage is similar to them.

---
<!--
wyeb:i/iconname returns an icon image of current icon theme of gtk.
wyeb:f/uri returns a favicon of the uri loaded before.
wyeb:F converted to the wyeb:f with a parent tag's href.
-->
[![](wyeb:i/wyeb) wyeb](https://github.com/jun7/wyeb)
[Wiki](https://github.com/jun7/wyeb/wiki)
[![](wyeb:F) Adblock](https://github.com/jun7/wyebadblock)
[![](wyeb:f/https://archlinux.org/) Arch Linux](https://archlinux.org/)


[NoTrack](http://127.0.0.1/admin)


[Print](http://127.0.0.1:631/)


[在线工具 | CheatSheet](https://sunnnychan.github.io/cheatsheet/ol-tools/#%E5%BC%80%E5%8F%91%E5%B7%A5%E5%85%B7)


[myvimplugin - Vim Awesome](https://vimawesome.com/plugin/myvimplugin)


[Learn Vimscript the Hard Way](https://learnvimscriptthehardway.stevelosh.com/)


[FZF + WordNet = Dictionary - Why, Scott, WHY?!?](https://ddrscott.github.io/blog/2017/fzf-dictionary/)


[masterpdf kg](https://cracksurl.com/master-pdf-editor/)


[disable DM](https://askubuntu.com/questions/882422/how-can-i-disable-all-display-managers)


[pam lib](https://ubuntuforums.org/showthread.php?t=803152)


[acpi off](https://unix.stackexchange.com/questions/443398/acpi-bios-error-ae-not-found)


[auto cpufreq](https://github.com/AdnanHodzic/auto-cpufreq)


[dragon fm](https://github.com/mwh/dragon)


[binder waydroid](https://github.com/choff/anbox-modules)


[apt prefrence](https://askubuntu.com/questions/116257/adding-debian-sid-as-package-repository)


[Forum Kali](https://forums.kali.org/forum.php)


[linux learn](https://nidkil.me/category/linux/page/2/)


[sh shere](http://dotshare.it/)


[maping vim](https://stackoverflow.com/questions/3776117/what-is-the-difference-between-the-remap-noremap-nnoremap-and-vnoremap-mapping)


[automount usb](https://serverfault.com/questions/766506/automount-usb-drives-with-systemd)


[fix python](https://technicalnavigator.in/how-to-fix-error-externally-managed-environment-in-python-kali-linux/)


[live server](http://127.0.0.1:8080/)


[wrap vimterm](https://unix.stackexchange.com/questions/20493/how-to-disable-line-wrap-in-a-terminal)


[rotate bg](https://blog.dotcs.me/posts/migrate-from-i3-to-sway)


[fzf wiki](https://github.com/junegunn/fzf/wiki)


[RIP portal](https://riptutorial.com/)


[web scrap](https://www.joyofdata.de/blog/using-linux-shell-web-scraping/)


[into tmux](https://gist.github.com/sdondley/b01cc5bb1169c8c83401e438a652b84e)


[shaders mpv](https://kohana.fi/article/mpv-for-anime)


[git on tmux](https://www.markneuburger.com/git-statuses-in-tmux-panes/)


[source list priority](https://difyel.com/linux/etc/apt/preferences/)


[keyboard symbole name](https://excelnotes.com/names-of-the-keyboard-symbols/)


[migration guide vim-neovim](https://neovim.io/doc/user/lua-guide.html)


[fix no authori](ation protocol specified https://itsfoss.com/switch-xorg-wayland/)


[wineland](https://github.com/Kron4ek/wine-wayland)


[lib32](https://support.humblebundle.com/hc/en-us/articles/202759400-Installing-32-bit-libs-on-a-64-bit-Linux-system)


[qjackctl](https://askubuntu.com/questions/224151/jack-server-could-not-be-started-when-using-qjackctl)


[cssmatic](https://www.cssmatic.com/)


[keycode](https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes)


[cdnfonts](https://cdnjs.com/libraries/font-awesome/5.15.4)


[Systemd](https://www.linux.com/training-tutorials/understanding-and-using-systemd/)


[javascript](https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties)


[w3school](https://www.w3schools.com/csS/default.asp)


[rwxrob](https://rwx.gg/)


[faker](https://fakerjs.dev/about/announcements/2022-01-14.html)


[alpine](https://unix.stackexchange.com/questions/327803/pine-alpine-with-gmail-2-step-authentication-enabled)


[vimc](https://github.com/amix/vimrc)


[4chan](https://www.4channel.org/)


[diaspora](https://diasp.org/users/sign_in)


[mopidy](https://www.digitalneanderthal.com/post/ncmpcpp/)


[dictionary](https://ddrscott.github.io/blog/2017/fzf-dictionary/)


[How I take Note with Pandoc, Vim and markdown](https://jamesbvaughan.com/markdown-pandoc-notes/)


[Kali Linux add PPA repository add-apt-re](https://www.blackmoreops.com/2014/02/21/kali-linux-add-ppa-repository-add-apt-repository/)
[13 resources for learning to write bette](https://www.redhat.com/sysadmin/learn-bash-scripting)
[Explore GitHub · GitHub](https://github.com/explore)
