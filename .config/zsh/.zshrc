# ~/.zshrc file for zsh interactive shells.
# see /usr/share/doc/zsh/examples/zshrc for examples

if [[  -d $HOME/.ssh && -z "$(pgrep -f ssh-agent)" ]]; then
    exec ssh-agent zsh
    # eval "$(ssh-agent)"
fi
albafetch

setopt autocd              # change directory just by typing its name
setopt correct            # auto correct mistakes
setopt interactivecomments # allow comments in interactive mode
setopt magicequalsubst     # enable filename expansion for arguments of the form ‘anything=expression’
setopt nonomatch           # hide error message if there is no match for the pattern
setopt notify              # report the status of background jobs immediately
setopt numericglobsort     # sort filenames numerically when it makes sense
setopt promptsubst         # enable command substitution in prompt

WORDCHARS=${WORDCHARS//\/} # Don't consider certain characters part of the word

# hide EOL sign ('%')
PROMPT_EOL_MARK=""

# configure key keybindings
set -o vi
bindkey ^R history-incremental-search-backward
bindkey ^S history-incremental-search-forward
bindkey -v                                        # Vi key bindings
bindkey ' ' magic-space                           # do history expansion on space
bindkey '^U' backward-kill-line                   # ctrl + U
bindkey '^[[3;5~' kill-word                       # ctrl + Supr
bindkey '^[[3~' delete-char                       # delete
bindkey '^[[1;5C' forward-word                    # ctrl + ->
bindkey '^[[1;5D' backward-word                   # ctrl + <-
bindkey '^[[5~' beginning-of-buffer-or-history    # page up
bindkey '^[[6~' end-of-buffer-or-history          # page down
bindkey '^[[H' beginning-of-line                  # home
bindkey '^[[F' end-of-line                        # end
bindkey '^[[Z' undo                               # shift + tab undo last action

# enable completion features
autoload -Uz compinit
compinit -d ~/.cache/zcompdump
# dont forget to enable local zsh function with sudo ln -s /your/zsh_function/directory /usr/local/share/zsh/site-functions
# fpath=($HOME/.zsh_functions $fpath);
# autoload -U $fpath[1]/*(.:t)
# fpath+=$HOME/zfunc
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' rehash true
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
zstyle ':completion:*' menu select

# History configurations
export HISTFILE="$XDG_STATE_HOME"/zsh/history
export HISTSIZE=1000
export SAVEHIST=2000
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt share_history         # share command history data

# force zsh to show the complete history
alias history="history 0"

# configure `time` format
TIMEFMT=$'\nreal\t%E\nuser\t%U\nsys\t%S\ncpu\t%P'

# make 'less' more friendly for non-text input files, see lesspipe(1)
[[ -x /usr/bin/lesspipe ]] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

# Git Status Indicators configuration
# ✗
ZSH_THEME_GIT_PROMPT_SHA_BEFORE="%b%F{%(#.blue.yellow)}"
ZSH_THEME_GIT_PROMPT_SHA_AFTER="%B%F{reset}"
ZSH_THEME_GIT_PROMPT_PREFIX="%b%F{%(#.blue.green)}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%B%F{reset}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %b%F{%(#.blue.white)}:%B%F{reset}"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%b%F{%(#.blue.white)}◒ "
ZSH_THEME_GIT_PROMPT_CLEAN=" "
ZSH_THEME_GIT_PROMPT_ADDED="%b%F{%(#.blue.cyan)}✓ "
ZSH_THEME_GIT_PROMPT_MODIFIED="%b%F{%(#.blue.blue)}△ "
ZSH_THEME_GIT_PROMPT_DELETED="%b%F{%(#.blue.red)}✖ "
ZSH_THEME_GIT_PROMPT_RENAMED="%b%F{%(#.blue.blue)}➜ "
ZSH_THEME_GIT_PROMPT_UNMERGED="%b%F{%(#.blue.cyan)}§ "
ZSH_THEME_GIT_PROMPT_AHEAD="%b%F{%(#.blue.blue)}▲ "
ZSH_THEME_GIT_TIME_SINCE_COMMIT_SHORT="%b%F{%(#.blue.white)}"
ZSH_THEME_GIT_TIME_SHORT_COMMIT_MEDIUM="%b%F{%(#.blue.blue)}"
ZSH_THEME_GIT_TIME_SINCE_COMMIT_LONG="%b%F{%(#.blue.yellow)}"
ZSH_THEME_GIT_TIME_SINCE_COMMIT_NEUTRAL="%b%F{%(#.blue.green)}"
# Get the time of the last commit on git
# Used in the prompt variable
function _git_branch_status() {
    # Only proceed if there is actually a commit. If not, don't do anything
    if git log -1 > /dev/null 2>&1; then
        # Get the branch name information.
        if [[ $(git symbolic-ref --short -q HEAD) == *"/"* ]]; then
            branch_name=$(git symbolic-ref --short -q HEAD | cut -f1 -d"-")
        else
            branch_name=$(git symbolic-ref --short -q HEAD)
        fi
        now=$(date +%s)
        last_commit=$(git log --pretty=format:'%at' -1 2> /dev/null)
        seconds_since_last_commit=$((now-last_commit))
        minutes=$((seconds_since_last_commit / 60))
        hours=$((seconds_since_last_commit/3600))
        if [[ -n $(git status -s 2> /dev/null) ]]; then
            SEPARATOR=" "
            if [ "$hours" -gt 4 ]; then
                COLOR="$ZSH_THEME_GIT_TIME_SINCE_COMMIT_LONG"
            elif [ "$minutes" -gt 30 ]; then
                COLOR="$ZSH_THEME_GIT_TIME_SHORT_COMMIT_MEDIUM"
            else
                COLOR="$ZSH_THEME_GIT_TIME_SINCE_COMMIT_SHORT"
            fi
        else
            SEPARATOR=""
            COLOR="$ZSH_THEME_GIT_TIME_SINCE_COMMIT_NEUTRAL"
        fi
        if [[ -n $(git diff --quiet 2>/dev/null) ]]; then
          STATUS="$ZSH_THEME_GIT_PROMPT_DIRTY "
          if (git status --porcelain -b | grep -q '^## .*ahead' 2>/dev/null) ; then
            STATUS+="$ZSH_THEME_GIT_PROMPT_AHEAD"
          fi
          if (git status --porcelain | grep -q '^.M ' 2>/dev/null) ; then
            MODIFIED=$(git status --porcelain | grep -c '^.M ')
            STATUS+="$ZSH_THEME_GIT_PROMPT_MODIFIED$MODIFIED "
          fi
          if (git status --porcelain | grep -q '^.D ' 2>/dev/null) ; then
            DELETED=$(git status --porcelain | grep -c '^.D ')
            STATUS+="$ZSH_THEME_GIT_PROMPT_DELETED$DELETED "
          fi
          if (git status --porcelain |grep -q '^.A ' 2>/dev/null) ; then
            ADDED=$(git status --porcelain | grep -c '^.A ')
            STATUS+="$ZSH_THEME_GIT_PROMPT_ADDED$ADDED "
          fi
          if (git status --porcelain |grep -q '^.[RT] ' 2>/dev/null) ; then
            RENAMED=$(git status --porcelain | grep -c '^.[RT] ')
            STATUS+="$ZSH_THEME_GIT_PROMPT_RENAMED$RENAMED "
          fi
          if (git status --porcelain | grep -q -E '^\?\? ' 2>/dev/null) ; then
            UNTRACKED=$(git status --porcelain | grep -c -E '^\?\? ' 2>/dev/null)
            STATUS+="$ZSH_THEME_GIT_PROMPT_UNTRACKED$UNTRACKED "
          fi
          if (git status --porcelain | grep -q '^UU ' 2>/dev/null) ; then
              UNMERGED=$(git status --porcelain | grep -c '^UU ')
              STATUS+="$ZSH_THEME_GIT_PROMPT_UNMERGED$UNMERGED "
          fi
        else
          STATUS="$ZSH_THEME_GIT_PROMPT_CLEAN"
        fi
        echo " ➜ %b%F{%(#.blue.magenta)}{$COLOR$branch_name${STATUS[@]}%b%F{%(#.blue.magenta)}}$SEPARATOR%B%F{reset}"
    fi
}

# Left Prompt:
configure_prompt() {
    prompt_symbol=㉿
    # Skull emoji for root terminal
    [ "$EUID" -eq 0 ] && prompt_symbol=💀
    case "$PROMPT_ALTERNATIVE" in
        twoline)
            # local _return_status="%{$fg_bold[red]%}%(?..⍉ )%B%F{reset}"
            PROMPT=$'%F{%(#.blue.green)}┌──${debian_chroot:+($debian_chroot)─}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))─}(%B%F{%(#.red.blue)}%n'$prompt_symbol$'%m%b%F{%(#.blue.green)})-[%B%F{reset}%(6~.%-1~/…/%4~.%5~)%b%F{%(#.blue.green)}]%B%F{reset}%b%F{%(#.blue.cyan)}$(_git_branch_status)%b%F{%(#.blue.green)}\n└─%B%(#.%F{red}#.%F{blue}$)%b%F{reset} '
            # RPROMPT='%{$fg_bold[cyan]%}%B%F{reset}${_git_branch_status}${_return_status} ➜ ${vim_mode}'
            ;;
        oneline)
            PROMPT=$'${debian_chroot:+($debian_chroot)}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))}%B%F{%(#.red.blue)}%n@%m%b%F{reset}:%B%F{%(#.blue.green)}%~%b%F{reset}%(#.#.$) '
            RPROMPT='${vim_mode}'
            ;;
        backtrack)
            PROMPT=$'${debian_chroot:+($debian_chroot)}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))}%B%F{red}%n@%m%b%F{reset}:%B%F{blue}%~%b%F{reset}%(#.#.$) '
            RPROMPT='${vim_mode}'
            ;;
    esac
    unset prompt_symbol
}

# Right Promp:
# Vi mode indicator
export KEYTIMEOUT=1
function zle-line-init zle-keymap-select {
    RPS1="${${KEYMAP/vicmd/-- NORMAL --}/(main|viins)/-- INSERT --}"
    RPS2=$RPS1
    # Change cursor shape for different vi modes.
    if [[ ${KEYMAP} == vicmd ]] ||
       [[ $1 = 'block' ]]; then
      echo -ne '\e[0 q'

    elif [[ ${KEYMAP} == main ]] ||
         [[ ${KEYMAP} == viins ]] ||
         [[ ${KEYMAP} = '' ]] ||
         [[ $1 = 'beam' ]]; then
      echo -ne '\e[5 q'
    fi
    zle reset-prompt
    zle -R
    }

# Use beam shape cursor on startup.
echo -ne '\e[0 q'

# Use beam shape cursor for each new prompt.
preexec() {
   echo -ne '\e[0 q'
}

function zle-line-finish {
    print -n -- "\E]50;CursorShape=0\C-G"  # block cursor
    }
zle-line-init() { zle -K vicmd; }
zle -N zle-line-init
zle -N zle-keymap-select
zle -N zle-line-finish

# Yank to the system clipboard
function vi-yank-xclip {
    zle vi-yank
   echo "$CUTBUFFER" | wl-copy
}

zle -N vi-yank-xclip
bindkey -M vicmd 'y' vi-yank-xclip


# The following block is surrounded by two delimiters.
# These delimiters must not be modified. Thanks.
# START KALI CONFIG VARIABLES
PROMPT_ALTERNATIVE=twoline
NEWLINE_BEFORE_PROMPT=yes
# STOP KALI CONFIG VARIABLES

if [ "$color_prompt" = yes ]; then
    # override default virtualenv indicator in prompt
    VIRTUAL_ENV_DISABLE_PROMPT=1

    configure_prompt

    # enable syntax-highlighting
    if [ -f /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
        . /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
        ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)
        ZSH_HIGHLIGHT_STYLES[default]=none
        ZSH_HIGHLIGHT_STYLES[unknown-token]=fg=white,underline
        ZSH_HIGHLIGHT_STYLES[reserved-word]=fg=cyan,bold
        ZSH_HIGHLIGHT_STYLES[suffix-alias]=fg=green,underline
        ZSH_HIGHLIGHT_STYLES[global-alias]=fg=green,bold
        ZSH_HIGHLIGHT_STYLES[precommand]=fg=green,underline
        ZSH_HIGHLIGHT_STYLES[commandseparator]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[autodirectory]=fg=green,underline
        ZSH_HIGHLIGHT_STYLES[path]=bold
        ZSH_HIGHLIGHT_STYLES[path_pathseparator]=
        ZSH_HIGHLIGHT_STYLES[path_prefix_pathseparator]=
        ZSH_HIGHLIGHT_STYLES[globbing]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[history-expansion]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[command-substitution]=none
        ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter]=fg=magenta,bold
        ZSH_HIGHLIGHT_STYLES[process-substitution]=none
        ZSH_HIGHLIGHT_STYLES[process-substitution-delimiter]=fg=magenta,bold
        ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=fg=green
        ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=fg=green
        ZSH_HIGHLIGHT_STYLES[back-quoted-argument]=none
        ZSH_HIGHLIGHT_STYLES[back-quoted-argument-delimiter]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[single-quoted-argument]=fg=yellow
        ZSH_HIGHLIGHT_STYLES[double-quoted-argument]=fg=yellow
        ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument]=fg=yellow
        ZSH_HIGHLIGHT_STYLES[rc-quote]=fg=magenta
        ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]=fg=magenta,bold
        ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]=fg=magenta,bold
        ZSH_HIGHLIGHT_STYLES[back-dollar-quoted-argument]=fg=magenta,bold
        ZSH_HIGHLIGHT_STYLES[assign]=none
        ZSH_HIGHLIGHT_STYLES[redirection]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[comment]=fg=black,bold
        ZSH_HIGHLIGHT_STYLES[named-fd]=none
        ZSH_HIGHLIGHT_STYLES[numeric-fd]=none
        ZSH_HIGHLIGHT_STYLES[arg0]=fg=cyan
        ZSH_HIGHLIGHT_STYLES[bracket-error]=fg=red,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-1]=fg=blue,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-2]=fg=green,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-3]=fg=magenta,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-4]=fg=yellow,bold
        ZSH_HIGHLIGHT_STYLES[bracket-level-5]=fg=cyan,bold
        ZSH_HIGHLIGHT_STYLES[cursor-matchingbracket]=standout
    fi
else
    PROMPT='${debian_chroot:+($debian_chroot)}%n@%m:%~%(#.#.$) '
fi
unset color_prompt force_color_prompt

toggle_oneline_prompt(){
    if [ "$PROMPT_ALTERNATIVE" = oneline ]; then
        PROMPT_ALTERNATIVE=twoline
    else
        PROMPT_ALTERNATIVE=oneline
    fi
    configure_prompt
    zle reset-prompt
    }
zle -N toggle_oneline_prompt
bindkey ^P toggle_oneline_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*|Eterm|aterm|kterm|gnome*|alacritty)
    TERM_TITLE=$'\e]0;${debian_chroot:+($debian_chroot)}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))}%n@%m: %~\a'
    ;;
*)
    ;;
esac

precmd() {
    # Print the previously configured title
    print -Pnr -- "$TERM_TITLE"

    # Print a new line before the prompt, but only if it is not the first line
    if [ "$NEWLINE_BEFORE_PROMPT" = yes ]; then
        if [ -z "$_NEW_LINE_BEFORE_PROMPT" ]; then
            _NEW_LINE_BEFORE_PROMPT=1
        else
            print ""
        fi
    fi
    }


# enable auto-suggestions based on the history
if [ -f /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh ]; then
    . /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
    # change suggestion color
    ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=#999'
fi

# enable command-not-found if installed
if [ -f /etc/zsh_command_not_found ]; then
    . /etc/zsh_command_not_found
fi

# Predictable SSH authentication socket location.
# SOCK="$HOME/.ssh/agent_sock"
# AGENT_ENV="$HOME/.ssh/agent_env"
# if [ -d $HOME/.ssh ] && (! ps -x | fgrep -v fgrep | fgrep -q $SOCK);
# then
#     rm -f $SOCK
#     ssh-agent -a $SOCK > $AGENT_ENV
#     eval $(cat $AGENT_ENV)
# fi

# export LC_ALL=en_US.UTF-8
# default apps
local SSH_KEY="$HOME"/.ssh/id_ed25519
export EDITOR=vi
export TERMINAL=alacritty
export BROWSER=wyeb
export OPENER=mimeopen
export SHELL=zsh
export MENU=wofi
export READER=apvlv
export MANPAGER="$EDITOR +Man!."
export MANWIDTH=999
# Alt-c : change directory.
# Ctrl-t: completion.
# Ctrl-r: search in CLI history command.
#source ${XDG_CONFIG_HOME:-$HOME/.config}/lf/lf-shellcd

# cleaning up home folder
export MPD_HOST=/run/mpd/socket
export WIKI_DIR="$HOME/wiki"
export WLR_NO_HARDWARE_CURSORS=1
# export GDK_BACKEND=xwayland qawl
# export GDK_BACKEND=wayland vlc
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XKB_DEFAULT_OPTIONS=caps:escape
export XKB_DEFAULT_LAYOUT=us
export XDG_RUNTIME_DIR=/run/user/1000
export LYNX_CFG="$HOME/.config/lynx/lynx.cfg"
export LYNX_LSS="$HOME/.config/lynx/lynx.lss"
export FZF_DEFAULT_COMMAND='rg --files --follow --hidden'
export FZF_TMUX=1
export FZF_TMUX_OPTS="-p"
export PATH="$HOME/.vim/plug/fzf-wordnet.vim/bin:$PATH"
export PATH="$PATH:${$(find ~/.local/bin -maxdepth 1 -type d -printf %p:)%%:}"
export PATH="$PATH:/opt/flutter/bin/"
export PATH="$PATH:$HOME/Android/Sdk/cmdline-tools/latest/bin"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:/usr/local/searx/searx-pyenv/bin/activate"
export SEARX_SETTINGS_PATH="/etc/searx/settings.yml"
export PATH="$PATH:/usr/bin"
export PATH="$PATH:/snap/bin/"
export PATH="$PATH:$HOME/.local/go/bin"
export PATH="$PATH:/usr/local/go/bin"
export PATH="$PATH:/var/lib/gems/3.1.0/gems"
# export PATH="$PATH:/root/.cargo/bin/"
# export PATH="$PATH:/.cargo/bin/"
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$GOPATH/bin"
export PYTHONSTARTUP="/etc/python/pythonrc"
export GOPATH="$XDG_DATA_HOME"/go
export ZDOTDIR="$HOME"/.config/zsh
[[ ! -f ${ZDOTDIR:-~}/.p10k.zsh ]] || source ${ZDOTDIR:-~}/.p10k.zsh
fpath+=${ZDOTDIR:-~}/.zsh_functions
export PARINIT="rTbgqR B=.,?_A_a Q=_s>| w160"
# export PATH="$PATH:/opt/Cemu/build/bin/"
# export SSH_AUTH_SOCK=${HOME}/.ssh-agent.sock
# export PATH=$PATH:/home/sandun/.local/bin

# Firefox wayland:
if [ "$XDG_SESSION_TYPE" == "wayland" ]; then
    export MOZ_ENABLE_WAYLAND=1
    # export MOZ_DBUS_REMOTE DEFAULT=1
    export XDG_CURRENT_DESKTOP=sway
    export XDG_SESSION_DESKTOP=sway
    export SDL_VIDEODRIVER=wayland
    export _JAVA_AWT_WM_NONREPARENTING=1
    # for GTK-based programs such as firefox and emacs:
    export GDK_DPI_SCALE=1
    export GDK_SCALE=1
    export GDK_BACKEND=wayland
    export CLUTTER_BACKEND=wayland
    # for QT-based programs
    export QT_WAYLAND_FORCE_DPI="physical"
    # or if still too small, use a custom DPI
    export QT_WAYLAND_FORCE_DPI=192 # 2x scaling
    export QT_QPA_PLATFORM="wayland;xcb"
    export QT_QPA_PLATFORM_PLUGIN_PATH="/usr/lib/x86_64-linux-gnu/qt5/plugins/"
    export QT_AUTO_SCREEN_SET_FACTOR=0
    export QT_SCALE_FACTOR=1
    export QT_FONT_DPI=96
fi

# enable color support of ls, less and man, and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    # Fix color scheme terminal:
    export LS_COLORS="$LS_COLORS:ow=00"
    export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
    export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
    export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
    export LESS_TERMCAP_so=$'\E[01;33m'    # begin reverse video
    export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
    export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
    export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

    alias ls='lsd'
    alias cat='batcat'
    alias more='less'
    alias rm='rm -i'
    alias ps='ps -x'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias diff='diff --color=auto'
    alias ip='ip --color=auto'

    # Take advantage of $LS_COLORS for completion as well
    zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
    zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
fi

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias f='$EDITOR "$(fzf)"'
alias fzf="fzf --preview 'batcat --style=numbers --color=always --line-range :500 {} '"
alias yt="pipe-viewer"
alias showkey="sudo showkey"
# alias cat="batcat"
alias ytg="nohup gtk-pipe-viewer &"
alias doas="doas --"
alias ..="cd .."
alias ...="cd ../.."
alias .3="cd ../../.."
alias .4="cd ../../../.."
alias .5="cd ../../../../.."
alias df="df -h"
alias cp="cp -i"
alias free="free -m"
alias psmem="ps auxf|sort -nr -k 4"
alias psmem10="ps auxf|sort -nr -k 4|head -10"
alias pscpu="ps auxf|sort -nr -k 3"
alias pscpu10="ps auxf|sort -nr -k 3|head -10"
alias addup="git add -u"
alias addall="git add ."
alias branch="git branch"
alias checkout="git checkout"
alias commit="git commit -m"
alias fetch="git fetch"
alias pull="git pull origin"
alias push="git push origin"
alias status="git status"
alias tag="git tag"
alias nwetag="git tag -a"
alias jctl="journalctl -p 3 -xb"
alias c="clear"
# alias config="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
alias play="ncmpcpp-ueberzug"
alias tmux="tmux -f $XDG_CONFIG_HOME/tmux/tmux.conf"
# alias mpi='mpv --config-dir=$HOME/.config/mvi'
alias pip="python3 -m pip"
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"

# LF Ueberzurg integration:
cleanup() {
  # Clean up the tempdir if and only if we created it.
  if [ -n "$LF_SHELLCD_TEMPDIR" ]; then
    rm -rf "$LF_SHELLCD_TEMPDIR" 2>/dev/null
  fi
  # kill $ueberzugpid 2>/dev/null
}

if [ "$(basename $(command -v lf))" != "lf" ]; then
  return 0
else
  lfd () {
    # Set up a temporary directory if not alreay done so by a calling
    # wrapper script (e.g. lfcd).
    if [ -z "$LF_SHELLCD_TEMPDIR" ]; then
      LF_SHELLCD_TEMPDIR="$(mktemp -d -t lf-shellcd-XXXXXX)"
      export LF_SHELLCD_TEMPDIR
    fi
    trap cleanup INT HUP
    lf -last-dir-path "$LF_SHELLCD_TEMPDIR/lastdir" "$@"
    if [ -e "$LF_SHELLCD_TEMPDIR/changecwd" ] &&
      dir="$(cat "$LF_SHELLCD_TEMPDIR/lastdir")" 2>/dev/null; then
      cd "$dir"
    fi
    cleanup
    unset LF_SHELLCD_TEMPDIR
  }
fi
alias lf="lfd"

# Fzf integration
source <(fzf --zsh)
fzf_history() { zle -I; eval $(history | fzf +s | sed 's/ *[0-9]* *//') ; }; zle -N fzf_history; bindkey '^F' fzf_history

fzf_killps() { zle -I; ps -ef | sed 1d | fzf -m | awk '{print $2}' | xargs kill -${1:-9} ; }; zle -N fzf_killps; bindkey '^Q' fzf_killps

fzf_cd() { zle -I; DIR=$(find ${1:-*} -path '*/\.*' -prune -o -type d -print 2> /dev/null | fzf) && cd "$DIR" ; }; zle -N fzf_cd; bindkey '^E' fzf_cd

# Neovim config switcher:
export VIMINIT="set nocp | source ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/vimrc"
alias v="$EDITOR"

function vs() {
  export VIMINIT=""
  items=("default" "NvChad")
  config=$(printf "%s\n" "${items[@]}" | fzf --prompt=" Neovim Config  " --height=~50% --layout=reverse --border --exit-0)
  if [[ -z $config ]]; then
    echo "Nothing selected"
    return 0
  elif [[ $config == "default" ]]; then
    config=""
    export VIMINIT="set nocp | source ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/vimrc"
  fi
  NVIM_APPNAME=$config nvim $@
  export VIMINIT="set nocp | source ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/vimrc"
}

bindkey -s ^a "vs\n"

# Lynx config modular:
if [[ -n $(which lynx) ]]; then
  [[ -r ~/.config/lynx/lynx.cfg ]]  && lynxcfg="-cfg=$HOME/.config/lynx/lynx.cfg"
  [[ -r ~/.config/lynx/lynx.lss ]]  && lynxlss="-lss=$HOME/.config/lynx/lynx.lss"
  alias lynx="lynx '$lynxcfg' '$lynxlss' -use_mouse -justify -homepage=https://www.duckduckgo.com"
fi

#Prompt Login to DE/WM (Sway)
# Make sure this on te bottom of script so all script above can beload
if ( command -v sway > /dev/null ) && [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
    exec sway
    xhost +SI:localuser:root > /dev/null
elif ( command -v tmux > /dev/null ) && [ "${XDG_VTNR}" -eq 2 ] ; then # check if tmux command exists
    if [[ "$(tty)" =~ /dev/tty ]] && [[ ! "$TERM" =~ screen ]] && [ -z "$TMUX" ]; then
        # We're on a TTY and *not* in tmux
        exec tmux -u -f /home/sandun/.config/tmux/tmux.conf
    fi
fi

if [[ $(pidof ssh-agent) -gt 0 ]]; then
    ssh-add "$SSH_KEY" 2> /dev/null
    # ssh -oStrictHostKeyChecking=no \
    #   -i "$SSH_KEY" \
    #   -L 3307:your-db.cluster-ro-abcdefghijkl.ap-southeast-1.rds.amazonaws.com:3306 \
    #   -N ec2-user@13.12.123.123 &
fi
alias config='/usr/bin/git --git-dir=/home/sandun/.dotfiles/ --work-tree=/home/sandun'
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
