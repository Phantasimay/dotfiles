#!/bin/sh

# copy script pada coment dibawah ini ke dalam config sway :
# i3 config in ~/.config/i3/config :
# bar {
#   status_command exec /home/you/.config/i3status/mybar.sh
# }

# Set up Item pada status bar:
# Tambahkan space dengan mengkopi seperlunya setelah space8,
#
bg_bar_color="#32323200"

# Print a left caret separator
# @params {string} $1 text color, ex: "#FF0000"
# @params {string} $2 background color, ex: "#FF0000"
separator() {
#  scale=1.4
  echo "{"
  echo "\"full_text\":\"\","
  echo "\"separator\":false,"
  echo "\"separator_block_width\":0,"
  echo "\"border\":\"$bg_bar_color\","
  echo "\"border_left\":0,"
  echo "\"border_right\":0,"
  echo "\"border_top\":3,"
  echo "\"border_bottom\":4,"
  echo "\"color\":\"$1\","
  echo "\"background\":\"$2\""
  echo "}"
}

common() {
  echo "\"border\": \"$bg_bar_color\","
  echo "\"separator\":false,"
  echo "\"separator_block_width\":0,"
  echo "\"border_top\":3,"
  echo "\"border_bottom\":4,"
  echo "\"border_left\":0,"
  echo "\"border_right\":0,"
}

space1() {
  bg="#FFD180"
  separator $bg $bg_bar_color
  bg_separator_previous=$bg
  icon=
  script=$(date "+%a %d/%m %H:%M")
  echo ",{"
  echo "\"name\":\"date\","
  echo "\"full_text\":\" ${icon} ${script} \","
  echo "\"color\":\"#000000\","
  echo "\"background\":\"$bg\","
  common
  echo "},"
}

space2() {
  bg="#1976D2"
  separator $bg $bg_separator_previous
  bg_separator_previous=$bg
  icon=↑
  script=$(uptime | cut -d ',' -f1  | cut -d ' ' -f4,5)
  echo ",{"
  echo "\"name\":\"up_time\","
  echo "\"full_text\":\" ${icon}${script} \","
  echo "\"color\":\"#FFFFFF\","
  echo "\"background\":\"$bg\","
  common
  echo "},"
}

space3() {
  bg="#424242" # grey darken-3
  separator $bg $bg_separator_previous # background left previous block
  bg_separator_previous=$bg
  icon=
  script=$(ip route get 1 | sed -n 's/.*src \([0-9.]\+\).*/\1/p')
  echo ",{"
  echo "\"name\":\"local_ip\","
  echo "\"full_text\":\" ${icon} ${script} \","
  echo "\"color\":\"#FFFFFF\","
  echo "\"background\":\"$bg\","
  common
  echo "},"
}

space4() {
  bg="#2E7D32"
  separator $bg $bg_separator_previous
  bg_separator_previous=$bg
  icon=
  script=$(iw dev wlan0 link | grep SSID | cut -d: -f2)
  echo ",{"
  echo "\"name\":\"wifi\","
  echo "\"full_text\":\" ${icon}${script} \","
  echo "\"color\":\"#FFFFFF\","
  echo "\"background\":\"$bg\","
  common
  echo "},"
}

space5() {
  bg="#3949AB"
  separator $bg $bg_separator_previous
  bg_separator_previous=$bg
  icon=
  script=$(printf "%.1f" "$(free | grep Mem | awk '{print $3/$2 * 100.0}')")
  echo ",{"
  echo "\"name\":\"RAM_usage\","
  echo "\"full_text\":\" ${icon} ${script}% \","
  echo "\"color\":\"#FFFFFF\","
  echo "\"background\":\"$bg\","
  common
  echo "},"
}

space6() {
  bg=#A3F518
  separator "$bg" $bg_separator_previous
  bg_separator_previous=$bg
  icon=
  script=$(printf "%.1f" "$( grep 'cpu ' /proc/stat | awk '{print ($2+$4)*100/($2+$4+$5)}')")
  echo ",{"
  echo "\"name\":\"CPU_usage\","
  echo "\"full_text\":\" ${icon} ${script}% \","
  echo "\"color\":\"#000000\","
  echo "\"background\":\"$bg\","
  common
  echo "},"
}

space7() {
  bg="#546E7A"
  separator $bg "$bg_separator_previous"
  bg_separator_previous=$bg
  icon=
  script=$(brightnessctl | grep Current | cut -d '(' -f2 | cut -d '%' -f1)%
  echo ",{"
  echo "\"name\":\"brightness\","
  echo "\"full_text\":\" ${icon} ${script} \","
  echo "\"color\":\"#FFFFFF\","
  echo "\"background\":\"$bg\","
  common
  echo "},"
}

## Sempel struktur kode setup space
#space() {						--> Nama Space
#  local bg="#546E7A"					--> Warna Bg
#  separator $bg $bg_separator_previous			--> gunakan warna $bg space sebelumnya sebagai warna sparator
#  bg_separator_previous=$bg				--> Simpan warna $bg untuk digunakan pada command berikutnya
#  icon=						--> Icon space
#  script=$(brightnessctl)%				--> Script logika
#  echo -n ",{"						--> Awal Isi yang akan ditampilkan
#  echo -n "\"name\":\"brightness\","			--> Id penanda spasi untuk klik event
#  echo -n "\"full_text\":\" ${icon} ${script}% \","	--> Isikan command script pada  <cs>
#  echo -n "\"color\":\"#FFFFFF\","			--> Warna font
#  echo -n "\"background\":\"$bg\","			--> Warna Bg yang ditampilkan
#  common
#  echo -n "},"						--> Ahir isis yang akan ditampilkan
#}

space8() {
  if [ -f /sys/class/power_supply/BAT0/uevent ]; then
    bg="#D69E2E"
    separator $bg $bg_separator_previous
    bg_separator_previous=$bg
    prct=$( grep "POWER_SUPPLY_CAPACITY=" /sys/class/power_supply/BAT0/uevent | cut -d'=' -f2)
    charging=$( grep "POWER_SUPPLY_STATUS" /sys/class/power_supply/BAT0/uevent| cut -d'=' -f2) # POWER_SUPPLY_STATUS=Discharging|Charging
    icon=" "
    if [ "$charging" = "Charging" ]; then
      icon=""
    fi
    echo ",{"
    echo "\"name\":\"power\","
    echo "\"full_text\":\" ${icon} ${prct}% \","
    echo "\"color\":\"#000000\","
    echo "\"background\":\"$bg\","
    common
    echo "},"
#  else
#    bg_separator_previous=$bg_separator_previous
  fi
}

spaceA() {
  bg="#673AB7"
  separator $bg $bg_separator_previous
  vol=$(amixer -M get Master |awk -F"[][]" '/Left:/ { print $2 }')
  stv=$(amixer -M get Master |awk -F"[][]" '/Left:/ { print $4 }')
  echo ",{"
  echo "\"name\":\"volume\","
  if [ "$stv" = "off" ]; then
    echo "\"full_text\":\"  ${vol} \","
  else
    echo "\"full_text\":\" ${vol} \","
  fi
  echo "\"background\":\"$bg\","
  common
  echo "},"
  separator "#424242" $bg
}

systemupdate() {
  nb=$(checkupdates | wc -l)
  bg="#424242"
  icon=
  if [ "$nb" -gt 0 ]; then
    echo ",{"
    echo "\"name\":\"id_systemupdate\","
    echo "\"full_text\":\" ${icon} ${nb}\""
    echo "}"
  fi
}

logout() {
  echo ",{"
  echo "\"name\":\"id_logout\","
  echo "\"full_text\":\"\""
  echo "}"
}

# https://github.com/i3/i3/blob/next/contrib/trivial-bar-script.sh
echo '{ "version": 1, "click_events":true }'     # Send the header so that i3bar knows we want to use JSON:
echo '['                    # Begin the endless array.
echo '[]'                   # We send an empty first array of blocks to make the loop simpler:

# Now send blocks with information forever:
# Urutan Penampilan di status bar:
(while :;
do
  echo ",["
  space1
  space2
  space3
  space4
  space5
  space6
  space7
  space8
#Tambahkan Space dibawah ini jika butuh:

#Jangan Edit letak space  dibawah ini :
  spaceA
  systemupdate
  logout
  echo "]"
        sleep 1
done) &

# Set up fungsi click pada status bar:
# click events
echo "$line" > ~/tmp.txt
#  {"name":"local_ip","button":1,"modifiers":["Mod2"],"x":2982,"y":9,"relative_x":67,"relative_y":9,"width":95,"height":22}
while read -r line;
do
  # TIME
  if [ "$line" = "space1" ]; then
    exec alacritty -e ncal &

  # Wifi control
  elif [ "$line" = "space3" ]; then
    exec alacritty -e nmtui &

  # CPU
  elif [ "$line" = "space5" ]; then
    exec alacritty -e top &

  # Browser
  elif [ "$line" = "space6" ]; then
    exec alacritty -e qutebrowser &

  # Calender
  elif [ "$line" = "space7" ]; then
    alacritty -e ~/.config/i3status/click_time.sh &

  # volume
  elif [ "$line" = "spaceA" ]; then
    exec alacritty -e alsamixer &

  # CHECK UPDATES
  elif [ "$line" = "systemupdate" ]; then
    exec alacritty -e sudo nala upgrade &

  # LOGOUT
  elif [ "$line" = "logout" ]; then
    swaynag -t warning -m 'Log out ?' -b 'yes' 'swaymsg exit' > /dev/null &

  fi
done
