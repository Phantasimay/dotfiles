= Herbal penurun darah tinggi:

* link:./acalypha_spp_20231103080924.adoc[Akar kucing]
* link:./prunus_amygdalus_20231222064055.adoc[Almond oil].
* link:./moringa_oleifera_20240910152315.adoc[Daun Kelor].
* link:./pandanus_amaryllifolius_20240919014108.adoc[Daun Pandan].
* link:./syzygium_polyanthum_20231215074417.adoc[Daun Salam].
* link:./plantago_major_20231114052046.adoc[Daun sendok]
* link:/sonchus_arvensis_20240203020350[Daun Tempuyung].
* link:./zingiber_officinale_20231127075544.adoc[Jahe].
* link:./melaleuca_spp_20231218024536.adoc[Kayu putih]
* link:./curcuma_domestica_20240925041534.adoc[Kunyit].
* link:./phyllanthus_niruri_20240206060304.adoc[Meniran].
* link:./pimpinella_alpina_20231127050614.adoc[Purwoceng]
* link:./hibiscus_sabdariffa_20240207021823.adoc[Rosela].
* link:./rosmarinus_officinalis_20231219053626.adoc[Rosmarin]
* link:./apium_graveolens_20240221080420.adoc[Seledri]
* link:./cyperus_rotundus_20240213081101.adoc[Suket Teki]
* link:./sonchus_arvensis_20240203020350.adoc[Tempuyung].
* link:./tribulus_terrestris_20231128022155.adoc[Tribulus]
* link:./olea_europea_20231224205147.adoc[Zaitun].

== Peringatan

* Hanya untuk pasien darah tinggi yang telah ditetapkan oleh dokter<<1>>.
* Selama penggunaan konsultasikan pd dokter secara berkala<<1>>.

== link:$WIKI_DIR/deck/0.adoc[QnA]

Herpal apa saja untuk penurun darah tinggi?::

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./penandaan_OBAOKSK_20240928094611.adoc[Penandaan]

'''''
include::./reference/20231105114947.adoc[20231105114947]
