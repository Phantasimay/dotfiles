= _Euphorbia hirta_
:keywords: herbal, euphorbia, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Euphorbia_hirta_NP.JPG/1024px-Euphorbia_hirta_NP.JPG[Patikan Kebo by wikipedia]

* Species  : E. hirta L.
* Local    : Patikan kebo.
* Synonyms : Asthma-plant.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241113041751.adoc[20241113041751]
