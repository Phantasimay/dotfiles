= _Citrus Hystrix_
:keywords: herbal, citrus, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Citrus_hystrix_Blanco2.408-cropped.jpg/800px-Citrus_hystrix_Blanco2.408-cropped.jpg[Jeruk Purut by wikipedia]

* Species  : C. Hystrix DC.
* Local    : Jeruk Purut.
* Synonyms : Kaffir lime.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241123112848.adoc[20241123112848]
