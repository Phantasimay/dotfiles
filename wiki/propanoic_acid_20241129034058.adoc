= Propanoic Acid
:keywords: btp, herbal
:toc:

* Species  : Propanoic Acid
* Local    : As. Propionat.
* Synonyms : Carboxyethane(C~3~H~6~O~2~).
* INS/CAS  : E280

== Peringatan

* Batas Max. 10000mg/Kg<<1>><<2>>.

== Khasiat

. link:./bahan_pengawet_20241017204243.adoc[Pengawet]<<1>><<2>>

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./calcium_propanoate_20241129041111.adoc[Ca. Propionat]

'''''
include::./reference/20241129034058.adoc[20241129034058]
