= Bare Kali Linux

The idea is to install kali linux in bare bone version so we can expend it base on our need, whitout instaling kali linux bloat. first thing first lets download kali linux image iso:

. you can download https://www.kali.org/get-kali/#kali-installer-images[kali official], i recommend you to download just recommend offline installer image or netinstaller.
. Prepare your usb minimum 4GB.
. Make USB Boot with your favorite USBBoot image maker (http://rufus.ie/en/[rufus], https://etcher.balena.io/[balenaEtcher], or https://help.ubuntu.com/community/mkusb[mkusb]).
. I don’t want to guide you how to make USB Boot.
. Boot to USB and install kali linux.
.. Make partition with formula :
    * swap=2xRam
    * boot=1G
    * storage=Btrfs
.. if you get software selection screen _just checklist_ :
    * Collection of tools
.. and keep _others uncheck_.
+
image::./asset/bare-bones-install.png[Software Collection]

. When first time bootup you will login into TTY session. (these is true form of linux)

=== MindMap

. link:./optimization_repo.adoc[Optimized linux repo]
. link:./favorite.adoc[Favorite]

link:./reference/20230910071803.adoc[20230910071803]
