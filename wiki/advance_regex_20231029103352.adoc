= Advance Regular Expressions.

Some link:./regex_20230911085122.adoc[regex] engine have expand syntax for more complex application.

== Beyond Grouping.

what is the best solution for using many backreference in regex?::
backreference can be give n_me with syntax `(?<name>group)=\k<name>`. it’s equal to `(group)=\1`.

== Regex Comment

How to add comment in regex token?::
We can use syntax `(?#comment)` to
give comment on regex. `(?#` mean comment.

=== Atomic grouping.

Why we need atomic group?::
In grouping syntax `(?>group)` backtrack will be thr_w aw_y after l_av_ng these group. `(?>` make regex engine f_rg_tf_ll. these syntax is alt_rn_t_ve of possesive link:./repetition_regex_20231024151826.adoc[quantifier]. Remember these syntax s_nsit_ve to gro_p ord_r because it use link:./alteration_regex_20231024151535.adoc[alteration].

== Relative Backreference.

How relative backreference work?::
Some regex engine support relative backreference, it use negative number. It will lookng group (capturing and non-capturing) from the reference number to beginning of string.
Exemp:

----
(a)(s)(d)\k<-2> will match asds.
-3 -2 -1    |
    ^=======+`
----

== Recursion.

What is recursion regex?::
Some new regex engine support recursion. `(?0)` or `\R` will *repeating whole regex token* if token before recursion are found.

== Subroutine.

what is subroutine regex?::
Some regex engine support subroutine, it work like recursion but *only repeat token inside capturing group* for `(?<name>group)`:
. `(?&name)` will recall group by name.
. `(?1)` will recall group number.
. `(?+1)` will recall group after number.
. `(?-1)` will recall group before number.

== Match Balanced Constructs.

How to check match balanced string construct?::
Match balanced construct are syntax to keep recrulsive with backtrack prevention to matching recursive character string.
The common syntax are:
. With recursion syntax `b(?:m|(?0))*e` or atomic version `b(?>m|(?0))*e`
. With subroutine syntax `(b(?:m|(?1))*e)` or atomic version `(b(?>m|(?0))*e)`

`b` is what begins the construct.
`m` is what can occure in the middle of the construct.
`e` is what can occure at the end of the construct.

if you want to matching character string with unbalance parentheses use subroutine syntax.

the `b(?>m|(?R))*e` are alternative syntax with better performance. the `(?:b(?R)*e|m)` will match balanced without middle construct. for more flexible application we can use syntax `\A(b(?:m|(\1))*e)\Z`, its The generic regex for checking than string consist of balanching perenthess.

== Seperate Subroutine Definition :

How to make subroutine like-parameter definition in regex token?::
There are special group to make subroutine define in sparate syntax. We can use syntax `(?(DEFINE)(?<name>group))`, in this syntax `DEFINE` are special group where all token inside that subroutine `(?<name>group)` are ignore. It like make definition of a parameter in code syntax, so we can reuse/recall it with subroutine syntax `\k<name>` or `\g<name>`.

Another syntax to use this method are to use _zero quantifier_ on subroutine definition `(?<name>group){0}`.

== Conditional regex.

How to use conditional on regex token?::
`(?(if)then|else)` is use to conditional matching.

== Character subtraction.

What’s character substraction?::
To specificly use part of character classes we can use character substraction `[class-[substract]]`, it will read as part of `class` minus `substraction`.

== Caharacter Intersection.

What’s intersection character are?::
To use specifically character valid in two or more character classes. We can use syntax `[class&&[intersect]]`

== Branch Reset Group.

What is branch reset group?::
it’s some alternate group with use one reference number, we can use syntax `(?|`. `(?|groupA|groupB|groupC)` all alteration in that link:./grouping_regex_20231024153157.adoc[group] use same reference number `\1`.

'''''

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[favorite]
. link:./xml_regex_20231024032337.adoc[xml scheme regex]
. link:./xregexp_js_20231024040940.adoc[XRegExp]
. link:./ruby_regex_20231024055008.adoc[Ruby regex]
. link:./rlang_regex_20231024055320.adoc[R Lang]
. link:./delphi_regex_20231024055627.adoc[Delphi]
. link:./php_regex_20231024060014.adoc[PHP regex]
. link:./pcre_20231024060508.adoc[PCRE]
. link:./pcre2_20231024060235.adoc[PCRE2]
. link:./perl_regex_20231024060730.adoc[Perl]
. link:./jgsoft_regex_20231024061000.adoc[JGsoft]
. link:./boost_regex_20231024061329.adoc[Boost]

link:./reference/20231029103352.adoc[20231029103352]
