= Other Apps

* Gnome Software Center
* WPS (document editor)
* Master Pdf Editor (pdf editor)
* PDFChain+PDFtk (pdf tool)
* Darktable (photo editor)
* CUPS+Gutenprint (Printer driver)
* PipeWire (Audio driver)
* Pipe-Viewer (YT Client)
* newsboat (RSS client)
* neomutt (Mail client)
* Konqueror (Browser web and file)
* Dillo (Lightweight Browser web and file)
* QuteBrowser (Mid-weight Vim-like browser)
* FireFox Dev (WebDev Browser)
* Gpick (Color picker)
* galculator
* redshift (Hue Generator)
* xdman (Linux IDM)
* Clipman
* blueman-manager (bluetooth manager)
* bleachbit (System Cleaner)
* mkusb (USB Boot maker)
* NoTrack (Ads Blocker)
* Tor (Anonymous)
* VsCodium
* Android Studio
* MySQL Workbench
* Postman
* CISCO PacketTracker
* CTPV (Terminal Viewer)
* Wordnet (dictionary)
* fastfatch

'''''

== MindMap

. link:./profile.adoc[Project resurrection]
. link:./basic_system_20230910015902.adoc[Basic system]
. link:./favorite.adoc[favorite]

link:./reference/20230910020145.adoc[20230910020145]
