link:../curcuma_domestica_20240925041534.adoc[20240925041534]

== Reference

* [[1]] BPOM RI. 2008. Taksonomi Volume I: Koleksi Tanaman Obat Kebun Tanaman Obat Citeureup. Hal.32. Direktorat Obat Asli Indonesia. Jakarta.
* [[2]] KEMENKES RI. 2017. Farmakope Herbal Indonesia Edisi II. Hal.268-272. Dirjen Kefarmasian dan Alat Kesehtan. Jakarta.
* [[3]] BPOM RI. 2014. Pedoman Rasionalisasi Komposisi Obat Tradisional Volime I. Hal.8,10,13,23-29,33-37,38,41-49,51-58,60-67,84,89,93-96,103-111. Direktorat Obat Asli Indonesia. Jakarta.
* [[4]] Amir, Nurhidayah dkk. 2023. Terapi Komplementer pada Remaja. Hal.38,39,103,105,109. Yayasan Kita Menulis. Surakarta.
* [[5]] Dahlia,A.Amaliah,dkk. 2023. Tanaman Potensial Peningkat Imunitas dan Pengobatan Tradisional. Hal.46-49. Yayasan Pendidikan Cendikia Muslim. Sumatra Barat.
* [[6]] Hamiyati. 2023. Tanaman Obat. Hal.3,11. APWI. Jakarta Selatan.
* [[7]] Efriwati, dkk. 2023. Pengantar Pangan Fungsional. Hal.11. GetPress Indonesia. Sumatra Barat.
* [[8]] Dirjen Farmakes. 2023. Materia Kosmetika Bahan Alam Indonesia. Hal.158-160. KEMENKES RI. Jakarta.
* [[9]] Rahmawati,Nuning dkk. 2023. Jurnal Tumbuhan Obat Indonesia Vol. 16 No. 2: Pemanfaatan Tumbuhan Obat sebagai Antihiperkolesterolemia di Berbagai Etnis di Pulau Sulawesi. Hal.34-43. KEMENKES RI. Jakarta.
* [[10]] Maryuni, dkk. 2022. Terapi Komplementer pada Kebidanan. Hal.158,163,169. PT Global Eksekutif Teknologi. Padang.
* [[11]] Patria,D.Galih &Sutrisno.A.P. 2022. Pangan Fungsional dan Manfaatnya untuk Kesehatan. Hal.11. UMG Press. Gersik.
* [[12]] KEMENKES RI. 2022. Formularium Fitofarmaka. Hal.19,43,46. Dirjen Kefarmasian dan Alat Kesehatan. Jakarta.
* [[13]] Prameswari,Yuanita. 2022. Ensiklopedia Tumbuhan Obat Berdasarkan Hasil Studi Etnobotani Tumbuhan Obat di Desa Kawasan KPH Saradan. Hal.29. PGRI. Madiun.
* [[14]] Ririn.dkk. 2021. Tumbuhan Berpotensi Obat: Desa Sanrobone, Kabupatem Takalar. Hal.29-40. PT.Nas Media Indonesia. Makassar.
* [[15]] Nurcahyanti.N,dkk. 2021. Tanaman Obat Keluarga Warisan Leluhur Melestarikan Sumber Daya Alam dan kearifan lokal. Hal.123-127. CV.Kaaffah Learning Center. Sulawesi slatan.
* [[16]] Widiyastuti,Yuli dkk. 2021. Seledri (Apium graviolens L.) Tanaman Aromatik Melawan Hipertensi. Hal.71. KEMENKES RI. Jakarta.
* [[17]] KEMENKES RI. 2021. Petunjuk Praktis Asuhan Mandiri Pemanfaatan TOGA dan Akupresur Buku Saku 3. Hal.11,16,35,59. Dirjen Pelayanan Kesehatan. Jakarta.
* [[18]] KEMENKES RI. 2021. Petunjuk Praktis Asuhan Mandiri Pemanfaatan TOGA dan Akupresur Buku Saku 2. Hal.26,34,51. Dirjen Pelayanan Kesehatan. Jakarta.
* [[19]] Tandi,Joni. 2021. Obat Tradisional Hasi Penelitian STIFA Pelita MAs Palu Edisi I. Hal.14,113,115,117,119,121,123,125,127. LPPM STIFA Pelita Mas. Palu.
* [[20]] Satya DS,Bayu. 2021. Koleksi Tumbuhan Berkhasiat. Hal.9,10,20,38,104-108,131-134. Rapha Publishing. Yogyakarta.
* [[21]] BPOM. 2023. PerBPOM No.25 Tentang Kriteria dan Tata Laksana Regristrasi Obat Bahan Alam. BPOM RI. Indonesia.
* [[22]] BPOM. 2023. PerBPOM No.24 Tentang Persyaratan Keamanan dan Mutu Suplemen Kesehatan. BPOM RI. Indonesia
