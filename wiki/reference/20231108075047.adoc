link:../reading_20231108075047.adoc[20231108075047]

== Further Reading:

* Adler, Mortimer.J and Doren, Charles.V. 1972. How to Read a Book.
Simon and Schuster. New York.
* https://improvingliteracy.org/#content
