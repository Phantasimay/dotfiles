link:../morinda_citrifolia_20240525033300.adoc[20240525033300]

== Reference

* [[1]] BPOM RI. 2008. Taksonomi Volume I: Koleksi Tanaman Obat Kebun Tanaman Obat Citeureup. Hal.57. Direktorat Obat Asli Indonesia. Jakarta.
* [[2]] KEMENKES RI. 2017. Farmakope Herbal Indonesia Edisi II. Hal.311-314. Dirjen Kefarmasian dan Alat Kesehtan. Jakarta.
* [[3]] BPOM RI. 2014. Pedoman Rasionalisasi Komposisi Obat Tradisional Volime I. Hal.8,28,38,44,45,53,58,62,64,68,75,76,80,84,85,89,94,104,107,111. Direktorat Obat Asli Indonesia. Jakarta.
* [[4]] Dahlia,A.Amaliah.dkk. 2023. Tanaman Potensial Peningkat Immunitas Tubuh dan Pengobatan Tradisional. Hal.33-35. Yayasan Pendidikan Cendikia Muslim. Sumatra Barat.
* [[5]] Rahmawati,Nuning dkk. 2023. Jurnal Tumbuhan Obat Indonesia Vol.16 No.2: Pemanfaatan Tumbuhan Obat sebagai Antihiperkolesterolemia di Berbagai Etnis di Pulau Sulawesi. Hal.34-43. KEMENKES RI. Jakarta.
* [[6]] Rahmawati,Nuning dkk. 2022. Jurnal Tumbuhan Obat Indonesia Vol.15 No.1: Studi Etnofarmakologi Tumbuhan Obat untuk Pengobatan Hiperkolesterolemia di Kalimantan. Hal.1-15. KEMENKES RI. Jakarta.
* [[7]] Prameswari,Yuanita. 2023. Ensiklopedia Tumbuhan Obat Berdasarkan Hasil Studi Etnobotani Tumbuhan Obat di Desa Kawasan KPH Saradan. Hal.24. PGRI. Madiun.
* [[8]] Satya DS,Bayu. 2021. Koleksi Tumbuhan Berkhasiat. Hal.149-151. Rapha Publishing. Yogyakarta.
* [[9]] Mustofa,F.Indrian, dkk. 2021. Jurnal Tumbuhan Obat Indonesia Vol.14 No.1: Etnomedisin Tumbuhan Obat yang Digunakan oleh Pengobat Tradisional untuk Mengatasi Cidera Tulang di Kalimantan Barat. Hal.29-47. KEMENKES RI. Jakarta.
* [[10]] Ryandini,Yuanita I. 2021. Tanaman Obat untuk Mencegah Covid-19. Hal.7. UPT Laboratorium Herbal Materia Medica. Batu.
* [[11]] KEMENTRIAN PERTANIAN RI. 2021. Buku Saku Tanaman Obat: Warisan Tradisi Nusantara untuk Kesejahtraan Rakyat. Hal.54-56. PUSLITBANG Perkebunan. Jakarta.
* [[12]] Dirjen Pelayanan Kesehayan RI. 2020. SE No: JK.02.02/IV.2243/2020 Tentang Pemanfaatan Obat Tradisional untuk Pemeliharaan Kesehatan, Pencegahan Penyakit dan Perawatan Kesehatan. KEMENKES RI. Jakarta.
* [[13]] Bermawie,Nurliani,dkk. 2020. Potensi Tanaman Rempah, Obat dan Atsiri Menghadapi Masa Pandemi Covid 19. Hal.144-146. LITBANG Pertanian RI. Bogor.
* [[14]] Pranaka,R.Nanda dkk. 2020. Jurnal Tumbuhan Obat Indonesia Vol.13 No.1: Pemanfaatan Tanaman Obat Oleh Masyarakat Suku Melayu di Kabupaten Sambas. Hal.1-24. KEMENTRIAN RI. Jakarta.
* [[15]] BPOM RI. 2020. Informatorium Obat Moderen Asli Indonesia (OMAI) di Masa Pandemi COVID-19. Hal.30-54. Deputi Bidang OTSK. Jakarta.
* [[16]] Ulung,Gagas &LPPM IPB. 2020. 40 Resep Wedang Empon-Empon Penangkal Virus, Penambah Imun. Hal.74. PT.Gramedia Pustaka Utama. Jakarta.
* [[17]] Sihin.dkk. 2019. Tumbuhan Hutan Sekolah Berkhasiat Obat SDN 002 Malinau Selatan Hilir. Hal.60-61. WWF ESD Unit. Indonesia.
* [[18]] Kementrian Pertanian. 2019. Tanaman Obat Warisan Tradisi Nusantara untuk Kesejahteraan Rakyat. Hal.67-69. Balai Penelitian Tanaman Rempah dan Obat. Bogor.
* [[19]] Agoes,Azwar. 2019. Tanaman Obat Indonesia Buku 2. Hal.75-78. Salemba Medika. Jakarta selatan.
* [[20]] Saida dkk. 2019. Jurnal Tumbuhan Obat Indonesia Vol.12 No.2: Eksplorasi Spesies Tumbuhan Berkhasiat Obat Berbasis pengetahuan Lokal di Kabupaten Pidie. Hal.56-67. KEMENKES RI. Jakarta.
* [[21]] BPOM. 2023. PerBPOM No.25 Tentang Kriteria dan Tata Laksana Regristrasi Obat Bahan Alam. BPOM RI. Indonesia.
