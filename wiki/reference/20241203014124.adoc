link:../ascorbyl_salt_20241203014124.adoc[20241203014124]

== References

* [[1]] BPOM. 2023. PerBPOM No.29 Tentang Persyaratan Keamanan dan Mutu Obat Bahan Alam. BPOM RI. Indonesia
* [[2]] BPOM. 2023. PerBPOM No.24 Tentang Persyaratan Keamanan dan Mutu Suplemen Kesehatan. BPOM RI. Indonesia
