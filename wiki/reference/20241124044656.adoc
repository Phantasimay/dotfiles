link:../arthospira_spp_20241124044656.adoc[20241124044656]

== References

* [[1]] BPOM. 2023. PerBPOM No.25 Tentang Kriteria dan Tata Laksana Regristrasi Obat Bahan Alam. BPOM RI. Indonesia.
* [[2]] BPOM. 2023. PerBPOM No.29 Tentang Persyaratan Keamanan dan Mutu Obat Bahan Alam. BPOM RI. Indonesia
