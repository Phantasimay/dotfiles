link:../oryza_sativa_20240824020115.adoc[20240824020115]

= Reference
* [[1]] Yusro, Fathul dkk. 2020. Jurnal Tumbuhan Obat Indonesia Vol.13 No.1: Pemanfaatan Tanaman Obat Oleh Masyarakat Suku Melayu di Kabupaten Sambas. Hal.1-24. B2P2TO-OT. Jawa Tengah.
* [[2]] BPOM RI. 2020. Informatorium Obat Moderen Asli Indonesia (OMAI) di Masa Pandemi COVID-19. Hal.34-37. Deputi Bidang OTSK. Jakarta.
* [[3]] Widiyastuti, Yuli dkk. 2020. Budidaya dan Manfaat Sirih untuk Kesehatan. Hal.59. B2P2TO-OT. Jakarta.
* [[4]] Sasongkowati,Retno. 2020. 13 Terapi Buah Sakti Penghancur Penyakit. Hal.77. Desa Pustaka Indonesia. Jawa Tengah.
* [[5]] Kementrian Pertanian. 2019. Tanaman Obat Warisan Tradisi Nusantara untuk Kesejahteraan Rakyat. Hal.35. Balai Penelitian Tanaman Rempah dan Obat. Bogor.
* [[6]] Agoes,Azwar. 2019. Tanaman Obat Indonesia Buku 3. Hal.47,69,92. Salemba Medika. Jakarta selatan.
* [[7]] Agoes,Azwar. 2019. Tanaman Obat Indonesia Buku 2. Hal.35,59. Salemba Medika. Jakarta selatan.
* [[8]] Agoes,Azwar. 2019. Tanaman Obat Indonesia Buku 1. Hal.53,86. Salemba Medika. Jakarta selatan.
* [[9]] Arifin,Zainol,dkk. 2019. Jamu Tradisional Ditinjau dari Aspek Ekonomi dan kesehatan. Hal.21,142. IRDH. Malang.
* [[10]] Ernawati,Lia. 2019. Daun-Daun dan Buah-Buah Penumpas Penyakit. Hal.41. Laksana. Yogyakarta.
* [[11]] Widjaja,Cyntia. 2019. 365 Tips Sehat Tanpa Sakit. Hal.4,32,68,103. Desa Pustaka Indonesia. Jawa Tengah.
* [[12]] Silalahi,Marina dkk. 2018. Tumbuhan Obat Sumatra Utara Jilid I: Monokotildon. Hal.3. UKI Press. Jakarta.
* [[13]] Asmaliyah,dkk. 2018. Tumbuhan Obat dan Herbal dari Hutan untuk Penyakit Degeneratif Metabolik Gaya Hidup Kembali ke Alam. Hal.31,103,139,149. UNSARI Press. Palembang.
* [[14]] Sopandi. 2018. Tanaman Obat Tradisional Jilid III. Hal.15,33,34. PT.Sarana Pancakarya Nusa. Bandung.
* [[15]] Ellis,Lioni. 2018. Super Plants for Super Health. Hal.13-15. Metagraf. Solo.
* [[16]] El-Basyier, Zainul.A. 2018. Sehat dengan 65 Tanaman Obat. Hal.61. PT. Sunda Kelapa Pustaka. Jakarta.
* [[17]] Kriswanto, Agung. 2018. Primbon Padukuhan Teks Pengobatan dari Tradisi Merapi-Merbabu. Hal.71. Perpus Press. Jakarta.
* [[18]] Sukini. 2018. Jamu Gendong Solusi Sehat Tanpa Obat. Hal.26-27. KEMENDIKBUD. Jakarta.
* [[19]] BPOM. 2023. PerBPOM No.25 Tentang Kriteria dan Tata Laksana Regristrasi Obat Bahan Alam. BPOM RI. Indonesia.
