link:../praklinik_ot_20241227035043.adoc[20241227035043]

== References

* [[1]] BPOM. 2023. PerBPOM No.20 Tentang Pedoman Uji FarmakodinamiFarmakodinamik Praklinik Obat Tradisional. BPOM RI. Indonesia.
* [[2]] MENTAN. 2007. PerMENTAN No.44/Permentan/OT.140/5/2007 Tentang Pedoman Berlaboratorium Veteriner yang Baik. MENTAN RI. IndonesiIndonesia.
* [[3]] Laurance,D.R and A.L.Bacharach. 1964. Evaluation of Drug Activities: Pharmacometrics Vol.I. Hal.161. Academic Press. London.
* [[4]] FDA. 2005. Guidance For Industry: Estimating The Maximum Safe Starting Dose in Initial CLinical Trials for Therapeutics in Adult Healthy Volunteers. FDA CDER. USA.
