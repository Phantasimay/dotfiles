link:../linux_debug_20230911083844.adoc[20230911083844]

= Further Reading :

* http://epsi-rns.github.io/system/2023/05/01/driver-interface.html
* https://wiki.ubuntu.com/Kernel/Debugging
* https://wiki.archlinux.org/title/Systemd/Journal
* https://www.pragmaticlinux.com/2021/03/manually-mount-a-usb-drive-in-the-linux-terminal/
