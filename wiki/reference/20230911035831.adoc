link:../tui_20230911035831.adoc[20230911035831]

= Further Reading :

* https://www.wikihow.com/Setup-and-Use-the-Tor-Network[Setup tor]
* https://www.linuxuprising.com/2018/10/how-to-install-and-use-tor-as-proxy-in.html[Install tor]
* https://askubuntu.com/questions/833021/could-not-bind-to-127-0-0-19050-address-already-in-use-is-tor-already-running[Is my tor working]
* https://www.computerhilfen.de/english/lynx-browser-how-to-make-a-meta-refresh-reload-page.html[Lynx Meta Refresh]
* https://stackoverflow.com/questions/10005225/map-keys-in-lynx-to-run-shell-command[Lynx add script]
* https://unix.stackexchange.com/questions/295847/lynx-read-urls-from-file-and-download-links[Extrac URL with lynx]
* https://somedudesays.com/2021/09/what-is-rss-and-atom/[RSS and Atom]
* https://zapier.com/blog/how-to-find-rss-feed-url/[Atom/RSS Tips]
* https://wiki.archlinux.org/title/Newsboat[Newsboat]
* https://mpd.readthedocs.io/en/stable/user.html[MPD]
* https://www.alsa-project.org/main/index.php/Asoundrc#JACK_plugin[Alsa-Jack]
* https://linuxaudiofoundation.org/musiclounge-configure-mpd/[config mpd]
* https://jcorporation.github.io/myMPD/configuration/[myMPD]
* https://newsboat.org/releases/2.13/docs/newsboat.html#_filter_language[Newsboat syntax]
* https://wiki.archlinux.org/title/Web_feed[Web Feed]
