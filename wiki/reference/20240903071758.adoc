link:../channa_striata_20240903071758.adoc[20240903071758]

= Reference:

* [[1]] Hendika, Y. dkk. 2024. Forte Journal Vol.4 No.2: Aktivitas Penyembuhan Luka Bakar Sediaan Albumin Ikan Gabus(Channa striata). Hal.495-501. UHSU. Sumatra Utara.
* [[2]] Fitrianti, E. dkk. 2023. FILOGENI Vol.3 No.2: Efektivitas Albumin Ekstrak Ikan Gabus(Channa striata) Terhadap Penyembuhan Luka Pascaoperasi pada Kucing Domestik di UPTD Puskeswan Makassar. Hal.79-84. UIN Alauddin. Makassar.
* [[3]] Khairunnisa. 2022. Jurnal Ilmiah Makisitek Vol.7 No.4: Pengaruh Perbandingan Konsentrasi Fase Minyak Salep Ekstrak Ikan Gabus(Channa striata) Terhadap Lama Penyembuhan Luka Sayat pada Tikus Putih Gulir Wistar(Rattus norvegicus). Hal.33-38. UMSUT. Indonesia.
* [[4]] Komari, N. dan Gusti I.J. 2022. Jurnal UPI Vol.2 No.1: Virtual Screening Peptida Aktiv Antikangker dari Myosin Ikan Gabus(Channa striata). Hal.84-93. Universitas Lambung Mangkurat. Indonesia.
* [[5]] Harahap, H.S. 2022. Formulasi dan Uji Efektivitas Anti-Aging Sediaan Krim Ekstrak Ikan Gabus(Channa striata). Universitas Aufa Royhan. Padang.
* [[6]] Qunifah, V. 2022. Karakterisasi dan Uji Aktivitas Antidiabetes Albumin Ikan Gabus(Channa striata) Terhadap Mencit(Mus musculuc L.). Universitas Lampung. Bandar Lampung.
* [[7]] Amperawati, M. dan Nanang K.U. 2020. Jurnal Skala Kesehatan Vol.11 No.1: Sediaan Nanopartikel Kitosan Ekstrak Ikan Gabus(Channa striata) dan Uji Aktivitas Albumin Terhadap Penyembuhan Luka Pasca Pencabutan Gigi. Hal.12-20. Poltekes Kemenkes Banjarmasin. Banjarmasin.
* [[8]] Fauziah, M. dan Firinda S. 2020. Jurnal Penelitian Perawat Profesional Vol.2 No.1: Efektivitas Ekstrak Gabus Sebagai Antihipertensi. Hal.69-69. Universitas Lampung. Indonesia.
* [[9]] Hendriati, L. et al. 2019. Traditional Medicine Journal Vol.24 No.3: The Influence of Channa Striata Extract Emulgel on Incision Wound Healing in White Rats. Hal.210-215. Universitas Airlangga. Surabaya.
* [[10]] Ningrum, Dwi I.L. 2018. Pengaruh Pemberian Ekstrak Ikan Gabus(Channa striata) pada Struktur Histologi Hati Mencit(Mus musculuc) Hiperglikemik. ITS. Surakarta.
* [[11]] Sunarno, dkk. 2018. Buletin Anatomi dan Fisiologi Vol.3 No.2: Respon Histologis Hepar Tikus Wistar yang Mengalami Stres Fisiologis Setelah Pemberian Pakan dengan Suplementasi Daging Ikan Gabus(Channa striata). UNDIP. Semarang.
* [[12]] Mayasari, Andi W.M. 2018. Efektivitas Ekstrak Ikan Haruan(Channa striata) Terhadap Kadar Alkalin Fosfatase pada Odontoblast Cell Line. UNHAS. Makassar.
* [[13]] Andrie, M. dan Dies Sihombing. 2017. Pharm Sic Res Vol.4 No.2: Efektivitas Sediaan Salep yang Mengandung Ekstrak Ikan Gabus(Channa striata) pada Proses Penyembuhan Luka Akut Stadium II Terbuka pada Tikus Jantan Galur Wistar. Hal.88-101. Universitas Tanjungpura. Pontianak.
* [[14]] Sari, D.E. dkk. 2016. Life Science Vol.5 No.1: Uji Aktivitas Antibakteri Tepung Ikan Gabus(Channa striata) Terhadap Bakteri Patogen Pangan. Hal.25-30. IKIP PGRI Madiun. Indonesia.
* [[15]] Hartini, P.S. dkk. 2015. Dentofasial Vol.14 No.1: Ekstrak Ikan Haruan(Channa striata) Menurunkan Jumlah Makrofag pada Fase Inflamasi Proses Penyembuhan Luka. Hal.6-10. Universitas Lmbung Mangkurat. Banjarmasin.
* [[16]] Izzaty, A. dkk. 2014. Dentofasial Vol.13 No.3: Ekstrak Ikan Haruan(Channa striata) Menurunkan Jumlah limfosit pada Fase Inflamasi Proses Penyembuhan Luka. Hal.176-181. Universitas Lmbung Mangkurat. Banjarmasin.
* [[17]] BPOM. 2023. PerBPOM No.25 Tentang Kriteria dan Tata Laksana Regristrasi Obat Bahan Alam. BPOM RI. Indonesia.
