link:../ageratum_conyzoides_20240516081819.adoc[20240516081819]

== Reference

* [[1]] BPOM RI. 2008. Taksonomi Volume I: Koleksi Tanaman Obat Kebun Tanaman Obat Citeureup. Hal.5. Direktorat Obat Asli Indonesia. Jakarta.
* [[2]] KEMENKES RI. 2017. Farmakope Herbal Indonesia Edisi II. Hal.40-44. Dirjen Kefarmasian dan Alat Kesehtan. Jakarta.
* [[3]] BPOM RI. 2014. Pedoman Rasionalisasi Komposisi Obat Tradisional Volime I. Hal.7,63. Direktorat Obat Asli Indonesia. Jakarta.
* [[4]] Dahlia,A.Amaliah.dkk. 2023. Tanaman Potensial Peningkat Immunitas Tubuh dan Pengobatan Tradisional. Hal.22-23. Yayasan Pendidikan Cendikia Muslim. Sumatra Barat.
* [[5]] Satya DS,Bayu. 2021. Koleksi Tumbuhan Berkhasiat. Hal.15-16. Rapha Publishing. Yogyakarta.
* [[6]] Rahmadianti,Indah dkk. 2021. Ensiklopedia Tanaman Obat di Kawasan Hutan lindung Wonomulyo, Magetan. Hal.23. UNIPMA Press. Jawa Timur.
* [[7]] Rahmawati,Fitria dkk. 2021. Buletin Materia Medica Batu: Terminologi dan Kegunaan Beberapa Tanaman Obat Edisi 1. Hal.29. UPT Laboratorium Herbal Materia Medica. Batu.
* [[8]] KEMENTRIAN PERTANIAN RI. 2021. Buku Saku Tanaman Obat: Warisan Tradisi Nusantara untuk Kesejahtraan Rakyat. Hal.6-8. PUSLITBANG Perkebunan. Jakarta.
* [[9]] Kementrian Pertanian RI. 2019. Tanaman Obat Warisan Tradisi Nusantara untuk Kesejahtraan Rakyat. Hal.8-10. BALITBANG Pertanian. Bogor.
* [[10]] Zakaria. 2019. Fitokimia Tumbuhan Artocarpus. Hal.11-13. Sahifah. Aceh.
* [[11]] Asmaliyah,dkk. 2018. Tumbuhan Obat dan Herbal dari Hutan untuk Penyakit Degeneratif Metabolik Gaya Hidup Kembali ke Alam. Hal.27-28. UNSIR Press. Palembang.
* [[12]] Sopandi. 2018. Tanaman Obat Tradisional Jilid I. Hal.29-34. PT.Sarana Pancakarya Nusa. Bandung.
* [[13]] Ellis,Lioni. 2018. Super Plants for Super Health. Hal.126-128. Metagraf. Solo.
* [[14]] Karyati dan M. Agus Adhi. 2018. Janis-jenis Tumbuhan Bawah di Hutan Pendidikan Fakultas Kehutanan Universitas Mulawarman. Hal.22-23. Mulawarman University Press. Kalimantan Timur.
* [[15]] Wahyuni, Indra dkk. 2018. Ensiklopedia Tanaman Pangan dan Obat Berbasis Pemanfaatan Keaneragaman Hayati di Masyarakat Adat Baduy Dalam. Hal.85-86. FKIP Untirta Publishing. Banten.
* [[16]] Fitmawati dan Juliantari,Erwina. 2017. Tanaman Obat dari Semak Menjadi Obat. Hal.16-19. UR Press. Riau.
* [[17]] Syamsiyah dkk. 2016. Tumbuhan Obat Tradisional Etnis Lokal Sulawesi Barat. Hal.13. Alauddin University Press. Sulawesi Barat.
* [[18]] Badrunasar,Anas dan Santoso, Harry.B. 2017. Tumbuhan Liar Berkhasiat Obat. Hal.25-30. Forda Press. Jawa Barat.
* [[19]] Hakim,Luchman. 2015. Rempah dan Herba Kebun Pekarangan Rumah Masyarakat: Keragaman, Sumber Fitofarmaka dan Wisata Kesehatan-kebugaran. Hal.133. Danra Creative. Sleman.
* [[20]] Napitupulu,Rodame.M dan Hidayat,Syamsul. 2015. Kitab Tumbuhan Obat. Hal.44. AgriFlo(Penebar Swadaya Group). Jakarta Timur.
* [[21]] BPOM. 2023. PerBPOM No.25 Tentang Kriteria dan Tata Laksana Regristrasi Obat Bahan Alam. BPOM RI. Indonesia.
