link:../printer_driver_20230911021127.adoc[20230911021127]

= Further Reading :

* https://askubuntu.com/questions/873640/how-to-solve-canon-pixma-g2000-driver-problems-on-ubuntu-16-04/943899#943899
* https://askubuntu.com/questions/951251/install-canon-printer-pixma-g2000-in-ubuntu-16-04
* https://www.youtube.com/watch?v=fqBgu6bNlYg
* https://opensource.com/article/19/1/cups-printing-linux
* https://itsubuntu.com/linux-printers-drivers-and-utilities-with-download-link/
* https://wiki.archlinux.org/title/CUPS/Printer-specific_problems#Canon
* https://canonpixmadriverdownload.com/canon-pixma-g2010-drivers-download/
* https://forum.manjaro.org/t/print-settings-no-longer-available-why/60996
* https://itsubuntu.com/how-to-install-canon-printer-driver-in-ubuntu-22-04-lts/
* https://openprinting.github.io/
