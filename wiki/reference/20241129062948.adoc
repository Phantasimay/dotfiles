link:../ethylparaben_20241129062948.adoc[20241129062948]

== References

* [[1]] BPOM. 2023. PerBPOM No.29 Tentang Persyaratan Keamanan dan Mutu Obat Bahan Alam. BPOM RI. Indonesia
* [[2]] BPOM. 2023. PerBPOM No.24 Tentang Persyaratan Keamanan dan Mutu Suplemen Kesehatan. BPOM RI. Indonesia
