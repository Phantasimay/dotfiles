link:../talinum_paniculatum_20231128053007.adoc[20231128053007]

== Reference:

* [[1]] BPOM RI. 2008. Taksonomi Volume I: Koleksi Tanaman Obat Kebun Tanaman Obat Citeureup. Hal.92. Direktorat Obat Asli Indonesia. Jakarta.
* [[2]] BPOM RI. 2014. Pedoman Rasionalisasi Komposisi Obat Tradisional Volime I. Hal.113. Direktorat Obat Asli Indonesia. Jakarta.
* [[3]] KEMENKES RI. 2021. Petunjuk Praktis Asuhan Mandiri Pemanfaatan TOGA dan Akupresur Buku Saku 2. Hal.9-10. DIRJEN Pelayanan Kesehatan. Jakarta.
* [[4]] Perlupi,Bambang,dkk. 2019. Koleksi Tanaman Kebun Sekolah SDN Tahai Baru 2. Hal.31. WWF-ESD Unit. Indonesia.
* [[5]] Bardan,Sri N. 2018. Tanaman Berkhasiat Obat. Hal.46. PT. Sunda Kelapa Pustaka. Jakarta Selatan.
* [[6]] Badrunasar,Anas dan Santoso, Harry.B. 2017. Tumbuhan Liar Berkhasiat Obat. Hal.152-154. Forda Press. Jawa Barat.
* [[7]] MENKES RI. 2016. Peraturan Mentri Kesehatan Republik Indonesia Nomor 6 Tahun 2016 Tentang Formularium Obat Asli Indonesia. Hal.211-212. KEMENKES RI. Jakarta.
* [[8]] Hidayat,Samsul,dkk. 2016. Jalur Wisata Tumbuhan Obat di Kebun Raya Bogor. Hal.259. LIPI Press. Bogor.
* [[9]] Napitupulu,Rodame.M dan Hidayat,Syamsul. 2015. Kitab Tumbuhan Obat. Hal.140. AgriFlo(Penebar Swadaya Group). Jakarta Timur.
* [[10]] Hariana,Arief. 2013. 262 Tumbuhan Obat dan Khasiatnya. Hal.335-336. Penebar Swadaya. Jakarta.
* [[11]] Wulandari,Ari dan Suparni. 2012. Herbal Nusantara : 1001 Ramuan Tradisional Asli Indonesia. Hal.98-99. Rapha-ANDI Publishing. Yogyakarta.
* [[12]] Kinho,Julianus.dkk. 2011. Tumbuhan Obat Tradisional di Sulawesi Utara Jilid I. Hal.94-96. Kementrian Kehutanan RI. Manado.
* [[13]] KEMENKES RI. 2011. Formularium Obat Herbal Asli Indonesia Volume 1. Hal.179-181. DIRJEN Bina Gizi dan KIA. Jakarta.
* [[14]] KEMENKES RI. 2011. 100 Top Tanaman Obat Indonesia. Hal.194. B2P2TOOT. Jakarta.
* [[15]] Lestari,Puji dan Yogasmara,erryga. 2010. Buku Pintar Keluarga Sehat Panduan Praktis Hidup Sehat bagi Seluruh Anggota Keluarga. Hal.92,99. PT.Gramedia Pustaka Utama. Jakarta.
* [[16]] Widodo, Harto dan Fitriana. 2008. Olahan Sehat Berkhasiat Obat. Hal.2,20. B2P2TO-OT. Jawa Tengah.
* [[17]] AgroMedia. 2008. Buku Pintar Tanaman Obat: 431 Jenis Tanaman Penggempur Aneka Penyakit. Hal.202,229. PT.Agromedia Pustaka. Jakarta Selatan.
* [[18]] Dalimartha, Setiawan. 2008. 1001 Resep Herbal. Hal.46,191,204. Penebar Swadaya. Depok.
* [[19]] Dalimartha,Setiawan. 2002. Atlas Tumbuhan Obat Indonesia Jilid I. Hal.136-138. Trubus Agriwidya. Depok.
* [[20]] Wijayakusuma,Hembing. 1992. Masakan untuk Pengobatan dan Kesehatan. Hal.7,10,108. PUSTAKA KARTINI. Jakarta.
* [[21]] BPOM. 2023. PerBPOM No.25 Tentang Kriteria dan Tata Laksana Regristrasi Obat Bahan Alam. BPOM RI. Indonesia.
