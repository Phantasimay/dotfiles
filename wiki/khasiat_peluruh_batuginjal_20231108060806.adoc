= Herbal pemecah batu ginjal.

* link:./acalypha_spp_20231103080924.adoc[Anting-anting].
* link:./oroxylum_indicum_20240225063253.adoc[Bungli]
* link:./centella_asiatica_20231128071631.adoc[Daun Pegagan].
* link:./plantago_major_20231114052046.adoc[Daun sendok]
* link:/sonchus_arvensis_20240203020350[Daun Tempuyung].
* link:./cinnammomum_burmanni_20231217140313.adoc[Kayu Manis].
* link:./curcuma_domestica_20240925041534.adoc[Kunyit].
* link:./phyllanthus_niruri_20240206060304.adoc[Meniran].
* link:./pimpinella_alpina_20231127050614.adoc[Purwoceng]
* link:./hibiscus_sabdariffa_20240207021823.adoc[Rosela].
* link:./caesalpinia_sappan_20240118080253.adoc[Secang]
* link:./apium_graveolens_20240221080420.adoc[Seledri]
* link:./cyperus_rotundus_20240213081101.adoc[Suket Teki]
* link:./sonchus_arvensis_20240203020350.adoc[Tempuyung].
* link:./tribulus_terrestris_20231128022155.adoc[Tribulus].

== Peringatan

* Tidak direkomendasikan u/ konsumsi secara terus menerus<<1>>.
* Harus disertai minum air putih >2 L/hari<<1>>.
* Bila setelah penggunaan >3 hari tidak ada perbaikan segera hubungi dokter<<1>>.

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./penandaan_OBAOKSK_20240928094611.adoc[Penandaan]

'''''
include::./reference/20231108060806.adoc[20231108060806]
