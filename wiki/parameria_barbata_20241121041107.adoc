= _Parameria Barbata_
:keywords: herbal, parameria, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/%E9%95%B7%E7%AF%80%E7%8F%A0_Parameria_barbata_-%E6%B3%B0%E5%9C%8B%E6%B8%85%E9%82%81%E8%8A%B1%E5%B1%95_Royal_Flora_Ratchaphruek%2C_Thailand-_%289237497841%29.jpg/800px-%E9%95%B7%E7%AF%80%E7%8F%A0_Parameria_barbata_-%E6%B3%B0%E5%9C%8B%E6%B8%85%E9%82%81%E8%8A%B1%E5%B1%95_Royal_Flora_Ratchaphruek%2C_Thailand-_%289237497841%29.jpg[Kayu rapet by Wikidata]

* Species  : P. barbata (Blume) K.Schum(Syn. Urceola laevigata (Juss.) D.J.Middleton & Livsh.)
* Local    : Kayu Rapet.
* Synonyms : Cebu Balsam.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241121041107.adoc[20241121041107]
