= Ketangguhan diri.
:keywords: sikologi
:toc:

== GAMES penguat ketanguhan.
. ``G``ratitude (Rasa syukur).
+
Verbal => Tindakan timbal balik => Rasional.
link:./todo/jurnal_syukur_20250129091809.adoc[Jurnal Syukur] komitmen isi tiap hari selama 3 minggu .

=== Memupuk rasa syukur.
. Balita : mencontoh ortu.
. Anak kecil : mulai belajar mengucapkan syukur.
. Anak belum masuk sekolah, saat berkumpul biasakan :
  * Mensyukuri hal baik.
  * Mengutarakan hal yg kurang disukai.
  * Mengutarakan harapan.
. Anak sekolah dasar : biasakan bersukur atas apa yg diterima.
. Anak remaja : mulai belajar berkontribusi untuk masyarakat.

. ``A``utonomy.
. ``M``otivation.
. ``E``mpathy.
. ``S``elf-regulation.



'''''

== MindMap
. link:./deck/0.adoc[QnA]
. link:./favorite.adoc[Favorite]
. link:./parenting_20250127071811.adoc[Parenting]

include::./reference/[]

