= SWAP, ZRAM, ZSWAP, and ZCACHE

For optimal resource usage on low spec machine `(ram < 4G)`, linux have memory optimization in every situation. Lets configure memory optimization to realize memory compression and achieve memory expansion:
. https://haydenjames.io/linux-performance-almost-always-add-swap-space/[SWAP] : Phisical partition for save unuse cache.
. https://github.com/ecdye/zram-config[ZRAM] : a compress RAM base block device, good for machine without swap.
. https://linuxreviews.org/Zswap[ZSWAP] : a compress cache on swap partition.
. https://lwn.net/Articles/397574/[ZCACHE] : a compress cache page on virtual RAM thingy (transcendent memory), depreciated.

Installation zram:
. Open Terminal.
. Run `modinfo` to confirm zram support.
. Run `lsmod|grep zram` to confirm zram not enabled.
. Run `sudo apt-get install zram-config`.
. That is it. Zram automatically configure and activated.
. To *remove* zram run `sudo apt-get remove --purge zram-config zramswap-enabler`.

*Note :* We can use swap to add memory expansion. _Keep in mine to setup swap=2xRam_.

== For Optimization

`/etc/sysctl.conf`

[source,config]
----
###################################################################
# Magic system request Key
# 0=disable, 1=enable all, >1 bitmask of sysrq functions
# See https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html
# for what other values do
#kernel.sysrq=438
#swab setting zram
#vm.swappiness=100
vm.vfs_cache_pressure=250
vm.dirty_background_bytes=16777216
vm.dirty_bytes=50331648
#vm.dirty_background_ratio=1
#vm.dirty_ratio=20
#vm.pagecluster=0
vm.extfrag_threshold=0
vm.min_free_kbytes =102400
vm.laptop_mode = 5
----

. `swappiness` karnel setting to avoid use swap, range: 0-100. Lower value cause karnel to avoid swap.
. `vfs_cache_pressure` karnel setting to reclaim memory, range 50-500. Lower value cause ram fully usage.
. `pagecluster` reduce some useless SwapCached from swap-in.
. `extfrag_threshold` karnel will compact memory or direct reclaim it, range: (-1)-1000. Karnel will not compact memory if fragmentation index `<= extfrag_threshold`.
. `min_free_kbytes` force karnel to keep a minimum number of free ram.
. `dirty_background_ratio` background processes will start writing right away when it hits the % limit, range : 1-10.
. `dirty_background_bytes` background processes wills start writing right away when it hits the bytes limit.
. `dirty_bytes` the system won’t force asynchronous I/O until it get to bytes ratio treshold.
. `dirty_ratio` the system won’t force asynchronous I/O until it get to % dirty ratio, range : 20-50.

*Note :* we can check each parameter value with command `cat /proc/sys/vm/$PARAMETER`

. Open `/etc/fstab`, and copy UUID of swap ($UUID).
. `/etc/default/grub`

[source,config]
----
GRUB_DEFAULT=0
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
GRUB_CMDLINE_LINUX_DEFAULT+="zswap.enabled=1 zswap.compressor=lz4hc zswap.max_pool_percent=20 zswap.zpool=zsmalloc"
GRUB_CMDLINE_LINUX_DEFAULT+="pcie_aspm=force acpi_osi=linux acpi_backlight=native resume=$UUID mem_sleep_default=deep"
GRUB_CMDLINE_LINUX=""
GRUB_DISABLE_OS_PROBER=true         # for dual boot and not use LVM or raw disk devices
----

. `/etc/initramfs-tools/conf.d/resume`

[source,config]
----
RESUME=UUID=$UUID
----

. `/etc/initramfs-tools/modules`

[source,config]
----
lz4hc
lz4hc_compress
----

. Update GRUP and initramfs
  `sudo update-grub && sudo update-initramfs -c -k all`.
. Reboot.

== MindMap

. link:./favorite.adoc[Favorites]
. link:./optimization_20230910073620.adoc[Optimization power]
. link:./optimization_repo.adoc[Optimization repository]
. link:./optimization_cpu_20230910081137.adoc[Optimization CPU]
. link:./optimization_other.adoc[Optimization other]

link:./reference/20230910075722.adoc[20230910075722]
