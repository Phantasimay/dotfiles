= PHP Regular Expressions Function.

There are 3 type of PHP flavour regex:
. `ereg` use POSIX ERE library for old version.
. `mb_ereg` expansion of ereg with additional character support.
. `preg` use PCRE library for newer version(4.2 or later).

== Feature Support:

. All character are link:./literal_character_20231024092839.adoc[literal] except link:./bre_ere_20231023140222.adoc[ERE].
. link:./anchors_regex_20231024091633.adoc[Escape sequence].
. Character, Control character, NULL, Hexadecimal and Octal escape.
. Eager link:./alteration_regex_20231024151535.adoc[alteration].
. All character inside `[]` are literal, except `]^\-`.
. POSIX ASCII and Unicode classes support.
. link:./shorthand_character_20231022150233.adoc[Shorthand] ASCII and Unicode support.
. String, line and attempt link:./anchors_regex_20231024091633.adoc[anchor].
. POSIX and Unicode link:./anchors_regex_20231024091633.adoc[word boundary] support.
. Greedy `*` (star), possessive `+`, lazy `?` and fix `{N}` link:./repetition_regex_20231024151826.adoc[quantifier].
. link:./literal_character_20231024092839.adoc[Unicode] grapheme, category, script, property and code point.
. Named, non-capturing, duplicate and atomic link:./grouping_regex_20231024153157.adoc[group].
. Named, nested, relative and failed link:./grouping_regex_20231024153157.adoc[backreference].
. link:./grouping_regex_20231024153157.adoc[Forwardreference].
. link:./advance_regex_20231029103352.adoc[Comments] support.
. link:./advance_regex_20231029103352.adoc[Branch reset group].
. link:./lookaround_regex_20231024153449.adoc[Lookaround] support.
. link:./advance_regex_20231029103352.adoc[Conditional] regex support.
. link:./mode_modifier_regex_20231030014212.adoc[Mode modifier] support.
. Named, forward, definition and relative link:./advance_regex_20231029103352.adoc[subroutine] call.
. link:./advance_regex_20231029103352.adoc[Recursion], atomic recursion and revert recursion regex.

== Regex Syntax:

PHP regex flavor metacharacter can be escape with link:./shell_supressor_20231028172059.adoc[suppressor].
. ereg syntax: `Output Command(Pattern, Repalacment, String[, Opt])`
+
Command :
  * ereg => sensitive matching.
  * eregi => insensitive matching.
  * ereg_repalace => global reculsive repalacment.
  * split => split string.

. mb_ereg syntax: mb_ereg are extenssion of ereg with multi byte (ASCII and Unicode) support.
+
`Output Command(Pattern, Repalacment, String[, Opt])`
+
Command :
  * mb_ereg => sensitive matching.
  * mb_regex_encoding => insensitive matching.

. preg syntax: PHP preg use nonalphanumeric `[:^alnum:]` as delimiter of regex pattern.
+
`Output Command(/Pattern/, Repalacment, String[, Opt])`
+
Command :
  * preg_match => matching.
  * preg_match_all => recursive matching.
  * preg_grep => recursive global matching.
  * preg_repalace => repalacment.
  * preg_repalace_callback => repalacment callback.
  * preg_split => split string.
+
Output can be:
* int => decimal.
* string => string.
* bool => boolean.
* array => array output.
* mixed => array or string.
+
Opt :
* flags.
* limit.
* groups.

'''''

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[favorite]
. link:./regex_20230911085122.adoc[regex]

link:./reference/20231024060014.adoc[20231024060014]
