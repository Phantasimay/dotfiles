= _Lumbricus Rubellus_
:keywords: herbal, lumbricus, lowrisk, sst
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/7/74/Lumbricus.rubellus.jpg[Cacing tanah by wikipedia]

* Species  : L. rubellus Hoffmeister.
* Local    : Cacing tanah.
* Synonyms : Earthworm.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./serbuk_simplisia_20241125075637.adoc[SST]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241124044122.adoc[20241124044122]
