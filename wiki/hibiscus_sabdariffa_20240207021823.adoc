= _Hibiscus sabdariffa_
:keywords: herbal, nicolaia, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/4/4d/Roselle_2%2C_Hibiscus_sabdariffa%2C_2014.JPG[Roselle by Wikipedia]

* Species  : H. sabdariffa L. Calyx<<1>><<14>>
* Local    : Rosela.
* Synonyms : Roselle.
* Part     : link:./herbal_morfologi_20231105085240.adoc[Flos].

== Organoleptis

* Bentuk: Flos<<2>>.
* Bau   : Khas.
* Warna : Merah keunguan hingga kehitaman.
* Rasa  : Asam.

== Peringatan

* Tidak direkomendasikan pada lambung sensitif (Ph seduhan rosela 3,14)<<5>><<7>><<11>>, dapat menyebabkan mual dan muntah<<14>><<17>>.
* Tidak direkomendasikan untuk anak usia `<` 12 tahun<<7>>, ibu hamil dan menyusui<<14>>.
* Hindari mengkonsumsi bersamaan AINSD atau anti-malaria<<17>>.

== Khasiat

. Flos.
  * Meredakan link:./khasiat_pereda_asamurat_20231103074233.adoc[asam urat]<<3>><<5>><<17>>.
  * Meredakan link:./khasiat_pereda_radang_20231105101234.adoc[peradangan]<<9>><<18>>.
  * Melancarkan link:./khasiat_pelancar_pembuluhdarah_20231105115154.adoc[darah]<<3>><<5>><<6>>.
  * Mengatasi link:./khasiat_pereda_darahtinggi_20231105114947.adoc[darah tinggi]<<10>><<12>><<15>>.
  * Meredakan link:./khasiat_pereda_kencingmanis_20231105115811.adoc[gula darah]<<5>><<6>><<9>>.
  * Melancarkan link:./khasiat_pelancar_kencing_20231105121243.adoc[kencing]<<3>><<5>><<6>>.
  * Peluruh link:./khasiat_peluruh_batuginjal_20231108060806.adoc[batu ginjal]<<5>><<6>>.
  * Peluruh link:./khasiat_peluruh_empedu_20231105121030.adoc[garam empedu]<<12>>.
  * Melindungi link:./khasiat_pereda_sakitkuning_20231105120326.adoc[hati]<<5>><<11>>.
  * Penawar link:./khasiat_penawar_racun_20231227014702.adoc[racun]<<5>><<11>><<12>>.
  * Menurunkan link:./khasiat_pereda_lemakdarah_20231105115624.adoc[lemak darah]<<5>><<7>><<8>>.
  * Meredakan link:./khasiat_penekan_nafsumakan_20231105114729.adoc[nafsu makan]<<6>><<10>><<14>>.
  * Membantu terapi link:./khasiat_pereda_kangker_20231113084517.adoc[tumor jinak]<<15>><<16>><<18>>.
  * Menangani link:./khasiat_menjaga_kesuburanwanita_20231105122148.adoc[haid] tidak teratur<<4>><<12>>.
  * Bahan pewarna merah<<4>><<11>>.
  * Meredakan link:./khasiat_meredakan_mualmuntah_20231105111506.adoc[mual]<<5>><<19>><<20>>.
  * Melegakan link:./khasiat_pelega_pernafasan_20231106023057.adoc[nafas]<<19>>.
  * Membasmi link:./khasiat_pereda_cacingan_20231105110851.adoc[cacingan]<<5>><<9>><<10>>.
  * Meningkatkan link:./khasiat_peningkat_dayatahan_20231105122625.adoc[daya tahan tubuh]<<5>><<6>><<11>>.
  * Meningkatkan link:./khasiat_peningkat_staminapria_20231105122002.adoc[gairah pria]<<5>><<12>>.
  * Menjaga link:./khasiat_penyegar_badan_20231105122850.adoc[kebugaran]<<12>>.
  * Memberi link:./khasiat_pelelap_tidur_20231106024122.adoc[ketenangan]<<5>><<6>>.
  * Memelihara link:./khasiat_pereda_gangguanmata_20231106023455.adoc[pengelihatan]<<5>>.
  * Melancarkan link:./khasiat_pelancar_sembelit_20231105103346.adoc[sembelit]<<5>><<9>><<12>>.
  * Meredakan link:./khasiat_perawatan_tulang_20231227021215.adoc[tulang keropos]<<9>>.
  * Membersihkan link:./khasiat_pereda_baumulut_20231105121441.adoc[gigi dan mulut]<<11>>.
. Semen.
  * Melancarkan link:./khasiat_pelancar_kencing_20231105121243.adoc[kencing]<<19>>.
. Folium.
  * Menangani link:./khasiat_pereda_korengkulit_20231106041006.adoc[koreng]<<4>><<19>>.
  * Memelihara link:./khasiat_menjaga_kesuburanwanita_20231105122148.adoc[kesuburan wanita]<<13>>.
. Radix.
  * Meredakan link:./khasiat_pereda_batuk_20231114055433.adoc[batuk]<<4>><<9>><<16>>.
  * Melembutkan link:./khasiat_perawatan_kulit_20231106062703.adoc[kulit]<<5>><<6>><<10>>.
  * Meredakan link:./khasiat_penurun_panas_20231105101010.adoc[demam]<<5>><<9>><<11>>.
  * Menekan pertumbuhan link:./khasiat_penghambat_mikroorganisme_20231106023934.adoc[mikroorganisme]<<5>><<9>><<11>>.

== Takaran pakai

Waktu terapi 4 minggu.

* 1-2 X sehari 6-10 gr kering<<5>><<7>>.
* 1-2 X sehari 100-500mg ekstrak<<7>><<6>>.
* 1 X sehari 7-24gr/dl segar<<5>><<20>>.
* Pewarna 11,5% rosela : 1,5% As. sitrat(buffer)<<11>>.

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap:

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20240207021823.adoc[20240207021823]
