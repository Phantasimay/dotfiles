= _Morus Alba_
:keywords: herbal, morus, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Morus_alba_fruits_7th_Brigade_Park_Chermside_P1070826.jpg/800px-Morus_alba_fruits_7th_Brigade_Park_Chermside_P1070826.jpg[Murbai by wikipedia]

* Species  : M. Alba L.
* Local    : Murbai.
* Synonyms : Silkworm mulberry.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241114030413.adoc[20241114030413]
