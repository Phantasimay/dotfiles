= Teknologi
:keywords: disiplin
:toc:

----
Segala hal yg mempermudah hidup manusia, teknologi.
----

== History of Technology.
-----
  ┌──────────────┐   ┌────────────┐   ┌───────────┐   ┌─────────────────┐   ┌──────────────┐   ┌─────────────────┐   ┌───────────────┐   ┌────────────────┐   ┌─────────────┐
  │ Pra-Teknology│   │Paleolitikum│   │ Neolitikum│   │  Teknologi Awal │   │  Renaisans   │   │Industrialization│   │Elektro-Mekanik│   │    Komputasi   │   │Globalization│
  │ (<40.000 SM) ├──►│(<10.000 SM)├──►│ (<4.000SM)├──►│     (<500 M)    ├──►│  (<1650 M)   ├──►│    (<1800 M)    ├──►│   (<1945 M)   ├──►│    (<2000 M)   ├──►│  (>2000 M)  │
  │──────────────│   │────────────│   │───────────│   │─────────────────│   │──────────────│   │─────────────────│   │───────────────│   │────────────────│   │─────────────│
  │. Alat batu   │   │ . Mineral  │   │. Metalurgi│   │. Roda/pengungkit│   │. Percetakan  │   │. Konversi energi│   │  . Electric   │   │. Semiconductor │   │  . Network  │
  │. Api         │   └────────────┘   │. Alkima   │   │. Agriculture    │   │. Seni        │   │. Engineering    │   │  . Magnetic   │   │. Microprocessor│   └─────────────┘
  │. Kayu/tulang │                    └───────────┘   └─────────────────┘   │. Jurnalistik │   └─────────────────┘   └───────────────┘   └────────────────┘
  └──────────────┘                                                          │. Architecture│
                                                                            └──────────────┘
-----

== Computer.
----
Smart is a must, but unsufficed.
----
=== Hardware.
=== Software.
==== Protokol(aturan).
==== Programming Language(Operational Code).
==== Tools.
. Basic Imput Output System(BIOS).
. link:./os_20230911080019.adoc[Operating System](OS).
. Development Enviroment(DE).
. Networking(Server-client).
. Security(Offensive-Deffensive).
. link:./vi2nvim_20230911044757.adoc[Text Editor].
. Intepreter-Complier.
. link:./virtualization_containerization_20241021032950.adoc[Virtualization-Containerization].
. Framework(Tamplate).
. link:./starterpack_webapp_20241204020558.adoc[Stack Kit].
. link:./vcs_20230910073111.adoc[Version Control Sysytem](VCS).
. Centralization-Decentralization System.
. Open Source Software(OSS).
. Artificial Intellegent(AI).
. Cryptograpy.

== Library
. https://learnvimscriptthehardway.stevelosh.com/
. https://www.wikipedia.org/
. http://scihi.org/
. https://www.guru99.com/
. https://handwiki.org/wiki/Start
. https://linuxreviews.org
. https://www.unixmen.com/what-you-need-to-become-a-linux-programmer/
. https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.adoc
. https://www.firewall.cx/
. https://wiki.archlinux.org/
. https://www.w3.org/
. https://www.rfc-editor.org/
. https://martinfowler.com/
. https://spec.whatwg.org/
. https://developer.mozilla.org/en-US/
. https://download.lenovo.com/bsco/#/
. https://access.redhat.com/products/
. https://devdocs.io/
. https://www.freedesktop.org/wiki/
. https://paletton.com/
. http://epsi-rns.github.io/pages/index.html
. https://www.theodinproject.com/
. https://www.freecodecamp.org/
. https://rwx.gg/ Tricky web with great basic magisterial.
. https://docs.darlinghq.org/darling-shell.html
. https://www.cyberciti.biz/
. https://opencv.org/
. https://forum.mobilism.org
. https://roadmap.sh/
. https://papersizes.io/
. https://www.exceldemy.com/
. https://earthly.dev/
. https://think4web.github.io

'''''

== link:./deck/0.adoc[QnA]

== MindMap
. link:./favorite.adoc[Favorite]

include::./reference/20250107033414.adoc[20250107033414]
