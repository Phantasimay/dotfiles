= _Santalum Album_
:keywords: herbal, Santalum, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/e/e4/Santalum_album_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-128.jpg[Cendana by wikipedia]

* Species  : S. Album L.
* Local    : Cendana.
* Synonyms : White Sandalwood.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241123101440.adoc[20241123101440]
