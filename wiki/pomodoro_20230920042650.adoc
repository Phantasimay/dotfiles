= Keep your productivity stay focus

Pomodoro are productivity method tought by Francesco Cirillo.
How it work:
.  Make TODO list.
.  Break down each big task to 45 minuts activity.
  * make it specific.
  * focus 1 pomodoro session just for 1 task.
  * recommended use pomodoro on make TODO list.
.  Turn off all distraction.
.  Turn on pomodoro timer.
  * you can use my app link:../.local/bin/focus[here] if you like.
.  _Focus_ with your task until time over.
  * if you get distraction, writh it down on note.
  * avoid checkeng remaining time.
.  When time up take break (5-15 minuts).
  * if you on flow state of focus dont take breake, just continue what you doing.
  * Forget your task in break time.
  * Good ratio for break and work are 1/3 - 1/4.
.  Repeat pomodoro cycle.
  * best recommendation to take long break after 4 pomodoro session.

Understanding then memorialization the best to learn are when we recall our knowledged from our brain.

''''''
include::./reference/20230920042650.adoc[20230920042650]
