= Basic System

* OS : Rolling release (Kali Linux or Arch)
* FS : 1:Boot 2xRam:Swap Btrfs:Storage
* WM : Sway (Wayland)
* APPSTORE : Snap, Flatpak, APT(nala), Gdebi
* SHELL : Zsh ft. OhMyZsh (opt zap)
* TERMINAL : Alacritty ft. Tmux (theme: Dracula)
* EDITOR : Vim
* FILE MANAGER : LF (List Filemanager)
* RECOVERY : TimeShift
* BROWSER : Lynx
* PLAYER : MPD ft. ncmpcpp (Audio), Mpv (Video)
* SYSOPT

'''''

== MindMap

. link:./profile.adoc[Project Resurrection]
. link:./other_app.adoc[Other app]
. link:./favorite.adoc[favorite]

link:./reference/20230910015902.adoc[20230910015902]
