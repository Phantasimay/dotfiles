= Herbal pelancar pembuluh darah:

* link:./prunus_amygdalus_20231222064055.adoc[Almond oil].
* link:./oroxylum_indicum_20240225063253.adoc[Bungli]
* link:./centella_asiatica_20231128071631.adoc[Daun Pegagan].
* link:./mentha_spp_20231227033549.adoc[Mint].
* link:./cinnammomum_burmanni_20231217140313.adoc[Kayu Manis].
* link:./cocos_nucifera_20231228040303.adoc[Kelapa].
* link:./kaempferia_galanga_20240919074058.adoc[Kencur].
* link:./curcuma_domestica_20240925041534.adoc[Kunyit].
* link:./lavandula_spp_20231229063359.adoc[Lavender].
* link:./hibiscus_sabdariffa_20240207021823.adoc[Rosela].
* link:./caesalpinia_sappan_20240118080253.adoc[Secang]
* link:./apium_graveolens_20240221080420.adoc[Seledri]
* link:./talinum_paniculatum_20231128053007.adoc[Som].
* link:./olea_europea_20231224205147.adoc[Zaitun].

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]

'''''
include::./reference/20231105115154.adoc[20231105115154]
