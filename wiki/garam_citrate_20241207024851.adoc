= Garam citrate.
:keywords: btp, herbal
:toc:

* IUPAC    : Trisodium 2-hydroxypropane-1,2,3-tricarboxylate.
* Local    : Na. Sitrat.
* Synonyms : Trisodium citrate.
* INS/CAS  : 331.

* IUPAC    : Tripotassium 2-hydroxypropane-1,2,3-tricarboxylate.
* Local    : K. Sitrat.
* Synonyms : Tripotassium citrate.
* INS/CAS  : 332.

* IUPAC    : 2-hydroxy-1,2,3-propane-tricarboxylic acid calcium salt (2:3).
* Local    : Ca. Sitrat.
* Synonyms : Tricalcium dicitrate.
* INS/CAS  : 333.

== Peringatan

* Sediaan <=20.000mg/Kg as As. Sitrat<<1>><<2>>.

== Khasiat

. link:./bahan_penstabil_20241128083643.adoc[Bahan buffering]<<1>><<2>>.

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]

'''''
include::./reference/20241207024851.adoc[20241207024851]
