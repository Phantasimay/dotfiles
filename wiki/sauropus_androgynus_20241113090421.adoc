= _Sauropus androgynus_
:keywords: herbal, sauropus, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Rau_ng%C3%B3t_2.jpg/800px-Rau_ng%C3%B3t_2.jpg[Katuk by wikipedia]

* Species  : S. androgynus (Syn. Breynia androgyna (L.) Chakrab. & N.P.Balakr.)
* Local    : Katuk.
* Synonyms : Star gooseberry.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241113090421.adoc[20241113090421]
