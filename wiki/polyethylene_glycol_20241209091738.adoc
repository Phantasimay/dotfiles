= Polyethylene glycol
:keywords: herbal, glikol, graylist
:toc:

* IUPAC    : Poly(ethylene oxide).
* Local    : PEG (200-1000).
* Synonyms : Polyethylene glycol.
* INS/CAS  : 1521.

== Peringatan

* ADI <10ppm<<2>>.
* U/ COD perlu dilakukan perhitungan konversi ke link:./diethylene_glycol_20241209091117.adoc[DEG]/link:./ethylene_glycol_20241209090207.adoc[EG]<<1>>.
* Sediaan <=70.000mg/Kg<<3>>

== Khasiat

. link:./bahan_pembawa_20231222061058.adoc[Bahan Pembawa]<<2>><<3>>.

== Takaran pakai
. <30% pelarut PEG300/400<<2>>.

== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./senyawa_beresiko_20241207130650.adoc[Graylist]

'''''
include::./reference/20241209091738.adoc[20241209091738]
