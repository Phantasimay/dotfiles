= $MULTIPLEXER

Terminal multiplexer is a program that receive instruction from one input and forwards that input to any number of outputs. term ''multiplexer'' mean it can provide lots of screens within one frame, it useful for dealing with multiple programs from command line interfance. Let’s install it :

. Open terminal.
. Run `sudo apt-get install tmux`.
. Unfortunately official tmux not support file viewer, so i use https://github.com/csdvrx/tmux.git[fork tmux]
. Clone `git clone https://github.com/csdvrx/tmux.git && cd tmux`.
. Install dependencies `sudo apt-get install libevent-dev ncurses-dev build-essential bison pkg-config`.
. Config, build and install `sh autogen.sh && ./configure --enable-debug && make -j4 && sudo make install`.
. For auto start and syncronize tmux with alacritty, add these : `$HOME/.config/alacritty/alacritty.yml`
+
[source,config]
----
shell:
   program: /usr/bin/zsh # <- set this to the path of your tmux installation
   args:
    - -l
    - -c
    - "tmux attach || tmux"
----

. You can copy my link:../.config/tmux/tmux.conf[config] for more gimmick.

Note : don’t forget to install https://github.com/christoomey/vim-tmux-navigator[vim-tmux-navigator] plugin for seamlessly navigator between vim/neovim and tmux.

== MindMap

. link:./pde_pns.adoc[Personal Developement Environment]
. link:./shell_20231028172644.adoc[Shell]
. link:./terminal.adoc[Terminal]
. link:./favorite.adoc[Favorites]
. link:./editor_20230910104050.adoc[Editor]
. link:./file_manager_20230910104422.adoc[File manager]
. link:./package_manager.adoc[Package Manager]

link:./reference/20230910103530.adoc[20230910103530]
