= PACKAGE_MANAGER

Package manager is program to dealing with managing app or dependencies of our project or system. In linux enviroment we install app from many path, there are :

. Repository : are package or source code location on the cloud.
. Local      : are package or source code on our machine.
. App Store  : Official gui repository from our distro.

In linux we have essential package manager and project specific package manager, below are few list of package manager :

[source,ascii]
+++++
|---------------------|-----------|-----------|------------|
|        Distro       | Extension | Low Level | High Level |
|:===================:|:=========:|:=========:|:==========:|
|                          Essential                       |
|---------------------|-----------|-----------|------------|
|   Debian Derivates  |    .deb   |    dpkg   |     apt    |
|                     |           |           |    gdebi   |
|                     |           |           |    nala    |
|---------------------|-----------|-----------|------------|
|  Red Hat Derivates  |    .rpm   |    rpm    |     dnf    |
|                     |           |           |     yum    |
|---------------------|-----------|-----------|------------|
|    Arch Derivates   |    .tar   |   pacman  |     yay    |
|---------------------|-----------|-----------|------------|
| Slackware Derivates |    .tbz   |  pkgtools |  slackpkg  |
|                     |    .tlz   |           |   swaret   |
|                     |    .tgz   |           |  slapt-get |
|                     |    .txz   |           |   sbopkg   |
|                     |           |           |    slpkg   |
|---------------------|-----------|-----------|------------|
|    Suse Derivates   |    .rpm   |    ZYpp   |   zypper   |
|                     |    .deb   |           |            |
|---------------------|-----------|-----------|------------|
|    BSD Derivates    |    .txz   |    pkg    |  portsnap  |
|---------------------|-----------|-----------|------------|
|                         Project Base                     |
|---------------------|------------------------------------|
|        Python       |                pip                 |
|---------------------|------------------------------------|
|         Java        |              maven                 |
|                     |              gradle                |
|---------------------|------------------------------------|
|      JavaScript     |                npm                 |
|---------------------|------------------------------------|
|       Web dev       |                bower               |
|---------------------|------------------------------------|
|         .NET        |               nuget                |
|---------------------|------------------------------------|
|         Rust        |                cargo               |
|---------------------|------------------------------------|
|          Go         |                 go                 |
|---------------------|------------------------------------|
|         Ruby        |                gem                 |
|---------------------|------------------------------------|
|         Perl        |               cpan                 |
|---------------------|------------------------------------|
|                          Universal                       |
|---------------------|------------------------------------|
|     Source Code     |              make                  |
|                     |              cmake                 |
|                     |              meson                 |
|                     |              ninja                 |
|                     |             makepkg                |
|                     |             makedeb                |
|                     |             makerpm                |
|                     |             rpmbuild               |
|                     |             ebuild                 |
|                     |          build-essential           |
|---------------------|------------------------------------|
|      By Ubuntu      |                snap                |
|---------------------|------------------------------------|
|   By Flatpak Team   |               flatpak              |
|---------------------|------------------------------------|
|    By Simon Peter   |             appimage               |
|---------------------|------------------------------------|
|        By Nix       |                nix                 |
|---------------------|------------------------------------|
|   By mac community  |            homebrew                |
|---------------------|------------------------------------|
+++++

In my case i use dpkg, nala, build-essential, pip, npm, bower. Because i want to be web developer for now.

== MindMap

. link:./pde_pns.adoc[Personal Developement Environment]
. link:./shell_20231028172644.adoc[Shell]
. link:./terminal.adoc[Terminal]
. link:./multiplexer_20230910103530.adoc[Multiplexer]
. link:./favorite.adoc[Favorites]
. link:./editor_20230910104050.adoc[Editor]
. link:./file_manager_20230910104422.adoc[File manager]

link:./reference/20230910104806.adoc[20230910104806]
