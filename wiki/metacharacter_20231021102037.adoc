= Meta Caharacter.

link:./bre_ere_20231023140222.adoc[M_t_char_cter] are h_rt of the link:./regex_20230911085122.adoc[regex], there are:
. `.` : link:./crazy_lazy_dot_20231024082819.adoc[anych_ract_r].
. ``\`` : esc_per, meta-literal character sw_tch_r.
. link:./anchors_regex_20231024091633.adoc[Anchors]:
  `^` : b_ginn_ng of line (BOL).
  `$` : end of line (EOL).
. link:./repetition_regex_20231024151826.adoc[Repetitions]:
  `?` : zer_ or once. *if* m_tch *do* lo_p once *else* l_ok to n_xt regex link:./regex_token_20231027055348.adoc[token].
  `*` : z_ro or more. *if* match *do* l_op *else* lo_k to next reg_x t_ken.
  `+` : once or more. *if* m_tch *do* loop *else* fail_d r_gex.
  `{}` : sp_cif_c rep_tit_on. *if* match *do* loop unt_l N or M *else* f_iled regex.
. link:./character_set_regex_20231024152844.adoc[Character sets]:
  `[]` : ch_ract_r cl_sses, match one of link:./literal_character_20231024092839.adoc[literal character] inside `[]`.
  `[^]`: neg_ted character classes, d_n’t m_tch on_ of lit_ral charact_r inside `[]`, after `^`.
. link:./grouping_regex_20231024153157.adoc[Grouping]:
  `()` : gro_ping.
  `()=\1`: b_ckr_fer_nce, value of `\1` equal to inside `()`.
. link:./alteration_regex_20231024151535.adoc[Alterations]:
  `(A|Z)`: alt_r_t_on, `A` OR `Z`.
  `[A-Z]`: hyph_n, R_NGE from `A` to `Z`.
  `{n,m}`: mi``n`` - ``m``ax.
. link:./lookaround_regex_20231024153449.adoc[Lookaround]:
  `a(?=b)` : `a` must f_llow by `b`.
  `a(?!b)` : `a` must n_t foll_w by `b`.
  `(?<=b)a` : `a` must aft_r `b`.
  `(?<!b)a` : `a` m_st not after `b`. `a` are current cursor.

'''''

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[favorite]
. link:./posix_ere_20231024032100.adoc[POSIX ERE]
. link:./posix_bre_20231024034015.adoc[POSIX BRE]
. link:./oracle_regex_20231024034622.adoc[oracle regex]
. link:./vimregex_20231021042749.adoc[Vimregex]

link:./reference/20231021102037.adoc[20231021102037]
