= Herbal untuk meredakan batuk

* link:./prunus_amygdalus_20231222064055.adoc[Almond oil].
* link:./tamarandus_indica_20240923023148.adoc[Asam Jawa].
* link:./oroxylum_indicum_20240225063253.adoc[Bungli]
* link:./pandanus_amaryllifolius_20240919014108.adoc[Daun Pandan].
* link:./centella_asiatica_20231128071631.adoc[Daun Pegagan].
* link:./plantago_major_20231114052046.adoc[Daun sendok]
* link:./piper_betle_20241123102524.adoc[Daun Sirih].
* link:./zingiber_officinale_20231127075544.adoc[Jahe].
* link:./cinnammomum_burmanni_20231217140313.adoc[Kayu Manis].
* link:./melaleuca_spp_20231218024536.adoc[Kayu putih]
* link:./kaempferia_galanga_20240919074058.adoc[Kencur].
* link:./curcuma_domestica_20240925041534.adoc[Kunyit].
* link:./phyllanthus_niruri_20240206060304.adoc[Meniran].
* link:./mentha_spp_20231227033549.adoc[Mint].
* link:./oryza_sativa_20240824020115.adoc[Padi].
* link:./hibiscus_sabdariffa_20240207021823.adoc[Rosela].
* link:./rosmarinus_officinalis_20231219053626.adoc[Rosmarin]
* link:./caesalpinia_sappan_20240118080253.adoc[Secang]
* link:./apium_graveolens_20240221080420.adoc[Seledri]
* link:./talinum_paniculatum_20231128053007.adoc[Som].
* link:./tribulus_terrestris_20231128022155.adoc[Tribulus].

== link:$WIKI_DIR/deck/0.adoc[QnA]

Herbal untuk pereda batuk apa saja?::

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[herbal]

'''''
link:./reference/20231114055433.adoc[20231114055433]
