= Butylparaben
:keywords: btp, herbal, regulasi
:toc:

* Species  : Butyl 4-hydroxybenzoate
* Local    : Butilparaben
* Synonyms : C~4~H~9~O~2~CC~6~H~4~OH
* INS/CAS  : 94-26-8

== Peringatan

* Sediaan topikal <=4000mg/Kg.

== Khasiat

. link:./bahan_pengawet_20241017204243.adoc[Pengawet]<<1>>

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]

'''''
include::./reference/20241129062253.adoc[20241129062253]
