= Bare Git

On linux community we like to backup and share our configuration. Usually we use VCS (Version Control System) to backup our project/config then we use remote server to store our config on the cloud. _Bare Git_ or A _bare repository_ is the same as default git, but no commits can be made in a bare repository. The changes made in projects cannot be tracked by a bare repository as it doesn’t have a working tree. But we will hack it to be our VCS.

. Make sure no other files in $HOME with run `sudo rm -rf . && ls -la` (these will delete all our file on current working directory.
. Now you can clone and initialize bare-git with run:
    * `/bin/bash -c "$(curl -fsSL https://gitlab.com/Phantasimay/dotfiles/-/raw/master/.local/bin/baregit)"` or
    * `bash -ci "$(wget -qO - https://gitlab.com/Phantasimay/dotfiles/-/raw/master/.local/bin/baregit)"`
. Change all file on $HOME/.local/bin/ to be executable. Run `sudo chmod a+x ~/.local/bin/*`
. Reboot

== MindMap
. link:./technology_20250107033414.adoc[Teknologi]
. link:./favorite.adoc[Favorite]

link:./reference/20230910073111.adoc[20230910073111]
