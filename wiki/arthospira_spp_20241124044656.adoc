= _Arthospira spp_
:keywords: herbal, arthospira, lowrisk, sst, greylist
:toc:

image::https://microbewiki.kenyon.edu/images/b/b5/SpirulinaFilament.jpg[Lumut hijau by microbewiki]

* Species  : A. spp.
* Local    : Lumut hijau.
* Synonyms : Blue green algae.
* Part     : Spirulina

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

* Kandungan toksin cyanobacterial Microcystin-LR(MC-LR) <=0,02mcg/Kg/Hari<<2>>.

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./serbuk_simplisia_20241125075637.adoc[SST]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]
. link:./senyawa_beresiko_20241207130650.adoc[Greylist]

'''''
include::./reference/20241124044656.adoc[20241124044656]
