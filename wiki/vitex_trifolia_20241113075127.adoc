= _Vitex trifolia_
:keywords: herbal, vitex, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Vitex_trifolia.jpg/800px-Vitex_trifolia.jpg[Legundi by wikipedia]

* Species  : V. trifolia L.
* Local    : Legundi.
* Synonyms : Simpleleaf chastetree.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241113075127.adoc[20241113075127]
