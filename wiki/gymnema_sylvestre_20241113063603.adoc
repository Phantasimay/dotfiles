= _Gymnema Sylvestre_
:keywords: herbal, gymnema, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Gymnema_sylvestre_DSC_6920.JPG/1024px-Gymnema_sylvestre_DSC_6920.JPG[Gurmar by wikipedia]

* Species  : G. sylvestre R.Br.
* Local    : Gurmar.
* Synonyms : Cowplant.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241113063603.adoc[20241113063603]
