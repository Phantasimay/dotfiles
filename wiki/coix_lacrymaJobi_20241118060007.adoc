= _Coix lacryma-Jobi_
:keywords: herbal, coix, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/f/fc/Leiden_University_Library_-_Seikei_Zusetsu_vol._20%2C_page_011_-_%E8%96%8F%E8%8B%A1_-_Coix_lacryma-jobi_L.%2C_1804.jpg[Jali by wikipedia]

* Species  : C. lacryma-Jobi L.
* Local    : Jali.
* Synonyms : Adlay.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241118060007.adoc[20241118060007]
