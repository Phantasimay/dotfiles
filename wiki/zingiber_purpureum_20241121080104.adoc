= _Zingiber Purpureum_
:keywords: herbal, Zingiber, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Banglai_Inflorescence_et_fleur_3884.jpg/800px-Banglai_Inflorescence_et_fleur_3884.jpg[Bangle by wikipedia]

* Species  : Z. Purpureum Roscoe(Syn. Z. casumunar).
* Local    : Bangle.
* Synonyms : Indonesian ginger.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241121080104.adoc[20241121080104]
