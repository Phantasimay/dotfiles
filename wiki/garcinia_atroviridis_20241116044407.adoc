= _Garcinia atroviridis_
:keywords: herbal, garcinia, lowrisk
:toc:

image::https://static.inaturalist.org/photos/115884498/large.jpg[Asam gelugur by inaturalist]

* Species  : G. atroviridis Griff. ex T.Anderson.
* Local    : Asam gelugur.
* Synonyms : Somkhack.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241116044407.adoc[20241116044407]
