= Herbal pereda gangguan mata:

* link:./tamarandus_indica_20240923023148.adoc[Asam Jawa].
* link:./oroxylum_indicum_20240225063253.adoc[Bungli]
* link:./moringa_oleifera_20240910152315.adoc[Daun Kelor].
* link:./hibiscus_sabdariffa_20240207021823.adoc[Rosela].
* link:./apium_graveolens_20240221080420.adoc[Seledri]
* link:./tribulus_terrestris_20231128022155.adoc[Tribulus].

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]

'''''
include::./reference/20231106023455.adoc[20231106023455]
