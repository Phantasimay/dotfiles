= Firefox tips:

https://wiki.mozilla.org/Project_Fission[Fission project] from mozilla are suck alot ram. Let’s disable it from `about:config` firefox.
Optimize config:

[source,config]
----

 dom.webgpu.enabled                         true
 layers.gpu-process.enabled                 true
 layers.mlgpu.enabled                       true
 media.gpu-process-decoder                  true
 media.ffmpeg.vaapi.enabled                 true
 media.navigator.mediadatacoder_vpx_enabled true
 media.ffvpx.enabled                        false
 media.rdd-vpx.enabled                      false
 gfx.webrender.all                          false
 fission.autostart                          false
----

== MindMap

. link:./favorite.adoc[Favorites]

link:./reference/20230911075738.adoc[20230911075738]
