= Visual Basic Script Regular Expressions.

VBScript link:./regex_flavor_20231025032131.adoc[flavour] are close to link:./javascript_regex_20231024035300.adoc[JavaScript regex]. VBScript link:./regex_20230911085122.adoc[regex] are bare bond because it use just to validated like on link:./xml_regex_20231024032337.adoc[xml flavour].

== Feature Suppport:

. All character are link:./literal_character_20231024092839.adoc[Literal], except link:./bre_ere_20231023140222.adoc[ERE].
. Character, Control character, NULL, Octal, and Hexadecimal escape.
. Eager link:./alteration_regex_20231024151535.adoc[alteration].
. All character link:./character_set_regex_20231024152844.adoc[inside] `[]` are literal, except `]^\-`.
. link:./shorthand_character_20231022150233.adoc[Shorthand] ASCII support.
. String and line link:./anchors_regex_20231024091633.adoc[anchor].
. ASCII link:./anchors_regex_20231024091633.adoc[word boundary].
. Greedy `*` (star), lazy `?` and fix `{N}` link:./repetition_regex_20231024151826.adoc[quantifier].
. link:./literal_character_20231024092839.adoc[Unicode] code point.
. Non-capturing link:./grouping_regex_20231024153157.adoc[grouping].
. Nested link:./advance_regex_20231029103352.adoc[backreference].
. Support link:./lookaround_regex_20231024153449.adoc[lookahead].
. Mode link:./mode_modifier_regex_20231030014212.adoc[modifier] support.

== Regex Syntax:

'''''

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[favorite]

link:./reference/20231024042310.adoc[20231024042310]
