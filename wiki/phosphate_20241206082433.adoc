= Phosphate
:keywords: btp, herbal
:toc:

* Species  : Phosphate.
* Local    : Fosfat.
* Synonyms : Orthophosphate.
* INS/CAS  : 339.

== Peringatan

* Sediaan tablet dan kapsul <=220.000mg/Kg as fosforus<<1>><<2>>.
* Sediaan lainya <=2200mg/Kg as fosforus<<1>><<2>>.

== Khasiat

. link:./bahan_pembawa_20231222061058.adoc[Bahan Pembawa]<<1>><<2>>

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]

'''''
include::./reference/20241206082433.adoc[20241206082433]
