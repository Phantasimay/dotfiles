= _Cananga Odorata_
:keywords: herbal, cananga, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Cananga_odorata_Blanco1.221-original.png/800px-Cananga_odorata_Blanco1.221-original.png[Kenanga by wikipedia]

* Species  : C. Odorata (Lam.) Hook.f. & Thomson.
* Local    : Kenanga.
* Synonyms : Cananga.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241123114328.adoc[20241123114328]
