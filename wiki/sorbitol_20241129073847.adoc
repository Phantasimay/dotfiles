= Sorbitol
:keywords: btp, herbal, glikol, graylist
:toc:

* IUPAC    : D-Glucitol.
* Local    : Sorbitol.
* Synonyms : Sorbogem.
* Part     :

== Peringatan

* U/ COD perlu dilakukan perhitungan konversi ke link:./diethylene_glycol_20241209091117.adoc[DEG]/link:./ethylene_glycol_20241209090207.adoc[EG]<<1>>.

== Khasiat

. link:./bahan_perisa_20241013053807.adoc[Bahan Pemanis]<<1>>
. link:./bahan_pembawa_20231222061058.adoc[Bahan pembawa]<<2>>
. link:./bahan_penstabil_20241128083643.adoc[Bahan penstabil]<<2>>

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./senyawa_beresiko_20241207130650.adoc[Graylist]

'''''
include::./reference/20241129073847.adoc[20241129073847]
