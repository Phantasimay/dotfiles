= Sejarah berdirinya yahudi
:keywords: yahudi, israel, teologi
:toc:

* Yahudi => Yahuda (anak ke IV N.Yakub AS.)
* Istri dan Anak N. Yakub:
  . Rahel
    ** Yusuf (Nabi dan raja mesir).
    ** Benyamin.
  . Lea
    ** Ruben.
    ** Simeon.
    ** Lewi.
    ** Yahuda (Bapak kaum yahudi).
    ** Isakhar.
    ** Zebulon.
    ** Dina (Putri).
  . Bilha
    ** Dan.
    ** Naftali.
  . Zilpa
    ** Dad.
    ** Asyer.
* Nama anak2 N.yakub dijadikan nama suku2 yahudi.

== Sejarah Singkat Bani israel.
. Bani israel merupakan budak pd masa firaun mesir.
. Melakukan perjalanan ke tanah kanaan didampingi o/ N.Musa dan N.Harun.
. Sebelum sampai N.Musa dan N.Harun meninggal lalu diteruskan o/ N.Yoshua Bin Nun.
. Sesampainya di kanaan bani israel dipimpin o/ 12 hakim.
. Pd saat bani israel dipimpin N.Samuel mereka meminta u/ berubah mjd kerajaan.
. Raja pertama a/ Raja saul => Daud => Solmon.
. Sepeninggalan N. Solomon israel pecak mjd 2 (utara dan selatan) dan lahirlah agama Yudaisme..
. Israel selatan => kerajaan yudea => Suku Yudea dan benjamin.
. Israel utara => Ruben, Simeon, Lewi, Isakhar, Zebulon, Naftali, Gad, Asyer, Yusuf, Dan.

== 7 Gelar tuhan
* YHWH (Tetragrammaton)
* Eloah
* Elohim
* Adonai
* Ehyeh-Asher-Ehyeh
* El Shaddai
* Tzevaot


'''''

== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./theology_20241211060359.adoc[Theology]

include::./reference/20241212021504.adoc[20241212021504]
