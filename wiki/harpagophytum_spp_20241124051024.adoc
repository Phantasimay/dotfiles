= _Harpagophytum_
:keywords: herbal, harpagophytum, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Harpagophytum_procumbens00.jpg/800px-Harpagophytum_procumbens00.jpg[Devil's claw by wikipedia]

* Species  : H. spp DC. ex Meisn.
* Local    : devil's claw.
* Synonyms : Grapple plant.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241124051024.adoc[20241124051024]
