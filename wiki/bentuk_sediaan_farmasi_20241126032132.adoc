= Bentuk Sediaan Farmasi
:keywords: sediaan, regulasi
:toc:

== List Sediaan

|------------+----------------+--------------------+------------------+----------|
| Penggunaan | Konsistensi    | Sediaan            | Sub-sediaan      | Regulasi |
|:==========:+:===============+:===================+:=================+:=========|
| Obat Dalam | Padat          | Rajangan           |                  | OBA      |
|            |                | Serbuk             |                  | OBA,SK   |
|            |                |                    | Serbuk Instan    | OBA,SK   |
|            |                |                    | Serbuk Efervesen | OBA,SK   |
|            |                |                    | Granul           | OBA      |
|            |                | Tablet/Kaplet      |                  | OBA,SK   |
|            |                |                    | Tablet Efervesen | OBA,SK   |
|            |                |                    | Tablet Hisap     | OBA,SK   |
|            |                |                    | Tablet Salut     | OBA,SK   |
|            |                |                    | Tablet Kunyah    | OBA,SK   |
|            |                | Pil                |                  | OBA      |
|            |                | Pastiles           |                  | OBA      |
|            |                | Film strip         |                  | OBA,SK   |
|            |                | Kapsul             | Kapsul  Keras    | OBA,SK   |
|            | -------------  | ------------------ | ---------------- | -------- |
|            | Semi Padat     |                    | Kapsul Lunak     | OBA,SK   |
|            |                | Gummy chewable     |                  | OBA,SK   |
|            |                | Oral gel           |                  | OBA,SK   |
|            |                | Dodol/Jenang       |                  | OBA      |
|            | -------------  | ------------------ | ---------------- | -------- |
|            | Cair           | Cairan Obat Dalam  | Larutan          | OBA,SK   |
|            |                |                    | Spray            | SK       |
|            |                |                    | Emulsi           | OBA,SK   |
|            |                |                    | Syrup            | OBA,SK   |
|            |                |                    | Suspensi         | OBA,SK   |
|------------+----------------+--------------------+------------------+----------|
|  Obat Luar | Cair           | Cairan Obat Luar   |                  | OBA      |
|            |                | Losio              |                  | OBA      |
|            |                | Parm cair          |                  | OBA      |
|            |                | Aerosol            |                  | OBA      |
|            | -------------  | ------------------ | ---------------- | -------- |
|            | Semi Padat     | Salep/Balsam       |                  | OBA      |
|            |                | Krim               |                  | OBA      |
|            |                | Gel                |                  | OBA      |
|            | -------------- | ------------------ | ---------------- | -------- |
|            | Padat          | Parem Padat        |                  | OBA      |
|            |                | Serbuk Obat Luar   |                  | OBA      |
|            |                | Pilis              |                  | OBA      |
|            |                | Tapel              |                  | OBA      |
|            |                | Plaster            |                  | OBA      |
|            |                | Suppositoria       |                  | OBA      |
|            |                | Rajangan Obat Luar |                  | OBA      |
|------------+----------------+--------------------+------------------+----------|

. U/ _Suplemen kesehatan_ hanya memiliki sediaan *oral*.
. U/ _Obat Kuasi_ hanya memiliki sediaan *Topikal*.

'''''

== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./keamanan_mutu_OBA_20241125062048.adoc[Keamanan Mutu OBA]
. link:./keamanan_mutu_SK_20241219044147.adoc[Keamanan Mutu SK]
. link:./parameter_uji_produk_20241126041619.adoc[Parameter Uji]
. link:./senyawa_beresiko_20241207130650.adoc[Greylist]

include::./reference/20241126032132.adoc[20241126032132]
