= _Acalypha wilkesiana._

image::https://s3.amazonaws.com/eit-planttoolbox-prod/media/images/Acalypha_wilkesiana_red_Forest_and_Kim_Starr_CCBY20.jpg[Acalypha wilkesiana Muell. Arg. by ncsu.edu]

. Species : A wilkesiana Muell. Arg<<1>>.
. Local   : Ekor tupai.
. Synonyms: Joseph’s coat.
. Part    : Herba.

== Organoleptis

* Bentuk: link:./herbal_morfologi_20231105085240.adoc[Herba].<<1>>
* Bau   : Khas tanaman hias.
* Warna : Merah muda kehijauan hingga merah tua.
* Rasa  : Kelat agak pahit.

== Peringatan

== Khasiat

. Herba:
  * Menghambat link:./khasiat_penghambat_mikroorganisme_20231106023934.adoc[bakteri]<<1>>.
  * link:./khasiat_pereda_cacingan_20231105110851.adoc[Cacingan]<<1>>.
  * link:./khasiat_pereda_diare_20231105102437.adoc[Diare/Disentri].<<1>>
  * link:./khasiat_pereda_kencingmanis_20231105115811.adoc[Kencing manis]<<3>>.
  * Penurun link:./khasiat_pereda_lemakdarah_20231105115624.adoc[lemak darah]<<3>>.
  * Penghambat link:./khasiat_penghambat_jamur_20231109084744.adoc[jamur]<<1>><<3>>.
  * Penurun link:./khasiat_penurun_panas_20231105101010.adoc[panas]<<1>>.
  * link:./khasiat_pegal_linu_20231105100517.adoc[Pegal linu]<<2>>.
  * link:./khasiat_pereda_radang_20231105101234.adoc[Peradangan]<<1>>.
  * link:./khasiat_perawatan_kulit_20231106062703.adoc[Pigmentasi kulit]<<3>>.

== Takaran pakai

Remasan herba segar sebanyak 10 gr<<3>> direbus dalam air untuk diminum<<1>>.

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap:

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./acalypha_spp_20231103080924.adoc[Acalypha spp.]

''''''
include::./reference/20231109081818.adoc[20231109081818]
