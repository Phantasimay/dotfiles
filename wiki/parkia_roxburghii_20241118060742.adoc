= _Parkia Roxburghii_
:keywords: herbal, parkia, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Parkia_timoriana.jpg/1024px-Parkia_timoriana.jpg[Kedawung by wikipedia]

* Species  : P. Roxburghii G.Don(Syn. P. timoriana)
* Local    : Kedawung.
* Synonyms : Parkia.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241118060742.adoc[20241118060742]
