= Microcode Intel/Amd (CPU Driver)

For our machine to work flawlessly you maybe need to install microcode (firmware non free).
. Open Terminal.
. Run `Sudo apt-get install intel-microcode amd64-microcode`.
. Or if you want to use custom open firmware BIOS you can use https://www.coreboot.org/[Coreboot] and https://libreboot.org/[Libreboot].
+
CAUTION: https://en.wikipedia.org/wiki/RTFM[RTFM] !!.

. Reboot.

== MindMap

. link:./essential_driver.adoc[Essential driver]
. link:./favorite.adoc[Favorite]
. link:./wifi_driver.adoc[Wifi driver]
. link:./gpu_driver.adoc[GPU drivere]
. link:./audio_driver.adoc[Audio driver]
. link:./printer_driver.adoc[Printer driver]
. link:./mounting_device.adoc[Mounting devices]
. link:./authenticator.adoc[Authentications]

link:./reference/20230911021653.adoc[20230911021653]
