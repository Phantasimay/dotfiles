= _Mesua ferrea_
:keywords: herbal, mesua, lowrisk
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Mesua_ferrea_-_Young_leaves_and_flowers..jpg/800px-Mesua_ferrea_-_Young_leaves_and_flowers..jpg[Nagasari by wikipedia]

* Species  : M. ferrea L.
* Local    : Nagasari.
* Synonyms : Cobra saffron.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

*

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./lowrisk_oba_20241109014554.adoc[LowRisk]

'''''
include::./reference/20241114072124.adoc[20241114072124]
