= Potassium Benzoate
:keywords: herbal, btp
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/6/68/Potassium_benzoate.jpg[K. Benzoat by wikipedia]

* Species  : Potassium benzoate.
* Local    : K. Benzoat.
* Synonyms : C~6~H~5~COOK
* INS/CAS  : 212

== Peringatan

* Batas Max. 2000mg/Kg. as link:./benzoic_acid_20241129022227.adoc[benzoic acid]<<1>><<2>>

== Khasiat

. link:./bahan_pengawet_20241017204243.adoc[Pengawet]<<1>><<2>>.

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]

'''''
include::./reference/20241129024218.adoc[20241129024218]
