= Benzylpiperazine.
:keywords: blacklist, herbal, regulasi
:toc:

* IUPAC    : 1-Benzylpiperazine.
* Local    : Benzil piperazin.
* Synonyms : BZP.
* INS/CAS  : 2759-28-6

== Peringatan

* Dilarang diggunakan, memiliki efek mirip amfetamin (Amphetamine like effect)<<1>><<2>>

== Khasiat

. Sedatif aditif.
. Kecacingan.

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./cayenne_pepper_20241207150458.adoc[Cabe tanduk sapi]
. link:./bahan_kimia_obat_20241207042314.adoc[BKO]

'''''
include::./reference/20241207145313.adoc[20241207145313]
