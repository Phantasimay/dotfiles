= _Angelica sinenensis_
:keywords: herbal, angelica
:toc:

image::[]

* Species  :
* Local    : Gingseng betina.
* Synonyms :
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

* Tidak direkomendasikan untuk pasien yg sedang mengkonsumsi pengencer darah atau akan/setelah menjalani operasi<<1>>.
* Tidak direkomendasikan u/ wanita hamil<<1>>.

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./penandaan_OBAOKSK_20240928094611.adoc[Penandaan]

'''''
include::./reference/20241010043218.adoc[20241010043218]
