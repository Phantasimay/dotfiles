= Penandaan OBA OK SK
:keywords: regulasi, herbal, supleman
:toc:

== Aktif
* PerBPOM No.10 Th 2024 Tentang Penandaan Obat Bahan Alam, Obat Kuasi, Dan Suplemen Kesehatan.

== Pasal 1 Ketentuan Umum.
include::./ketum_OBA_20240724031633.adoc[]

== Pasal 2 Cakupan.
. Produk lokal dan impor wajib penandaan izin ke BPOM.
. Golongan/Kelompok OBA meliputi:
  * Jamu.
  * OHT.
  * Fitofarmaka.

== Pasal 3 Objek.
Tempat penandaan:
. Kemasan Primer.
. Kemasan Sekunder.
. Brosur/laflet.

== Pasal 4-10 Kriteria.
. Kriteria:
  * Objektif.
  * Lengkap.
  * Tdk Menyesatkan.

. Detail informasi:
  * Nama produk<<1>>.
  * NIE<<1>>.
  * No.Bets<<1>>.
  * ED<<1>>.
  * Nama dan Alamat PU/industri<<2>>.
  * Isi bersih<<3>>.
  * Barcode<<3>>.
  * Logo golongan/kelompok OBA<<3>>.
  * Bentuk Sediaan<<3>>.
  * Nama dan Alamat distributor/pemegang lisensi<<3>>.
  * Komposisi dan bahan tambahan<<3>>.
  * Khasiat<<3>>.
  * Cara penggunaan<<3>>.
  * Peringatan<<3>>.
  * Penyimpanan<<3>>.
  * Informasi lain<<3>>:
    .. Label halal.
    .. Kadar alkohol.
    .. Info asal bahan tertentu.
    .. Persen AKG/hari.

. Minimal penandaan.
  [[1]] Wajib ada.
  [[2]] Luas label > 10 cm^2^ wajib tercantum.
  [[3]] Info lain wajib ada pd brosur/laflet bila luas lebel terbatas.

. Penandaan harus awet, mudah dilihat dan dibaca.
. Usahakan menempel pd kemasan primer.
. Hindari menggunakan istilah asing.
. Latar belakang tdk mengkaburkan tulisan.


== Pasal 11-13 Nama Produk dan Bentuk Sediaan.
. Nama produk berupa:
  * Nama Umum.
  * Nama Dagang.
. Sifat Nama Produk:
  * Jelas dan mencerminkan satu kesatuan.
  * Tidak SARA(Suku, Agama. Ras, Antar golongan).
  * Nama dagang belum eksisting (secara utuh) pd:
    .. Golongan/kelompok OBAOKSK yg berbeda.
    .. Sediaan yg sama.
  * Tidak menggunakan istilah medis/klinis.
  * Tidak menggunakan nama produk bermasalah.
. Nama dagang dpt berupa nama payung dari:
  * Produk khasiat sejenis (perubahan khasiat/dosis).
    .. Komposisi inti sama.
    .. Penambahan nama umum(sebagian/seluruhnya).
    .. Terbukti bermanfaat.
  * Merk dagang/nama perusahaan.
. Bentuk sediaan harus telah eksisting.

== Pasal 14-17 Nama dan Alamat Produsen/Distributor.
. Alamat minimal: nama kota dan negara.
. Keterangan status:
  * diproduksi oleh:...
  * diproduksi oleh:... untuk...
  * diproduksi oleh:... di bawah lisensi...
  * diimpor oleh:...
  * Atau kalimat yg semakna.

== Pasal 18 Isi/Berat bersih.
. Mencantumkan keterangan:
  * "Isi bersih" u/ cairan.
  * "Berat bersih" u/ padat.
  * "Isi/Berat bersih" u/ semi padat.
  * Dikecualikan u/ sediaan non konvensional(plester, film, strip dkk) cukup *cantumkan jumlah*.

== Pasal 19-23 Komposisi.
. Bahan aktif dicantumkan scr kualitatif(apa?) dan kuantitatif(berapa?).
. Bahan tambahan dicantumkan scr kualitatif(apa?).
. Usahakan menggunakan nama latin.
. Produk probiotik wajib mencantumkan marga/jenis/strain.
. Kalimat pembuka:
  * "Komposisi:"
  * "Bahan yang digunakan:"
  * "Bahan-bahan:"
. Bahan tambahan dapat bertupa:
  * Pemanis.
  * Pengawet.
  * Pewarna.
  * Perisa.

== Pasal 24 Klaim Khasiat.
* Sesuai link:./klaim_khasiat_OBA_20241014061041.adoc[peraturan BPOM].

== Pasal 25 Aturan Pakai.
. Cara penyiapan.
. Cara penggunaan.
. Disertai gambar.

== Pasal 26 List Perhatian.
. Kandungan komposisi:
  * Asam Alfa-lipoat.
  * Zat Besi(> 15 mg/saji).
  * Kromium.
  * Gula bebas(> 6g/100ml).
  * Chitosan.
  * Citicholine.
  * Co-Enzyme Q10.
  * Methyl sulfonyl methane(MSM).
  * Asam Linoleat.
  * Octacosanol.
  * Policosanol.
  * Keratin.
  * Rutin.
  * Kafein.
  * Vitamin D.
  * Vitamin K.
  * Vitamin E.
  * Marine(Alergi).
  * link:./gadus_20241011082200.adoc[Minyak ikan].
  * link:./allium_sativum_20241010035126.adoc[Bawang putih].
  * link:./aloe_vera_20241010040557.adoc[Lidah buaya].
  * link:./andrographis_paniculata_20241010042350.adoc[Sambiloto].
  * link:./angelica_sinenensis_20241010043218.adoc[Gingseng betina].
  * link:./azadirachta_indica_20241010044351.adoc[Mimba].
  * link:./cinnamomum_camphora_20241010055010.adoc[Kamper].
  * link:./cassia_senna_20241010062704.adoc[Senna].
  * link:./centella_asiatica_20231128071631.adoc[Pegagan].
  * link:./echinacea_20241010065900.adoc[Bunga Topi Surya].
  * link:./oenothera_biennis_20241010071639.adoc[Sembulau malam].
  * link:./ganoderma_lucidum_20241010072841.adoc[Jamur lingzhi].
  * link:./gingko_biloba_20241010073627.adoc[Gingko].
  * link:./glychirhiza_glabra_20241010074738.adoc[Akar manis].
  * link:./panax_spp_20241010082004.adoc[Gingseng].
  * link:./apis_spp_20241010083055.adoc[Produk lebah].
  * link:./lactum_20241011085518.adoc[Turunan Susu].
  * link:./monascus_spp_20241011071159.adoc[Angkak].
  * link:./plantago_ovata_20241011073059[Sekam psilium].
  * link:./bahan_perisa_20241013053807.adoc[Bahan pemanis].

. Klaim khasiat:
  * link:./khasiat_pelancar_kencing_20231105121243.adoc[Pelancar kencing].
  * link:./khasiat_penurun_panas_20231105101010.adoc[Penurun demam].
  * link:./khasiat_pereda_diare_20231105102437.adoc[Pereda diare].
  * link:./khasiat_peluruh_batuginjal_20231108060806.adoc[Peluruh batu ginjal].
  * link:./khasiat_pereda_kencingmanis_20231105115811.adoc[Pereda kencing manis].
  * link:./khasiat_pereda_kangker_20231113084517.adoc[Support terapi kangker].
  * link:./khasiat_menjaga_kesuburanwanita_20231105122148.adoc[Menjaga kesuburan wanita].
  * link:./khasiat_penakan_kegemukan_20231105120037.adoc[Penurun berat badan].
  * link:./khasiat_pereda_masukangin_20231105110415.adoc[Pereda pilek masuk angin].
  * link:./khasiat_pereda_prostat_20241014024001.adoc[Meredakan prostat].
  * link:./khasiat_pereda_darahtinggi_20231105114947.adoc[Menurunkan darah tinggi].
  * link:./khasiat_pereda_maag_20231105111232.adoc[Pereda gangguan lambung].
untuk klaim OK/SK sesuaikan dengan peringatan klaim diatas.

== Pasal 27 Nomor Izin Edar (NIE).
* Struktur:
  POM XX 000000000E

XX: Kode golongan OBAOKSK.
000000000: No.reg dr BPOM.
E: Produk ekspor.

== Pasal 28 Kode Produksi (No. Bets).
. Kode sediaan.
. Produksi ke?
. Tgl produksi.
. Tgl ED NIE.

== Pasal 29 Kadaluarsa (ED).
. Struktur:
  * HHBBTT
  * BBTT
  HH: tgl.
  BB: bulan.
  TT: tahun.
. Bila terpisah dr kemasan primer beri keterangan:
  * "Kadaluarsa, lihat......"
. Cantumkan BUD (Beyond Used Date).

== Pasal 30 Kondisi Penyimpanan.
. Cantumkan kondisi penyimpanan setelah kemasan dibuka.

== Pasal 31 2D Barcode.
. Sesuai link:./ketentuan BPOM.

== Pasal 32-33 Logo golongan OBAOKSK.
. Hanya untuk peredaran/lisensi lokal.
. Wajib ada pada kemasan primer, skunder dan brosur.
. *Dikecualikan* u/ strip dan blister.
. Kontras jelas dan mudah dibaca.
. Jamu.
  * "Ranting daun dlm lingkaran hijau".
  * Letak: atas kiri.
  * Deskripsi: "JAMU".

image::https://istanaumkm.pom.go.id/storage/app/media/uploaded-files/1%20jamu.png[Logo Jamau by Istana UMKM]

. Obat Herbal Terstandar(OHT).
  * "Jari-jari daun(3 pasang) dlm lingkaran hijau".
  * Letak: atas kiri.
  * Deskripsi: "OBAT HERBAL TERSTANDAR".

image::https://istanaumkm.pom.go.id/storage/app/media/uploaded-files/2.obat%20herbal%20terstandar.jpg[Logo OHT by Istana UMKM]

. Fitofarmaka.
  * "Jari-jari daun(mambentuk bintang) dlm lingkaran hijau".
  * Letak: atas kiri.
  * Deskripsi: "FITOFARMAKA".

image::https://istanaumkm.pom.go.id/storage/app/media/uploaded-files/3.%20Fitorafmaka.jpg[Logo Fitofarmaka by Istana UMKM]

== Pasal 34-38 Info bahan/teknologi tertentu.
. Bahan harus tersertifikasi.
. Harus mencantumkan:
  * Produk Rekayasa Genetika(DNA GMO >=5%).
    ** Dicantumkan pd tiap nana bahan dlm komposisi produk.
  * Produk Nano.
    ** Dicantumkan pd tiap nana bahan dlm komposisi produk.
  * Produk Iradiasi(Sertifikasi BATAN).
    .. "TIDAK BOLEH DIIRADIASI ULANG".
    .. Tanggal (HH/BB/TTTT) iradiasi.
    .. Tempat iradiasi.

image::https://brainergyodienjaya.wordpress.com/wp-content/uploads/2014/10/logo-irradiasi.jpg[Logo Iradiasi by MENKES]

  * Produk Organik(bhn organik >95%).
    ** "Lingkaran terdiri dr 2 bagian, bertuliskan Organik Indonesia terdapat ganbar daun menempel pada huruf G berbentuk buntil akar."

image::https://javara.co.id/wp-content/uploads/2019/12/Logo-Organik-Indonesia-01.png[Logo Organik by BSN]

== Pasal 39-40 Pembinaan dan Larangan.
. Bebas dari bahan tertentu.
. Mencantumkan gambar/keterangan terkait tenaga kesehatan, tokoh agama/pejabat publik.
. Menyinggung SARA(Suku, Agama, Ras dan Antar golongan).
. Mencantumkan undian/sayembara/hadiah.
. Menyinggung norma, etika dan ketertiban umum.
. Mencantumkan visual/informasi yg berlebih/ tidak berhubungan dgn produk.
. Mencantumkan informasi yg menyesatkan.

== Pasal 41-42 Sanksi.
. Sanski Administratif:
  * Peringatan.
  * Penarikan.
  * Penghentian sementara.
  * Pencabutan CPOTB.
  * Pembatalan NIE.
  * Pengumuman publik.

== Pasal 43-44 Peralihan dan Penutup.
. Masa tengang 24 bln(aktif *Juni 2026*)

== Lampiran: Informasi lain pd penandaan.
. Informasi nilai gizi(ING).
  * Teruji di lab akreditasi *KAN*.
  * Juknis sesuai pangan olahan.
. Dokumen penjamin mutu.
  * Tersertifikasi (SNI, CPOTB, ISO dll).
. Informasi layanan konsumen.
  * Dapat berupa berupa: wesite, sosmed, tlp, email.
. Gambar/Logo/Tulisan.
  * Hanya bila terkandung dalam bahan aktif.
  * Bahan perisa hanya dlm bentuk gambar animasi.
  * Logo/gambar vegan hanya boleh bila tersertifikasi dan teruji kandungan DNA hewani.
. Info bebas gluten, alkohol, BTP.
  * Data dukung(uji lab).
. Info bebas/rendah laktosa.
  * Hanya u/ produk lazim laktosa.
  * Syarat:
    .. Bebas laktosa <= 10mg/100 kkal.
    .. Rendah laktosa <= 2g/100 g.
. Teruji klinis.
  * Memenuhi CUKB.
. Produk berprestasi.
  * Tersertifikasi aktif(<2 th).
  * Cantumkan tahun tersertifikasinya.

''''

== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./regulasi_20240724063752.adoc[Regulasi]
. link:./oba_20240724064126.adoc[OBA]
. link:./supleman_20241224084846.adoc[Suplemen Kesehatan].
. link:./regristrasi_OBA_20241016064416.adoc[Regristrasi]
. link:./checklist_penandaan_20241105030608.adoc[Checklist]
