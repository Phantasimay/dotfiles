= Command Layer on DBMS

== Data Definition Language (DDL)
. DBMS.
  * SHOW.
  * CREATE.
    ** USER.
    ** DATABASE.
    ** TABLE.
  * DROP.
  * USE.
  * GRANT. =`memberi previlage/kewenangan`
  * REVOKE. =`mencabut kewenangan/previlage`
  * DATABASE(S).
  * ENGINE(S).
. Database.
  * TABLE(S).
  * DESC(RIBE).
  * (UN)LOCK.
    ** READ.
    ** WRITE.
  * TRUNCATE. = `membersihkan isi table!!`.
  * ALTER.
    ** ADD.
    ** DROP.
    ** RENAME.
    ** MODIFY.

== Data Manipulation Language (DML)
. Field/Table.
  * INSERT.
    ** INTO.
  * SELECT.
    ** FROM.
  * DELETE.
    ** FROM.
  * UPDATE.
    ** SET.

== Addition Perfix/Surfix.
. Addition
  * WHERE. = `kondisional statment/IF`
  * ORDER BY.
    ** ASC.
    ** DESC.
  * VALUE(S).
  * DEFAULT. = `nilai default`.
  * CURRENT_(TYPEDATA).
  * (NOT) NULL. = `tidak boleh kosong`.
  * ZEROFILL. = `auto isi nol`.
  * (UN)SIGNED. = `(tidak) boleh pake minus`.
  * AS. = `alias, best practic hindari spasi ganti dengan _ atau -`.
  * BETWEEN.
  * IN.
  * LIKE.
  * link:./oracle_regex_20231024034622.adoc[REGEXP].
  * LIMIT.
  * DISTINCT. = `filter duplikat (pada saat ditampilkan) recomended untuk eta ENUM`
  * AUTO_INCREMENT. = `otomatis +1 kusus PRIMARY KEY`.
. Function Support.
  * Arithmatic/Numeric.
  * String.
  * Date/Time.
  * Flow Conttrol. = `If Statment`.
  * Aggregatae. = `Sort/Filter`.
    ** GROUP BY. = `pengabungan nilai, biasa digunakan untuk agregat data`.
    ** HAVING. = `pengati WHERE untuk kondisional pd agregat data`.
  * PRIMARY KEY.
  * UNIQUE KEY. = `cegah duplikat (pada saat input data) not recomended untuk data ENUM`
  * link:./dbms_relational_20240426024516.adoc[FOREIGN KEY]. = `syarat type harus sama dengan REFERENCES dan tidak boleh enable NOT NULL`.
    ** REFERENCES. = `syarat harus UNIQUE atau AUTO_INCREMENT meski PRIMARY dan memiliki type yang sama dengan FOREIGN KEY`.
  image::./asset/foreign_key_behavior.jpg[Behavior of Foreign Key]
  * Indexing:
    ** link:./dbms_indexing_20240417013345.adoc[INDEX]. = `Balansing Search(query)-Manipulation(CRUD) data performance, all KEY generate INDEX`.
    ** FULLTEXT.
       *** Natural Language.
       *** Boolean.
       *** Query Expanssion.
  * CONSTRAINT. = `pembatasan sebelum imput data`.
  * CHECK. =`kondisional cek`.
  * JOIN. =`mengabungkan >=2 kolumn dari tabel yg berbeda dgn type sama, hati2 dapat memperlambat query`
    ** INNER (default).
    ** CROSS (mengalikan tiap kolumn).
    ** RIGHT.
    ** LEFT.
  * Subquery
    Use between `()` in:
    ** WHERE.
    ** FROM.
  * Set Operator:
    ** UNION =`pengabungan dan duplikan dihapus`.
    ** UNION ALL.
    ** INTERSECT.
    ** MINUS.
  * TRANSACTION = `All or nothing SQL` with start,commit and rollback.
  * Locking =`mencegah inkonsistensi data` menggunakan SELECT FOR UPDATE
    ** Saat TRANSACTION aktif.
    ** Manual dengan SELECT FOR UPDATE.
    ** LOCK TABLE
        *** READ.
        *** WRITE.
    ** LOCK INSTANCE FOR BACKUP. untuk lock struktur database, DDL inaktif.

Note: Upper layer command can be use in inner layer

== MindMap

. link:./favorite.adoc[Favorite]

'''''
include::./reference/20240414201741.adoc[20240414201741]
