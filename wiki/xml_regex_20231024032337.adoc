= XML Schema Regular Expression.

XML sch_me link:./regex_20230911085122.adoc[reg_x] link:./regex_flavor_20231025032131.adoc[flav_ur] is very lim_ted, bec_us_ it j_st use to v_lid_ted ent_y el_ment.

== Feature Support:

. Use link:./regex-diredted_engine_20231027060457.adoc[Text-direct engine], so link:./anchors_regex_20231024091633.adoc[anchor] not support.
. All character are link:./literal_character_20231024092839.adoc[Literal], except link:./bre_ere_20231023140222.adoc[ERE].
. Character escape support.
. Eager link:./alteration_regex_20231024151535.adoc[alteration].
. All character inside `[]` are literal, except `]^\-`.
. link:./advance_regex_20231029103352.adoc[Substraction] link:./character_set_regex_20231024152844.adoc[character classes].
. Shorthand ASCII and Unicode.
. Additional link:./shorthand_character_20231022150233.adoc[Shorthand].
  * `\i` : 1st character.
  * `\c` : 2nd character.
. Greedy `*` (star) and fix `{N}` link:./repetition_regex_20231024151826.adoc[quantifier].
. https://en.wikipedia.org/wiki/Unicode_symbol[Unicode] category, properties, blocks and negated.
. link:./grouping_regex_20231024153157.adoc[Capturing] group.

'''''

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[favorite]
. link:./xpath_regex_20231024035009.adoc[xpath regex]
. link:./vbscript_regex_20231024042310.adoc[vbscript regex]

link:./reference/20231024032337.adoc[20231024032337]
