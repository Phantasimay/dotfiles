= _Piper methysticum_
:keywords: herbal, piper, blacklist
:toc:

image::https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Starr_070515-7054_Piper_methysticum.jpg/1024px-Starr_070515-7054_Piper_methysticum.jpg[Daun wati by wikipedia]

* Species  : P. methysticum G.Forst.
* Local    : Daun wati.
* Synonyms : Kava-kava.
* Part     :

== Organoleptis

. Bentuk :
  * Bau   :
  * Warna :
  * Rasa  :

== Peringatan

* *Seluruh bagian* dilarang digunakan u/ pengobatan.

== Khasiat

.
  *

== Takaran pakai


== link:./deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]
. link:./bahan_terlarang_20241105035019.adoc[Bahan Terlarang]

'''''
include::./reference/20241107152954.adoc[20241107152954]
