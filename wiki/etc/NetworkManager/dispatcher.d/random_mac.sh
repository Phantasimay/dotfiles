#!/bin/sh

IF=$1
STATUS=$2
PING=$(ping www.google.com -c 2)
MACCHANGER=/usr/bin/macchanger
WLANIFACE="wlan0"

if [ -z "$IF" ]; then
echo "$0: called with no interface" 1>&2
exit 1;
fi
if [ ! -x $MACCHANGER ]; then
echo "$0: can't call $MACCHANGER" 1>&2
exit 1;
fi

if [ "$IF" = "$WLANIFACE" ] && [ "$STATUS" = "down" ]; then
/usr/sbin/ip link set $IF down
$MACCHANGER -r $IF
/usr/sbin/ip link set $IF up
fi

while [ "$IF" = "$WLANIFACE" ] && [ "$STATUS" = "up" ]; do
ping www.google.com -c 2 1>/dev/null 2>&1
if [ $? -ne 0 ]; then
service NetworkManager restart
sleep 50
elif [ $? -eq 0 ]; then
ping www.google.com -c 2 1>/dev/null 2>&1
fi
sleep 50
done

