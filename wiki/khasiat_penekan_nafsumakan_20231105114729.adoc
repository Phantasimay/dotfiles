= Herbal penekan nafsu makna:

* link:./camellia_sinensis_20231214080837.adoc[Daun Teh].
* link:/sonchus_arvensis_20240203020350[Daun Tempuyung].
* link:./cocos_nucifera_20231228040303.adoc[Kelapa].
* link:./hibiscus_sabdariffa_20240207021823.adoc[Rosela].
* link:./sonchus_arvensis_20240203020350.adoc[Tempuyung].
* link:./olea_europea_20231224205147.adoc[Zaitun].

== link:$WIKI_DIR/deck/0.adoc[QnA]

== MindMap

. link:./favorite.adoc[Favorite]
. link:./herbal_list_20231018172017.adoc[Herbal]

'''''
include::./reference/20231105114729.adoc[20231105114729]
