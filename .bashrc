# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples
#
albafetch

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTFILE=~/.zsh_history
HISTSIZE=1000
HISTFILESIZE=2000

# force zsh to show the complete history
alias history="history 0"

WORDCHARS=${WORDCHARS//\/} # Don't consider certain characters part of the word

# hide EOL sign ('%')
PROMPT_EOL_MARK=""

# configure `time` format
TIMEFMT=$'\nreal\t%E\nuser\t%U\nsys\t%S\ncpu\t%P'

# configure key keybindings
set -o vi

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

# The following block is surrounded by two delimiters.
# These delimiters must not be modified. Thanks.
# START KALI CONFIG VARIABLES
PROMPT_ALTERNATIVE=twoline
NEWLINE_BEFORE_PROMPT=yes
# STOP KALI CONFIG VARIABLES

if [ "$color_prompt" = yes ]; then
    # override default virtualenv indicator in prompt
    VIRTUAL_ENV_DISABLE_PROMPT=1

    prompt_color='\[\033[;32m\]'
    info_color='\[\033[1;34m\]'
    prompt_symbol=㉿
    if [ "$EUID" -eq 0 ]; then # Change prompt colors for root user
        prompt_color='\[\033[;94m\]'
        info_color='\[\033[1;31m\]'
        # Skull emoji for root terminal
        #prompt_symbol=💀
    fi
    case "$PROMPT_ALTERNATIVE" in
        twoline)
            PS1=$prompt_color'┌──${debian_chroot:+($debian_chroot)──}${VIRTUAL_ENV:+(\[\033[0;1m\]$(basename $VIRTUAL_ENV)'$prompt_color')}('$info_color'\u'$prompt_symbol'\h'$prompt_color')-[\[\033[0;1m\]\w'$prompt_color']\n'$prompt_color'└─'$info_color'\$\[\033[0m\] ';;
        oneline)
            PS1='${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV)) }${debian_chroot:+($debian_chroot)}'$info_color'\u@\h\[\033[00m\]:'$prompt_color'\[\033[01m\]\w\[\033[00m\]\$ ';;
        backtrack)
            PS1='${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV)) }${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ ';;
    esac
    unset prompt_color
    unset info_color
    unset prompt_symbol
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*|Eterm|aterm|kterm|gnome*|alacritty)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

[ "$NEWLINE_BEFORE_PROMPT" = yes ] && PROMPT_COMMAND="PROMPT_COMMAND=echo"

# enable color support of ls, less and man, and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    export LS_COLORS="$LS_COLORS:ow=30;44:" # fix ls color for folders with 777 permissions

    alias ls='lsd'
    alias cat='batcat'
    alias more='less'
    alias rm='rm -i'
    alias ps='ps -x'
    alias psx='ps -aux'
    # alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias diff='diff --color=auto'
    alias ip='ip --color=auto'

    export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
    export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
    export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
    export LESS_TERMCAP_so=$'\E[01;33m'    # begin reverse video
    export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
    export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
    export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
. "$HOME/.cargo/env"
# Predictable SSH authentication socket location.
SOCK="$HOME/.ssh/agent_sock"
AGENT_ENV="$HOME/.ssh/agent_env"
if ! ps -x | fgrep -v fgrep | fgrep -q $SOCK;
then
    rm -f $SOCK
    ssh-agent -a $SOCK > $AGENT_ENV
fi
eval $(cat $AGENT_ENV)
agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)

# Lynx config modular:
if [[ -n $(which lynx) ]]; then
  [[ -r ~/.config/lynx/lynx.cfg ]]  && lynxcfg="-cfg=$HOME/.config/lynx/lynx.cfg"
  [[ -r ~/.config/lynx/lynx.lss ]]  && lynxlss="-lss=$HOME/.config/lynx/lynx.lss"
  alias lynx="lynx '$lynxcfg' '$lynxlss' -use_mouse -justify -homepage=https://www.duckduckgo.com"
fi

# Fix color scheme terminal:
LS_COLORS=$LS_COLORS:'ow=00' ; export LS_COLORS

alias f='vim "$(fzf)"'
alias fzf="fzf --preview 'batcat --style=numbers --color=always --line-range :500 {} '"
alias yt="pipe-viewer"
alias showkey="sudo showkey"
# alias cat="batcat"
alias ytg="nohup gtk-pipe-viewer &"
alias doas="doas --"
alias ..="cd .."
alias ...="cd ../.."
alias .3="cd ../../.."
alias .4="cd ../../../.."
alias .5="cd ../../../../.."
alias df="df -h"
alias cp="cp -i"
alias free="free -m"
alias psmem="ps auxf|sort -nr -k 4"
alias psmem10="ps auxf|sort -nr -k 4|head -10"
alias pscpu="ps auxf|sort -nr -k 3"
alias pscpu10="ps auxf|sort -nr -k 3|head -10"
alias addup="git add -u"
alias addall="git add ."
alias branch="git branch"
alias checkout="git checkout"
alias commit="git commit -m"
alias glog="git log --graph --oneline --decorate"
alias fetch="git fetch"
alias pull="git pull origin"
alias push="git push origin"
alias status="git status"
alias tag="git tag"
alias nwetag="git tag -a"
alias jctl="journalctl -p 3 -xb"
alias v=$EDITOR
alias c="clear"
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
alias play="ncmpcpp-ueberzug"
alias tmux="tmux -f $XDG_CONFIG_HOME/tmux/tmux.conf"
alias e="exit"


export LC_ALL=en_US.UTF-8
# default apps
export EDITOR=vi
export TERMINAL=alacritty
export BROWSER=wyeb
export OPENER=xdg-open||mimeopen
export SHELL=zsh
export MENU=fuzzel
export READER=apvlv
#source ${XDG_CONFIG_HOME:-$HOME/.config}/lf/lf-shellcd

# cleaning up home folder
export MPD_HOST=/run/mpd/socket
export WLR_NO_HARDWARE_CURSORS=1
export GDK_BACKEND=wayland vlc
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XKB_DEFAULT_OPTIONS=caps:escape
export XKB_DEFAULT_LAYOUT=us
export SSH_AUTH_SOCK=${HOME}/.ssh-agent.sock
export XDG_RUNTIME_DIR=/run/user/1000
export LYNX_CFG="$HOME/.config/lynx/lynx.cfg"
export LYNX_LSS="$HOME/.config/lynx/lynx.lss"
export FZF_DEFAULT_COMMAND='rg --files --follow --hidden'
export VIMINIT="set nocp | source ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/vimrc"
export PATH="$HOME/.vim/plug/fzf-wordnet.vim/bin:$PATH"
export PATH=$PATH:/opt/flutter/bin/
export PATH=$PATH:$HOME/Android/Sdk/cmdline-tools/latest/bin
export PATH=$PATH:/opt/Cemu/build/bin/
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$HOME/.local/go/bin
export PATH=$PATH:/usr/local/searx/searx-pyenv/bin/activate
export SEARX_SETTINGS_PATH="/etc/searx/settings.yml"
export PATH=$PATH:/root/.cargo/bin/
export PATH=$PATH:/.cargo/bin/
export PATH=$PATH:$HOME/.cargo/bin
export PATH=$PATH:/usr/bin
export PATH=$PATH:$HOME/.cargo/bin/
export PATH=$PATH:/snap/bin/
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$HOME/go/bin
fpath+=${ZDOTDIR:-~}/.zsh_functions
# fpath+=~/.zfunc

# Firefox wayland:
if [ "$XDG_SESSION_TYPE" == "wayland" ]; then
    export MOZ_ENABLE_WAYLAND=1
    export XDG_CURRENT_DESKTOP=sway
    # for GTK-based programs such as firefox and emacs:
    export GDK_DPI_SCALE=2
    # for QT-based programs
    export QT_WAYLAND_FORCE_DPI="physical"
    # or if still too small, use a custom DPI
    export QT_WAYLAND_FORCE_DPI=192 # 2x scaling
    export QT_QPA_PLATFORM="wayland-egl"

fi

#Prompt Login to DE/WM (Sway)
# Make sure this on te bottom of script so all script above can beload
 if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec sway
  xhost +SI:localuser:root > /dev/null
 elif ( command -v tmux>/dev/null ) && [ "${XDG_VTNR}" -eq 2 ] ; then # check if tmux command exists
    if [[ "$(tty)" =~ /dev/tty ]] && [[ ! "$TERM" =~ screen ]] && [ -z "$TMUX" ]; then
        # We're on a TTY and *not* in tmux
        exec tmux -u -f /home/sandun/.config/tmux/tmux.conf
    fi
 fi
